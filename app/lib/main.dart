import 'package:bdf_survey/features/menu/menu.dart';
import 'package:bdf_survey/features/para_data/para_data_scoped_model.dart';
import 'package:bdf_survey/screens/onboarding/login_implicit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'core/controller/start_app.dart';
import 'core/controller/util/responsive_ui.dart';
import 'core/model/color_pallet.dart';
import 'core/model/international.dart';
import 'core/state/configuration.dart';
import 'core/state/translations.dart';
import 'features/filter_drawer/state/filter.dart';
import 'screens/receipt_list/state/receipt_list_state.dart';
import 'screens/settings/state/settings_state.dart';

void main() async {
  await configureApp('prod');
  final prefs = await SharedPreferences.getInstance();
  const bool defaultFrench = true;
  final refresh_token = prefs.getString('refresh_token') ?? '';
  FlutterError.onError = (FlutterErrorDetails details) {};
  runApp(MyApp(
    defaultFrench: defaultFrench,
    refresh_token: refresh_token,
  ));
}

class MyApp extends StatefulWidget {
  final bool defaultFrench;
  final String refresh_token;

  MyApp({required this.defaultFrench, required this.refresh_token});

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    _setIsAuthenticated();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  Future<void> _setIsAuthenticated() async {
    final prefs = await SharedPreferences.getInstance();
    widget.refresh_token.isEmpty
        ? await prefs.setBool('isAuthenticated', false)
        : await prefs.setBool('isAuthenticated', true);
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModel<LanguageSetting>(
      model: LanguageSetting(),
      child: ScopedModelDescendant<LanguageSetting>(builder: (_, __, ___) {
        return ScopedModel<Configuration>(
          model: Configuration(),
          child: ScopedModelDescendant<Configuration>(
              builder: (_, __, configuration) {
            return ScopedModel<ParaDataScopedModel>(
                model: ParaDataScopedModel(configuration.paradata),
                child: ScopedModel<SettingState>(
                  model: SettingState(),
                  child: ScopedModel<FilterState>(
                    model: FilterState(GlobalKey<ScaffoldState>()),
                    child: ScopedModel<ReceiptListState>(
                      model: ReceiptListState(),
                      child: Builder(
                        builder: (context) {
                          return MaterialApp(
                              navigatorObservers: [
                                ParaDataScopedModel.of(context).observer()
                              ],
                              builder: (BuildContext context, Widget? child) {
                                initializeUIParemeters(context);
                                return MediaQuery(
                                  data: MediaQuery.of(context).copyWith(
                                      textScaler: TextScaler.linear(1)),
                                  child: child!,
                                );
                              },
                              debugShowCheckedModeBanner: false,
                              localizationsDelegates: const [
                                GlobalMaterialLocalizations.delegate,
                                GlobalWidgetsLocalizations.delegate,
                                GlobalCupertinoLocalizations.delegate,
                              ],
                              supportedLocales: International.locales(),
                              theme: ThemeData(
                                colorScheme: colorScheme,
                                scaffoldBackgroundColor: Colors.white,
                              ),
                              home: widget.refresh_token.isEmpty
                                  ? LoginImplicitScreen()
                                  : Menu());
                        },
                      ),
                    ),
                  ),
                ));
          }),
        );
      }),
    );
  }
}
