import 'package:flutter/widgets.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:sqflite/sqflite.dart';

import '../../core/data/database/database_helper.dart';
import '../../core/state/configuration.dart';
import 'para_data.dart';
import 'para_data_navigator_observer.dart';
import 'para_data_table.dart';

class ParaDataScopedModel extends Model {
  bool _isEnabled = true;

  ParaDataScopedModel(ParadataConfiguration paradataDefault) {
    _isEnabled = paradataDefault == ParadataConfiguration.enabled;
  }
  static ParaDataScopedModel of(BuildContext context) =>
      ScopedModel.of<ParaDataScopedModel>(context);

  Future<void> openScreen(String name, String action) async {
    // assert(name != null);
    if (_isEnabled) {
      print('open screen: $name');
      final _table = await _paraDataTable();
      await _table.insert(ParaData((b) => b
        ..timestamp = DateTime.now().toUtc()
        ..objectName = name
        ..action = action));
    }
  }

  Future<void> openApp() async {
    if (_isEnabled) {
      //print('open app');
      final _table = await _paraDataTable();
      await _table.insert(ParaData((b) => b
        ..timestamp = DateTime.now().toUtc()
        ..objectName = 'app'
        ..action = 'ouverture'));
    }
  }

  Future<Database> _database() async {
    return DatabaseHelper.instance.database;
  }

  Future<ParaDataTable> _paraDataTable() async {
    return ParaDataTable(await _database());
  }

  Future<void> closeScreen(String name, String action) async {
    if (_isEnabled) {
      print('close screen: $name');
      final _table = await _paraDataTable();
      await _table.insert(ParaData((b) => b
        ..timestamp = DateTime.now().toUtc()
        ..objectName = name
        ..action = action));
    }
  }

  Future<void> onTap(String widgetType, String action) async {
    print('tapped $widgetType to perform $action');
    if (_isEnabled) {
      final _table = await _paraDataTable();
      await _table.insert(ParaData((b) => b
        ..timestamp = DateTime.now().toUtc()
        ..objectName = widgetType
        ..action = action));
    }
  }

  NavigatorObserver observer() {
    return ParaDataNavigatorObserver(this);
  }
}
