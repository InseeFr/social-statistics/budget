import 'package:flutter/widgets.dart';
import 'para_data_scoped_model.dart';

class ParaDataNavigatorObserver extends NavigatorObserver
    with WidgetsBindingObserver {
  final ParaDataScopedModel model;

  ParaDataNavigatorObserver(this.model) {
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void didPop(Route<dynamic> route, Route<dynamic>? previousRoute) {
    //debugPrint('didPop: $route');
    final String? routeName = route.settings.name;
    if (routeName == 'ManualEntryScreen') {
      print('--- didPop when creating receipt ---');
      model.closeScreen(route.settings.name!, 'suppr_dep');
    }
  }

  @override
  void didPush(Route<dynamic> route, Route<dynamic>? previousRoute) {
    //debugPrint('didPush: $route');
    final String? routeName = route.settings.name;
    if (routeName == 'SearchNotFoundDialog') {
      print('didPush when using the Notfound while searching: $routeName');
      model.openScreen(route.settings.name!, 'pas_trouve');
    }
    // if (routeName != null) {
    //   print('didPush: $routeName');
    // }
  }

  // @override
  // void didRemove(Route<dynamic> route, Route<dynamic>? previousRoute) {
  //   //debugPrint('didRemove: $route');
  //   final String? routeName = route.settings.name;
  //   if (routeName != null) {
  //     print('didRemove: $routeName');
  //   }
  //   model.closeScreen(route.settings.name!);
  // }

  // @override
  // void didReplace({Route<dynamic>? newRoute, Route<dynamic>? oldRoute}) {
  //   //debugPrint('didReplace: $newRoute');
  //   final String? routeName = newRoute!.settings.name;
  //   if (routeName != null) {
  //     print('didReplace: $routeName');
  //   }
  //   model.closeScreen(oldRoute!.settings.name!);
  //   model.openScreen(newRoute.settings.name!);
  // }

  @override
  void didStartUserGesture(
      Route<dynamic> route, Route<dynamic>? previousRoute) {
    print('didStartUserGesture: $route ,from $previousRoute');
  }

  @override
  void didStopUserGesture() {
    print('didStopUserGesture');
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) {
      model.openApp();
    }
  }

  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
  }
}
