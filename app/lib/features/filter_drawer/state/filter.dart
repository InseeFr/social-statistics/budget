import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class FilterState extends Model {
  late GlobalKey<ScaffoldState> scaffoldKey;
  late bool isSearchMode = false;
  late String searchInput;
  late DateTime startDate, endDate;
  late double minValue, maxValue;

  FilterState(this.scaffoldKey) {
    reset();
  }

  void reset() {
    searchInput = '';
    minValue = double.infinity * -1;
    maxValue = double.infinity;
    startDate = DateTime(2019);
    endDate = DateTime.now();
    isSearchMode = false;
    notifyListeners();
  }

  bool isOff() {
    // ignore: unnecessary_null_comparison
    return minValue == null && maxValue == null && startDate == null && endDate == null;
  }

  void setSearchInput(String searchInput) {
    this.searchInput = searchInput;
    notifyListeners();
  }

  void enableSearchMode() {
    isSearchMode = true;
    notifyListeners();
  }

  void disableSearchMode() {
    isSearchMode = false;
    reset();
    notifyListeners();
  }

  void showDrawer() {
    scaffoldKey.currentState!.openEndDrawer();
  }

  void notify() {
    notifyListeners();
  }

  static FilterState of(BuildContext context) => ScopedModel.of<FilterState>(context);
  @override
  String toString() {
    return 'FilterState(scaffoldKey: $scaffoldKey, isSearchMode: $isSearchMode, searchInput: $searchInput, startDate: $startDate, endDate: $endDate, minValue: $minValue, maxValue: $maxValue)';
  }
}
