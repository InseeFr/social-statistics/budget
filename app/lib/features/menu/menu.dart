import 'dart:async';
import 'dart:math' as math;

import 'package:bdf_survey/core/controller/user_progress.dart';
import 'package:bdf_survey/core/controller/util/responsive_ui.dart';
import 'package:bdf_survey/core/data/server/auth_pkce.dart';
import 'package:bdf_survey/core/model/color_pallet.dart';
import 'package:bdf_survey/core/model/progress_lists.dart';
import 'package:bdf_survey/core/model/receipt.dart';
import 'package:bdf_survey/core/state/configuration.dart';
import 'package:bdf_survey/core/state/translations.dart';
import 'package:bdf_survey/core/widget/token_timed_out_dialog.dart';
import 'package:bdf_survey/features/filter_drawer/state/filter.dart';
import 'package:bdf_survey/screens/camera_entry/ocr_plugin/receipt_scanner.dart';
import 'package:bdf_survey/screens/insights/insights_screen.dart';
import 'package:bdf_survey/screens/manual_entry/manual_entry_screen.dart';
import 'package:bdf_survey/screens/overview/overview_screen.dart';
import 'package:bdf_survey/screens/receipt_list/receipt_list_screen.dart';
import 'package:bdf_survey/screens/settings/settings_screen.dart';
import 'package:bdf_survey/screens/settings/state/settings_state.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:scoped_model/scoped_model.dart';

GlobalKey keyButtonOverview = GlobalKey();
late Translations translations;

class Menu extends StatefulWidget {
  final currentIndex;
  const Menu({this.currentIndex = 0});

  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu>
    with TickerProviderStateMixin, WidgetsBindingObserver {
  Timer? _timer;
  static const List<IconData> icons = [
    Icons.camera_alt,
    Icons.keyboard,
  ];

  late AnimationController _controller;
  late ProgressLists progressLists;
  int _currentScreenIndex = 0;
  final List<Widget> _screens = [
    OverviewScreen(keyButtonOverview),
    ReceiptListScreen(),
    Container(),
    InsightsScreen(),
    SettingsScreen(),
  ];

  static void changeStatusBarColor() {
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: ColorPallet.primaryColor,
        systemNavigationBarColor: Colors.black,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    changeStatusBarColor();
    _currentScreenIndex = widget.currentIndex;
    WidgetsBinding.instance.addObserver(this);
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 250),
    );
    _timer = Timer.periodic(Duration(minutes: 5), (timer) {
      checkTokenAndShowDialog(AppLifecycleState.resumed);
    });

    UserProgressController()
        .getProgressLists()
        .then((value) => progressLists = value);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _timer?.cancel();
    super.dispose();
  }

  Future<void> checkTokenAndShowDialog(AppLifecycleState state) async {
    if (state == AppLifecycleState.resumed) {
      print(" >> AppLifecycleState.resumed");
      if (!await AuthPKCE.checkAndRefreshToken()) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return TokenTimedOutDialog();
          },
        );
      }
      ;
    }
  }

  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Calendar');

    return ScopedModelDescendant<Configuration>(
        builder: (_, __, configuration) {
      return PopScope(
        canPop: false,
        child: Stack(
          children: <Widget>[
            Positioned(
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      color: ColorPallet.primaryColor,
                    ),
                  ),
                  Expanded(
                    child: Container(
                      color: Colors.white,
                    ),
                  )
                ],
              ),
            ),
            Positioned(
              bottom: 10 * y,
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      key: keyButtonOverview,
                      height: 50.0 * y,
                      width: 56.0 * x,
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              child: Scaffold(
                resizeToAvoidBottomInset: false,
                floatingActionButtonLocation:
                    FloatingActionButtonLocation.centerDocked,
                floatingActionButton: SizedBox(
                  height: 230 * y,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: List.generate(icons.length, (int index) {
                      final child = Container(
                        height: 70.0 * y,
                        width: 56.0 * x,
                        alignment: FractionalOffset.topCenter,
                        child: ScaleTransition(
                          scale: CurvedAnimation(
                            parent: _controller,
                            curve: Interval(0, 1.0 - index / icons.length / 2.0,
                                curve: Curves.easeOut),
                          ),
                          child: FloatingActionButton(
                            heroTag: null,
                            foregroundColor: ColorPallet.white,
                            backgroundColor: ColorPallet.primaryColor,
                            onPressed: () async {
                              await _controller.reverse();
                              if (icons[index] == Icons.camera_alt) {
                                await openReceiptScanner(
                                    context, DateTime.now());
                              } else if (icons[index] == Icons.keyboard) {
                                await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    settings: const RouteSettings(
                                        name: 'ManualEntryScreen'),
                                    builder: (context) => ManualEntryScreen(
                                      Receipt.empty(),
                                      false,
                                      false,
                                      DateTime.now(),
                                    ),
                                  ),
                                );
                              }
                              _currentScreenIndex = 0;
                            },
                            child: Icon(icons[index],
                                color: Colors.white, size: 26 * x),
                          ),
                        ),
                      );
                      return child;
                    }).toList()
                      ..add(
                        Container(
                          width: 69.0 * x,
                          height: 69.0 * y,
                          child: FittedBox(
                            child: FloatingActionButton.large(
                              heroTag: null,
                              foregroundColor: ColorPallet.white,
                              backgroundColor: ColorPallet.primaryColor,
                              onPressed: () {
                                if (_controller.isDismissed) {
                                  _controller.forward();
                                } else {
                                  _controller.reverse();
                                }
                              },
                              child: AnimatedBuilder(
                                animation: _controller,
                                builder: (BuildContext context, Widget? child) {
                                  return Transform(
                                    transform: Matrix4.rotationZ(
                                        _controller.value * 0.75 * math.pi),
                                    alignment: FractionalOffset.center,
                                    child: Icon(
                                      _controller.isDismissed
                                          ? Icons.add
                                          : Icons.add,
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                        ),
                      ),
                  ),
                ),
                body: Container(
                  color: ColorPallet.primaryColor,
                  child: SafeArea(
                    bottom: false,
                    top: true,
                    left: false,
                    right: false,
                    child: Container(
                      color: Colors.white,
                      child: _screens[_currentScreenIndex],
                    ),
                  ),
                ),
                bottomNavigationBar: ScopedModelDescendant<SettingState>(
                    builder: (context, child, model) {
                  return Theme(
                    data: Theme.of(context).copyWith(
                        canvasColor: Colors.white,
                        primaryColor: Colors.blue,
                        textTheme: Theme.of(context).textTheme.copyWith(
                            bodySmall: const TextStyle(color: Colors.grey))),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                              color: ColorPallet.veryLightGray,
                              offset: Offset(0.0 * x, -2.2 * y),
                              blurRadius: 1.7 * y,
                              spreadRadius: 1.7 * y)
                        ],
                      ),
                      child: BottomNavigationBar(
                        elevation: 0,
                        iconSize: 30.0 * x,
                        onTap: (index) {
                          setState(() {
                            if (index != 2) {
                              FilterState.of(context).reset();
                              _currentScreenIndex = index;
                            }
                            _controller.reverse();
                          });
                        },
                        fixedColor: ColorPallet.primaryColor,
                        type: BottomNavigationBarType.fixed,
                        currentIndex: _currentScreenIndex,
                        items: [
                          {
                            'icon': Icons.event_note,
                            'text': translations.text('overviewPage')
                          },
                          {'icon': Icons.receipt, 'text': 'Dépenses'},
                          {'icon': Icons.add, 'text': ''},
                          {
                            'icon': Icons.equalizer,
                            'text': translations.text('insightsPage')
                          },
                          {
                            'icon': Icons.settings,
                            'text': translations.text('settings')
                          },
                        ].map(
                          (Map<String, dynamic> values) {
                            return BottomNavigationBarItem(
                              icon: Icon(values['icon']),
                              // ignore: deprecated_member_use
                              label: values['text'],
                            );
                          },
                        ).toList(),
                      ),
                    ),
                  );
                }),
              ),
            ),
          ],
        ),
      );
    });
  }
}
