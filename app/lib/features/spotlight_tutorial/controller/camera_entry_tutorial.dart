import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/state/translations.dart';
import '../widget/animated_focus_light.dart';
import '../widget/content_target.dart';
import '../widget/target_focus.dart';
import '../widget/tutorial_coach_mark.dart';

List<TargetFocus> targets = [];

TextStyle titleStyle = TextStyle(
  fontWeight: FontWeight.w700,
  color: Colors.white,
  fontSize: 22.0 * f,
  height: 1.4,
);
TextStyle bodyStyle = TextStyle(
  fontWeight: FontWeight.w400,
  color: Colors.white,
  fontSize: 15.0 * f,
  height: 1.4,
);

void initTargets(
    GlobalKey keyButton1,
    GlobalKey keyButton2,
    GlobalKey keyButton3, GlobalKey keyButton4) {
  if (targets.isEmpty) {
    targets.add(
      TargetFocus(
        identify: 'normal',
        keyTarget: keyButton1,
        contents: [
          ContentTarget(
              align: AlignContent.top,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 10 * y),
                  Text(
                    Translations.textStatic('cameratuto1', 'Camera', null),
                    style: titleStyle,
                    textAlign: TextAlign.center,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10.0 * y),
                    child: Text(
                      Translations.textStatic('cameratuto11', 'Camera', null) +
                          '\n' +
                          Translations.textStatic(
                              'cameratuto12', 'Camera', null) +
                          '\n' +
                          Translations.textStatic(
                              'cameratuto13', 'Camera', null),
                      style: bodyStyle,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: 150 * y),
                  Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/images/tab_symbol.png',
                        height: 100 * y,
                      ),
                      SizedBox(height: 20 * y),
                      Text(
                        Translations.textStatic(
                            'swipeToNavigate', 'Overview_T', null),
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w500,
                            fontSize: 20 * f),
                      ),
                    ],
                  ),
                ],
              ))
        ],
        shape: ShapeLightFocus.RRect,
      ),
    );
    targets.add(
      TargetFocus(
        identify: 'normal',
        keyTarget: keyButton2,
        contents: [
          ContentTarget(
              align: AlignContent.top,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 10 * y),
                  Text(
                    Translations.textStatic('cameratuto1', 'Camera', null),
                    style: titleStyle,
                    textAlign: TextAlign.center,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10.0 * y),
                    child: Text(
                      Translations.textStatic('cameratuto41', 'Camera', null) +
                          '\n' +
                          Translations.textStatic(
                              'cameratuto42', 'Camera', null) +
                          '\n' +
                          Translations.textStatic(
                              'cameratuto43', 'Camera', null),
                      style: bodyStyle,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: 150 * y),
                ],
              ))
        ],
        shape: ShapeLightFocus.RRect,
      ),
    );

    targets.add(
      TargetFocus(
        identify: 'blue',
        keyTarget: keyButton3,
        contents: [
          ContentTarget(
              child: Column(
            children: <Widget>[
              SizedBox(height: 10 * y),
              Text(
                Translations.textStatic('cameratuto2', 'Camera', null),
                style: titleStyle,
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0 * y),
                child: Text(
                  Translations.textStatic('cameratuto21', 'Camera', null) +
                      '\n' +
                      Translations.textStatic('cameratuto22', 'Camera', null) +
                      '\n' +
                      Translations.textStatic('cameratuto23', 'Camera', null),
                  style: bodyStyle,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 30 * y),
            ],
          ))
        ],
        shape: ShapeLightFocus.RRect,
      ),
    );

    targets.add(
      TargetFocus(
        identify: 'blue',
        keyTarget: keyButton4,
        contents: [
          ContentTarget(
              child: Column(
            children: <Widget>[
              SizedBox(height: 10 * y),
              Text(
                Translations.textStatic('cameratuto3', 'Camera', null),
                style: titleStyle,
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0 * y),
                child: Text(
                  Translations.textStatic('cameratuto31', 'Camera', null) +
                      '\n' +
                      Translations.textStatic('cameratuto32', 'Camera', null),
                  style: bodyStyle,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 30 * y),
            ],
          ))
        ],
        shape: ShapeLightFocus.RRect,
      ),
    );
  }
  print('targets: ${targets.length}');
}

void showTutorial(BuildContext context) {
  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(statusBarColor: ColorPallet.darkTextColor),
  );
  showGestureLogo = true;
  TutorialCoachMark(
    context,
    targets: targets,
    colorShadow: ColorPallet.darkTextColor,
    textSkip: Translations.textStatic('skip', '?', null),
    textStyleSkip: TextStyle(color: Colors.white, fontSize: 16 * f),
    alignSkip: Alignment.topLeft,
    opacityShadow: .98,
    clickSkip: () {
      showGestureLogo = false;
      SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: ColorPallet.primaryColor),
      );
    },
    finish: () {
      SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: ColorPallet.primaryColor),
      );
    },
    clickTarget: (TargetFocus target) {
      showGestureLogo = false;
    },
  ).show();
}
