import 'package:flutter/widgets.dart';

import '../../widget/target_focus.dart';
import '../../widget/target_position.dart';

TargetPosition? getTargetCurrent(TargetFocus target) {
  if (target.keyTarget != null) {
    final key = target.keyTarget;

    try {
      final RenderBox renderBoxRed =
          key!.currentContext!.findRenderObject() as RenderBox;
      final size = renderBoxRed.size;
      final offset = renderBoxRed.localToGlobal(Offset.zero);

      return TargetPosition(size, offset);
    } catch (e) {
      print('ERROR: There is not key information for this tutorial screen');
      return null;
    }
  } else {
    return target.targetPosition;
  }
}
