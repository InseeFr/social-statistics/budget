library tutorial_coach_mark;

import 'package:flutter/material.dart';

import 'target_focus.dart';
import 'tutorial_coach_mark_widget.dart';

class TutorialCoachMark {
  TutorialCoachMark(
    this._context, {
    required this.targets,
    this.colorShadow = Colors.black,
    required this.clickTarget,
    required this.finish,
    this.paddingFocus = 10,
    required this.clickSkip,
    this.alignSkip = Alignment.bottomRight,
    this.textSkip = 'SKIP',
    this.textStyleSkip = const TextStyle(color: Colors.white, fontSize: 16),
    this.opacityShadow = 0.8,
  });

  final AlignmentGeometry alignSkip;
  final Color colorShadow;
  final double opacityShadow;
  final double paddingFocus;
  final List<TargetFocus> targets;
  final String textSkip;
  final TextStyle textStyleSkip;

  final BuildContext _context;
  late OverlayEntry _overlayEntry = _buildOverlay();

  final Function(TargetFocus) clickTarget;

  final Function() finish;

  final Function() clickSkip;

  OverlayEntry _buildOverlay() {
    return OverlayEntry(builder: (context) {
      return TutorialCoachMarkWidget(
        targets: targets,
        clickTarget: clickTarget,
        paddingFocus: paddingFocus,
        clickSkip: clickSkip,
        alignSkip: alignSkip,
        textSkip: textSkip,
        textStyleSkip: textStyleSkip,
        colorShadow: colorShadow,
        opacityShadow: opacityShadow,
        finish: () {
          hide();
        },
        key: null,
      );
    });
  }

  void show() {
    Overlay.of(_context).insert(_overlayEntry);
  }

  void hide() {
    finish();
    _overlayEntry.remove();
    _overlayEntry = _buildOverlay();
  }
}
