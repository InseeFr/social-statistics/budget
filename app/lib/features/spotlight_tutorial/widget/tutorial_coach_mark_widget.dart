import 'dart:async';

import 'package:flutter/material.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../controller/util/util.dart';
import 'animated_focus_light.dart';
import 'content_target.dart';
import 'target_focus.dart';

class TutorialCoachMarkWidget extends StatefulWidget {
  const TutorialCoachMarkWidget(
      {Key? key,
      required this.targets,
      required this.finish,
      this.paddingFocus = 10,
      required this.clickTarget,
      this.alignSkip = Alignment.bottomRight,
      this.textSkip = 'SKIP',
      required this.clickSkip,
      this.colorShadow = Colors.black,
      this.opacityShadow = 0.8,
      this.textStyleSkip = const TextStyle(color: Colors.white)})
      : super(key: key);

  final AlignmentGeometry alignSkip;
  final Color colorShadow;
  final double opacityShadow;
  final double paddingFocus;
  final List<TargetFocus> targets;
  final String textSkip;
  final TextStyle textStyleSkip;

  @override
  _TutorialCoachMarkWidgetState createState() => _TutorialCoachMarkWidgetState();

  final Function(TargetFocus) clickTarget;

  final Function() finish;

  final Function() clickSkip;
}

class _TutorialCoachMarkWidgetState extends State<TutorialCoachMarkWidget> {
  late TargetFocus currentTarget;

  final StreamController _controllerFade = StreamController<double>.broadcast();
  final StreamController _controllerTapChild = StreamController<void>.broadcast();

  @override
  void dispose() {
    _controllerFade.close();
    super.dispose();
  }

  Widget _buildContents() {
    return StreamBuilder(
      stream: _controllerFade.stream,
      initialData: 0.0,
      builder: (_, snapshot) {
        try {
          return AnimatedOpacity(
            opacity: double.tryParse(snapshot.data.toString())!,
            duration: const Duration(milliseconds: 300),
            child: _buildPositionedsContents(),
          );
        } catch (err) {
          return Container();
        }
      },
    );
  }

  Widget _buildPositionedsContents() {
    // if (currentTarget == null) {
    //   return Container();
    // }

    var widgtes = <Widget>[];

    final target = getTargetCurrent(currentTarget);
    final positioned = Offset(target!.offset.dx + target.size.width / 2,
        target.offset.dy + target.size.height / 2);
    double haloWidth;
    double haloHeight;
    if (currentTarget.shape == ShapeLightFocus.Circle) {
      haloWidth = target.size.width > target.size.height ? target.size.width : target.size.height;
      haloHeight = haloWidth;
    } else {
      haloWidth = target.size.width;
      haloHeight = target.size.height;
    }
    haloWidth = haloWidth * 0.6 + widget.paddingFocus;
    haloHeight = haloHeight * 0.6 + widget.paddingFocus;
    var weight = 0.0;

    double? top;
    double? bottom;
    double left;

    widgtes = currentTarget.contents!.map<Widget>((i) {
      switch (i.align) {
        case AlignContent.bottom:
          {
            weight = MediaQuery.of(context).size.width;
            left = 0;
            top = positioned.dy + haloHeight;
            bottom = null;
          }
          break;
        case AlignContent.top:
          {
            weight = MediaQuery.of(context).size.width;
            left = 0;
            top = null;
            bottom = haloHeight + (MediaQuery.of(context).size.height - positioned.dy);
          }
          break;
        case AlignContent.left:
          {
            weight = positioned.dx - haloWidth;
            left = 0;
            top = positioned.dy - target.size.height / 2 - haloHeight;
            bottom = null;
          }
          break;
        case AlignContent.right:
          {
            left = positioned.dx + haloWidth;
            top = positioned.dy - target.size.height / 2 - haloHeight;
            bottom = null;
            weight = MediaQuery.of(context).size.width - left;
          }
          break;
      }

      return Positioned(
        top: top,
        bottom: bottom,
        left: left,
        child: GestureDetector(
          onTap: () {
            _controllerTapChild.add(null);
          },
          child: SizedBox(
            width: weight,
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: i.child,
            ),
          ),
        ),
      );
    }).toList();

    return Stack(
      children: widgtes,
    );
  }

  Widget _buildSkip() {
    return Align(
      alignment: widget.alignSkip,
      child: SafeArea(
        child: StreamBuilder(
          stream: _controllerFade.stream,
          initialData: 0.0,
          builder: (_, snapshot) {
            return AnimatedOpacity(
              opacity: double.tryParse(snapshot.data.toString())!,
              duration: const Duration(milliseconds: 300),
              child: InkWell(
                onTap: () {
                  widget.finish();
                  widget.clickSkip();
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20 * y),
                  child: const Icon(Icons.close, color: Colors.white),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Stack(
        children: <Widget>[
          AnimatedFocusLight(
            targets: widget.targets,
            finish: widget.finish,
            paddingFocus: widget.paddingFocus,
            colorShadow: widget.colorShadow,
            opacityShadow: widget.opacityShadow,
            clickTarget: (target) {
              widget.clickTarget(target);
            },
            focus: (target) {
              currentTarget = target;
              _controllerFade.sink.add(1.0);
            },
            removeFocus: () {
              _controllerFade.sink.add(0.0);
            },
            streamTap: _controllerTapChild.stream,
          ),
          _buildContents(),
          _buildSkip()
        ],
      ),
    );
  }
}
