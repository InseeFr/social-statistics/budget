import 'dart:math';

class StringDistance {
  // Temporary class to calculate the distance between two strings
  static double normalizedLevenshteinDistance(String s1, String s2) {
    int editDistance = levenshteinDistance(s1, s2);
    int maxLength = s1.length > s2.length ? s1.length : s2.length;
    return editDistance.toDouble() / maxLength.toDouble();
  }

  static int levenshteinDistance(String s1, String s2) {
    int m = s1.length;
    int n = s2.length;

    // Create a 2D matrix to store the distances
    List<List<int>> dp = List.generate(m + 1, (_) => List.filled(n + 1, 0));

    for (int i = 0; i <= m; i++) {
      for (int j = 0; j <= n; j++) {
        if (i == 0) {
          dp[i][j] = j;
        } else if (j == 0) {
          dp[i][j] = i;
        } else {
          int substitutionCost = s1[i - 1] == s2[j - 1] ? 0 : 1;
          dp[i][j] = _min(
            dp[i - 1][j] + 1, // Deletion
            dp[i][j - 1] + 1, // Insertion
            dp[i - 1][j - 1] + substitutionCost, // Substitution
          );
        }
      }
    }

    return dp[m][n];
  }

  static int _min(int a, int b, int c) {
    return a < b ? (a < c ? a : c) : (b < c ? b : c);
  }

  static int cosineSimilarity(String s1, String s2) {
    final tokens1 = s1.split(' ');
    final tokens2 = s2.split(' ');

    final tokenSet = Set<String>();
    tokenSet.addAll(tokens1);
    tokenSet.addAll(tokens2);

    final vector1 = List<int>.filled(tokenSet.length, 0);
    final vector2 = List<int>.filled(tokenSet.length, 0);

    for (final token in tokens1) {
      final index = tokenSet.toList().indexOf(token);
      vector1[index]++;
    }

    for (final token in tokens2) {
      final index = tokenSet.toList().indexOf(token);
      vector2[index]++;
    }

    var dotProduct = 0;
    var magnitude1 = 0;
    var magnitude2 = 0;

    for (var i = 0; i < vector1.length; i++) {
      dotProduct += vector1[i] * vector2[i];
      magnitude1 += vector1[i] * vector1[i];
      magnitude2 += vector2[i] * vector2[i];
    }

    final similarity = dotProduct / (sqrt(magnitude1) * sqrt(magnitude2));
    return (similarity * 100).round();
  }

  static int hammingDistance(String s1, String s2) {
    if (s1.length != s2.length) {
      throw ArgumentError('Strings must be of equal length');
    }

    var distance = 0;
    for (var i = 0; i < s1.length; i++) {
      if (s1[i] != s2[i]) {
        distance++;
      }
    }

    return distance;
  }
}
