import '../../state/translations.dart';

extension AddCurrencyFormat on String {
  String addCurrencyFormat() {
    final currencySymbol =
        Translations.textStatic('currencySymbol', 'CurrencySetting', null);
    return '${this.replaceAll('.', ',')}$currencySymbol';
  }
}
