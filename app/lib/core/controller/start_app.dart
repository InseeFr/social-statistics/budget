import 'dart:convert';
import 'dart:core';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../model/color_pallet.dart';
import '../state/translations.dart';
import 'user_progress.dart';

Future<void> configureApp(String env) async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: ColorPallet.primaryColor,
    statusBarBrightness: Brightness.dark,
  ));
  final prefs = await SharedPreferences.getInstance();
  LanguageSetting.key = prefs.getString('languagePreference') ?? 'fr';
  LanguageSetting.tablePreference =
      prefs.getString('tablePreference') ?? 'fr';
  await initializeTranslations();

  final contents = await rootBundle.loadString(
    'assets/config/config-$env.json',
  );

  final json = jsonDecode(contents);

  prefs.setString('apiUrl', json['apiUrl']);
  prefs.setString('issuer', json['issuer']);
  prefs.setString('authClientId', json['authClientId']);
  prefs.setString('discoveryUrl', json['discoveryUrl']);
  prefs.setString('redirectUrl', json['redirectUrl']);
  prefs.setString('authorizationEndpoint', json['authorizationEndpoint']);
  prefs.setString('tokenEndpoint', json['tokenEndpoint']);
  prefs.setString('endSessionEndpoint', json['endSessionEndpoint']);

}

Future<bool> isInitialized() async {
  return UserProgressController().isInitialized();
}
