// ignore_for_file: unnecessary_null_comparison

import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../controller/user_progress.dart';

enum InsightsConfiguration { enabled, delayed }

enum QuestionnaireConfiguration { enabled, disabled }

enum ParadataConfiguration { enabled, disabled }

enum OCRConfiguration { local_ocr, only_crop, disabled }

InsightsConfiguration insightsDefault = InsightsConfiguration.enabled;
QuestionnaireConfiguration questionnaireDefault =
    QuestionnaireConfiguration.disabled;
ParadataConfiguration paradataDefault = ParadataConfiguration.enabled;
OCRConfiguration ocrDefault = OCRConfiguration.disabled;

int insightsDelayInHours = 14 * 24;

class Configuration extends Model {
  bool _loaded = false;
  late InsightsConfiguration insights = insightsDefault;
  late QuestionnaireConfiguration questionnaire = questionnaireDefault;
  late ParadataConfiguration paradata = paradataDefault;
  late OCRConfiguration ocr = ocrDefault;

  Configuration() {
    if (_loaded == false) {
      load();
      _loaded = true;
    }
  }

  Future<void> load() async {

    insights = insightsDefault;
    questionnaire = questionnaireDefault;
    paradata = paradataDefault;
    ocr = ocrDefault;
    final progressLists = await UserProgressController().getProgressLists();
    print(progressLists.daysRemaining.length);
    if (progressLists.daysRemaining.isEmpty) {
      insights = InsightsConfiguration.enabled;
    }
    notifyListeners();
  }

  Future<void> save(
      {required String insightsString,
      required String questionnaireString,
      required String paradataString,
      required String ocrString,
      required String env}) async {
    final _preferences = await SharedPreferences.getInstance();

    insights =
        insightsString.toEnum(InsightsConfiguration.values, insightsDefault);
    questionnaire = questionnaireString.toEnum(
        QuestionnaireConfiguration.values, questionnaireDefault);
    paradata =
        paradataString.toEnum(ParadataConfiguration.values, paradataDefault);
    ocr = ocrString.toEnum(OCRConfiguration.values, ocrDefault);

    await _preferences.setString(
        'InsightsConfiguration', EnumToString.convertToString(insights));
    await _preferences.setString('QuestionnaireConfiguration',
        EnumToString.convertToString(questionnaire));
    await _preferences.setString(
        'ParadataConfiguration', EnumToString.convertToString(paradata));
    await _preferences.setString(
        'OCRConfiguration', EnumToString.convertToString(ocr));
    print(
        '>> Configuration saved: insight: $insights, questionnaire: $questionnaire, paradata:$paradata, ocr: $ocr');
    notifyListeners();
  }

  static Configuration of(BuildContext context) =>
      ScopedModel.of<Configuration>(context);
}

extension ToEnum on String {
  T toEnum<T>(List<T> enumValues, T defaultEnum) {
    if (this == null) {
      return defaultEnum;
    }
    final enumValue = EnumToString.fromString(enumValues, this);
    assert(enumValue != null);
    return enumValue ?? defaultEnum;
  }
}
