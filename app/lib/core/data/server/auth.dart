import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class AuthRequestService {
  
  static Map<String, String> getRequestHeaders(String token) {
    return {
      'Authorization': 'Bearer $token',
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    };
  }

  static Future<String> getRequest(Uri url, String body) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('access_token') ?? '';

    final response = await http.get(
      url,
      headers: getRequestHeaders(token),
    );

    if (response.statusCode == 200) {
      print('Response: ${response.body}');
      return jsonDecode(response.body);
    } else {
      throw Exception(
          'Failed to retrieve data, status code: ${response.statusCode}');
    }
  }

  static Future<Response> postRequest(Uri url, String body) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('access_token') ?? '';
    final response = await http.post(
      url,
      headers: getRequestHeaders(token),
      body: body,
    );

    if (response.statusCode == 200) {
      return response;
    } else {
      print(response.body);
      print(response.reasonPhrase);
      throw Exception(
          'Failed to retrieve data, status code: ${response.statusCode}');
    }
  }
}
