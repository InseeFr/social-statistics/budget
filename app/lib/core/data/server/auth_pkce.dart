import 'package:flutter_appauth/flutter_appauth.dart';
import 'package:jose/jose.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthPKCE {
  static FlutterAppAuth _appAuth = const FlutterAppAuth();
  static Future<AuthorizationServiceConfiguration>
      getServiceConfiguration() async {
    final prefs = await SharedPreferences.getInstance();
    final authorizationEndpoint =
        prefs.getString('authorizationEndpoint') ?? '';
    final tokenEndpoint = prefs.getString('tokenEndpoint') ?? '';
    final endSessionEndpoint = prefs.getString('endSessionEndpoint') ?? '';

    return AuthorizationServiceConfiguration(
      authorizationEndpoint: authorizationEndpoint,
      tokenEndpoint: tokenEndpoint,
      endSessionEndpoint: endSessionEndpoint,
    );
  }

  static Future<String> getClientId() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString('authClientId') ?? '';
  }

  static Future<String> getRedirectUrl() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString('redirectUrl') ?? '';
  }

  static Future<String> getIssuer() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString('issuer') ?? '';
  }

  static Future<String> getDiscoveryUrl() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString('discoveryUrl') ?? '';
  }

  static List<String> _scopes = <String>[
    'openid',
    'profile',
    'email',
    'offline_access'
  ];

  static Future<bool> signIn() async {
    final String clientId = await getClientId();
    final String redirectUrl = await getRedirectUrl();
    final List<String> scopes = _scopes;
    final AuthorizationServiceConfiguration serviceConfiguration =
        await getServiceConfiguration();

    final AuthorizationTokenResponse? result =
        await _appAuth.authorizeAndExchangeCode(
      AuthorizationTokenRequest(
        clientId,
        redirectUrl,
        serviceConfiguration: serviceConfiguration,
        scopes: scopes,
      ),
    );
    if (result != null) {
      var userInfo = await getUserInfo(result);
      print('Got token for user: ${userInfo['preferred_username']}');
      _processAuthTokenResponse(result);
      final prefs = await SharedPreferences.getInstance();
      prefs.setString('login_username', userInfo['preferred_username']);
      return true;
    } else {
      throw Exception('No authorization token response');
    }
  }

  static bool isTokenExpired(String token) {
    return JwtDecoder.isExpired(token);
  }

  static DateTime getTokenExpirationDate(String token) {
    return JwtDecoder.getExpirationDate(token);
  }

  static Future<void> _processAuthTokenResponse(
      AuthorizationTokenResponse response) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('access_token', response.accessToken!);
    prefs.setString('refresh_token', response.refreshToken!);
    prefs.setString('offline_token', response.refreshToken!);
    prefs.setInt('expires_in',
        response.accessTokenExpirationDateTime!.millisecondsSinceEpoch);
    prefs.setString('id_token', response.idToken!);
  }

  static Future<Map<String, dynamic>> getUserInfo(
      AuthorizationTokenResponse response) async {
    if (response.idToken == null) {
      throw Exception('No id token found in response');
    }
    var jwt = JsonWebToken.unverified(response.idToken!);

    // Return the claims (user information)
    return jwt.claims.toJson();
  }

  static Future<void> refreshToken() async {
    final prefs = await SharedPreferences.getInstance();
    final _refreshToken = prefs.getString('refresh_token');
    final _clientId = await getClientId();
    final _redirectUrl = await getRedirectUrl();
    final _issuer = await getIssuer();
    final TokenResponse? result = await _appAuth.token(TokenRequest(
        _clientId, _redirectUrl,
        refreshToken: _refreshToken, issuer: _issuer, scopes: _scopes));
    _processTokenResponse(result);
    print(
        ' >> Refreshed token now with expiry: ${result!.accessTokenExpirationDateTime}');
  }

  static Future<void> refreshTokenWithOfflineToken() async {
    final prefs = await SharedPreferences.getInstance();
    final _offlineToken = prefs.getString('offline_token');
    final _clientId = await getClientId();
    final _redirectUrl = await getRedirectUrl();
    final _issuer = await getIssuer();

    final TokenResponse? result = await _appAuth.token(TokenRequest(
        _clientId, _redirectUrl,
        refreshToken: _offlineToken, issuer: _issuer, scopes: _scopes));

    _processTokenResponse(result);
    print(
        ' >> Refreshed token now with expiry: ${result!.accessTokenExpirationDateTime}');
  }

  static Future<void> _processTokenResponse(TokenResponse? response) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('access_token', response!.accessToken!);
    prefs.setString('refresh_token', response.refreshToken!);
    prefs.setInt('expires_in',
        response.accessTokenExpirationDateTime!.millisecondsSinceEpoch);
  }

  static Future<bool> logout() async {
    final prefs = await SharedPreferences.getInstance();
    final _issuer = await getIssuer();
    final _serviceConfiguration = await getServiceConfiguration();
    //await DatabaseHelper.instance.clearDatabase();
    try {
      await _appAuth.endSession(EndSessionRequest(
        issuer: _issuer,
        serviceConfiguration: _serviceConfiguration,
        //postLogoutRedirectUrl: _redirectUrl
        //TODO: token hint
      ));
      prefs.setString('access_token', '');
      prefs.setBool('isAuthenticated', false);
    } catch (err) {
      print(err);
      return false;
    }
    return true;
  }

  static Future<bool> checkAndRefreshToken() async {
    final prefs = await SharedPreferences.getInstance();
    final refresh_token = prefs.getString('refresh_token') ?? '';
    print(' >> Check and refresh token');
    if (refresh_token.length > 0) {
      //bool isTokenExpired = AuthPKCE.isTokenExpired(refresh_token);
      AuthPKCE.refreshToken();
    }
    return true;
  }
}
