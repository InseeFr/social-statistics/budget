import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:bdf_survey/core/state/translations.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;


import 'package:bdf_survey/core/data/server/auth.dart';
import 'package:built_collection/built_collection.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../features/para_data/para_data.dart';
import '../../../features/para_data/para_data_table.dart';
import '../../../screens/search/controller/add_search_suggestion.dart';
import '../database/database_helper.dart';
import '../database/tables/receipt.dart';
import '../database/tables/receipt_product.dart';
import 'builtvalues/sync_image.dart';
import 'builtvalues/sync_paradata_body.dart';
import 'builtvalues/sync_phone.dart';
import 'builtvalues/sync_phone_info.dart';
import 'builtvalues/sync_product.dart';
import 'builtvalues/sync_pull_body.dart';
import 'builtvalues/sync_push_receipt_body.dart';
import 'builtvalues/sync_push_search_product_body.dart';
import 'builtvalues/sync_push_search_store_body.dart';
import 'builtvalues/sync_receipt_data.dart';
import 'builtvalues/sync_register_body.dart';
import 'builtvalues/sync_register_data.dart';
import 'builtvalues/sync_search_product.dart';
import 'builtvalues/sync_search_product_data.dart';
import 'builtvalues/sync_search_store.dart';
import 'builtvalues/sync_search_store_data.dart';
import 'builtvalues/sync_sync.dart';
import 'builtvalues/sync_transaction.dart';
import 'builtvalues/sync_user.dart';
import 'sync_db.dart';

const backendIsActive = true;

class Synchronise {
  static Future<String> getUrlBase() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString('apiUrl') ?? '';
  }

  static Future<Uri> getUrlPushReceipt() async {
    return Uri.parse('${await getUrlBase()}/budget/data/pushreceipt');
  }

static Future<Uri> getUrlPullReceipt() async {
    return Uri.parse('${await getUrlBase()}/budget/data/pullreceipt');
  }

static Future<Uri> getUrlPushProduct() async {
    return Uri.parse('${await getUrlBase()}/budget/data/pushproduct');
  }

static Future<Uri> getUrlPullProduct() async {
    return Uri.parse('${await getUrlBase()}/budget/data/pullproduct');
  }

static Future<Uri> getUrlPushStore() async {
    return Uri.parse('${await getUrlBase()}/budget/data/pushstore');
  }

static Future<Uri> getUrlPullStore() async {
    return Uri.parse('${await getUrlBase()}/budget/data/pullstore');
  }

static Future<Uri> getUrlRegister() async {
    return Uri.parse('${await getUrlBase()}/budget/phone/register');
  }

static Future<Uri> getUrlPushParaData() async {
    return Uri.parse('${await getUrlBase()}/budget/data/pushparadata');
  }

static Future<Uri> getHealthCheck() async {
    return Uri.parse('${await getUrlBase()}/health-check');
  }


static Future<bool> isBackendActive() async {
    final response = await http.get(await getHealthCheck());
    return response.statusCode == 200;
  }



  static Future<void> synchronise() async {
    final connectivityResult = await (Connectivity().checkConnectivity());

    if (connectivityResult == ConnectivityResult.none) {
      debugPrint('No internet connection');
      Fluttertoast.showToast(
          msg:
              'Veuillez vérifier votre connexion internet',
          toastLength: Toast.LENGTH_LONG);
      return;
    }

    final backendIsActive = await isBackendActive();
    if (!backendIsActive) {
      Fluttertoast.showToast(
          msg:
              'Le serveur distant n\'est pas disponible, la synchronisation ne peut pas être effectuée.',
          toastLength: Toast.LENGTH_LONG);
      debugPrint('backend is not active');
      return;
    }
    //debugPrint('--- Synchronise data with distant server ---');
    final _syncDatabase = SyncDatabase();
    final syncId = await _syncDatabase.getSyncId();

    if (syncId.userName != '' && syncId.phoneName != '') {
      final user = SyncUser.newInstance(syncId.userName!, syncId.userName!);
      final phone = SyncPhone.newInstance(syncId.phoneName!);
      //debugPrint('synchronise with user: ${user}');
      await pullAllReceipts(user, phone);
      await pullAllSearchProducts(user, phone);
      await pullAllSearchStores(user, phone);
      await pushAllDataTypes(user, phone); 
      Fluttertoast.showToast(
        msg: Translations.textStatic('syncOK', 'Calendar', null),
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
      );
    } else {
      debugPrint('Synchronise attempt without user or without phone');
    }
  }

  //pull and push all datatypes

  static Future<void> pullAllReceipts(SyncUser user, SyncPhone phone) async {
    var act = true;
    final _syncDatabase = SyncDatabase();
    while (act) {
      var syncOrder = await _syncDatabase.getSyncOrder(dataReceiptType);
      var syncData = await pullOneReceipt(user, phone, syncOrder);

      if (syncData.synchronisation.dataIdentifier == '') {
        act = false;
      } else {
        if (syncData.transaction.receiptLocation != '') {
          final image = imageName(syncData.transaction.receiptLocation);
          final receiptLocation = await imagePath(image);
          syncData = syncData.rebuild((b) => b.transaction = syncData
              .transaction
              .rebuild((b) => b.receiptLocation = receiptLocation)
              .toBuilder());
        }

        syncOrder = syncData.synchronisation.syncOrder!;
        syncData = syncData.rebuild((b) => b.synchronisation = syncData
            .synchronisation
            .rebuild((b) => b.syncOrder = 0)
            .toBuilder());
        final oldSyncSync = await _syncDatabase.getSyncSync(
            dataReceiptType, syncData.synchronisation.dataIdentifier!);
        if (oldSyncSync.dataIdentifier == '') {
          await _syncDatabase.createImportedSyncSync(syncData.synchronisation);
          if (syncData.synchronisation.action != deleted) {
            await ReceiptTable().insertFromBackend(syncData.transaction);
            for (final prod in syncData.products) {
              await ReceiptProductTable().insertFromBackend(prod);
            }
          }
          if (syncData.transaction.receiptLocation != '') {
            final imageBytes = base64.decode(syncData.image.base64image);
            final file = File(syncData.transaction.receiptLocation);
            await file.create(recursive: true);
            await file.writeAsBytes(imageBytes);
          }
        } else {
          if (oldSyncSync.syncTime! < syncData.synchronisation.syncTime!) {
            await _syncDatabase
                .updateImportedSyncSync(syncData.synchronisation);
            // await TransactionDatabase.deleteTransaction(syncData.synchronisation.dataIdentifier);
            if (syncData.synchronisation.action != deleted) {
              await ReceiptTable().insertFromBackend(syncData.transaction);
              for (final prod in syncData.products) {
                await ReceiptProductTable().insertFromBackend(prod);
              }
              if (syncData.transaction.receiptLocation != '') {
                final imageBytes = base64.decode(syncData.image.base64image);
                final file = File(syncData.transaction.receiptLocation);
                await file.writeAsBytes(imageBytes);
              }
            }
          }
        }
        await _syncDatabase.updateSyncOrder(dataReceiptType, syncOrder);
      }
    }
  }

  static Future<void> pullAllSearchProducts(
      SyncUser user, SyncPhone phone) async {
    var act = true;
    final _syncDatabase = SyncDatabase();
    while (act) {
      var syncOrder = await _syncDatabase.getSyncOrder(dataProductType);
      var syncData = await pullOneSearchProduct(user, phone, syncOrder);
      if (syncData.synchronisation.dataIdentifier == '') {
        act = false;
      } else {
        syncOrder = syncData.synchronisation.syncOrder!;
        syncData = syncData.rebuild((b) => b.synchronisation = syncData
            .synchronisation
            .rebuild((b) => b.syncOrder = 0)
            .toBuilder());
        final oldSyncSync = await _syncDatabase.getSyncSync(
            dataProductType, syncData.synchronisation.dataIdentifier!);
        if (oldSyncSync.dataIdentifier == '') {
          await _syncDatabase.createImportedSyncSync(syncData.synchronisation);
          if (syncData.synchronisation.action != deleted) {
            await _syncDatabase.insertSearchProduct(syncData.searchProduct);
          }
        } else {
          if (oldSyncSync.syncTime! < syncData.synchronisation.syncTime!) {
            await _syncDatabase
                .updateImportedSyncSync(syncData.synchronisation);
            await _syncDatabase
                .deleteSearchProduct(syncData.synchronisation.dataIdentifier!);
            if (syncData.synchronisation.action != deleted) {
              await _syncDatabase.insertSearchProduct(syncData.searchProduct);
            }
          }
        }
        await _syncDatabase.updateSyncOrder(dataProductType, syncOrder);
      }
    }
  }

  static Future<void> pullAllSearchStores(
      SyncUser user, SyncPhone phone) async {
    var act = true;
    final _syncDatabase = SyncDatabase();
    while (act) {
      var syncOrder = await _syncDatabase.getSyncOrder(dataStoreType);
      var syncData = await pullOneSearchStore(user, phone, syncOrder);
      if (syncData.synchronisation!.dataIdentifier == '') {
        act = false;
      } else {
        syncOrder = syncData.synchronisation!.syncOrder!;
        syncData = syncData.rebuild((b) => b.synchronisation = syncData
            .synchronisation!
            .rebuild((b) => b.syncOrder = 0)
            .toBuilder());
        final oldSyncSync = await _syncDatabase.getSyncSync(
            dataStoreType, syncData.synchronisation!.dataIdentifier!);
        if (oldSyncSync.dataIdentifier == '') {
          await _syncDatabase.createImportedSyncSync(syncData.synchronisation!);
          if (syncData.synchronisation!.action != deleted) {
            await _syncDatabase.insertSearchStore(syncData.searchStore!);
          }
        } else {
          if (oldSyncSync.syncTime! < syncData.synchronisation!.syncTime!) {
            await _syncDatabase
                .updateImportedSyncSync(syncData.synchronisation!);
            await _syncDatabase
                .deleteSearchStore(syncData.synchronisation!.dataIdentifier!);
            if (syncData.synchronisation!.action != deleted) {
              await _syncDatabase.insertSearchStore(syncData.searchStore!);
            }
          }
        }
        await _syncDatabase.updateSyncOrder(dataStoreType, syncOrder);
      }
    }
  }

  static Future<void> pushAllDataTypes(SyncUser user, SyncPhone phone) async {
    var lastPushed = -1;
    var attempt = 0;

    var act = true;
    var previousParadataPushSucceeded = true;

    final _syncDatabase = SyncDatabase();

    debugPrint('--- pushAllDataTypes to distant server---');

    while (act) {
      final syncRegisterData = await getPhonePushStatus(user, phone);
      final synchronisation = await _syncDatabase
          .getNextSyncSyncToPush(syncRegisterData.phone.syncOrder);

      if (lastPushed != synchronisation.syncOrder) {
        lastPushed = synchronisation.syncOrder!;
        attempt = 1;
      } else if (attempt < 5) {
        attempt = attempt + 1;
        sleep(const Duration(milliseconds: 100));
      } else {
        break;
      }

      if (synchronisation.dataIdentifier == '') {
        act = false;
      } else {
        if (synchronisation.dataType == dataReceiptType) {
          await pushOneReceipt(synchronisation, user, phone);
        } else if (synchronisation.dataType == dataProductType) {
          await pushOneSearchProduct(synchronisation, user, phone);
        } else if (synchronisation.dataType == dataStoreType) {
          await pushOneSearchStore(synchronisation, user, phone);
        } else if (synchronisation.dataType == dataTypeParadata) {
          previousParadataPushSucceeded = false;
        }
      }
    }

    SyncSync syncParadata;
    if (previousParadataPushSucceeded) {
      syncParadata = await _syncDatabase.getFirstSyncSync(dataTypeParadata);
      if (syncParadata.dataIdentifier != '') {
        //delete paradata < syncParadata.syncTime
        final _table = ParaDataTable(await DatabaseHelper.instance.database);
        await _table.delete(syncParadata.syncTime!);
      }
    }
    await _syncDatabase.maintainSyncSync(dataTypeParadata, '001');
    syncParadata = await _syncDatabase.getFirstSyncSync(dataTypeParadata);
    await pushAllParadata(syncParadata, user, phone);
  }

  //push one datatype

  static Future pushOneReceipt(
      SyncSync synchronisation, SyncUser user, SyncPhone phone) async {
    SyncTransaction transaction;
    final products = <SyncProduct>[];
    SyncImage image;
    debugPrint('-- PushOneReceipt --');
    debugPrint('synchronisation: ${synchronisation.dataIdentifier}');
    if (synchronisation.action != deleted) {
      final List<Map> trans =
          await ReceiptTable.syncQuery(synchronisation.dataIdentifier!);

      if (trans.isNotEmpty) {
        transaction =
            SyncTransaction.fromJson(Map<String, dynamic>.from(trans.first));
        final prods = await ReceiptProductTable.syncQuery(
            synchronisation.dataIdentifier!);

        for (final product in prods) {
          products.add(SyncProduct.fromJson(product));
        }

        image = SyncImage((b) => b
          ..transactionID = transaction.transactionID
          ..base64image = '');

        if (transaction.receiptLocation != '') {
          final imageFile = File(transaction.receiptLocation);
          final List<int> imageBytes = imageFile.readAsBytesSync();
          image =
              image.rebuild((b) => b.base64image = base64.encode(imageBytes));
          transaction = transaction.rebuild((b) =>
              b.receiptLocation = imageName(transaction.receiptLocation));
        }
      } else {
        transaction =
            SyncTransaction.newInstance(synchronisation.dataIdentifier!);
        image = SyncImage((b) => b
          ..transactionID = transaction.transactionID
          ..base64image = '');
      }

      final body = SyncPushReceiptBody((b) => b
        ..user = user.toBuilder()
        ..phone = phone.toBuilder()
        ..syncOrder = synchronisation.syncOrder
        ..synchronisation = synchronisation.toBuilder()
        ..transaction = transaction.toBuilder()
        ..products = ListBuilder<SyncProduct>(products)
        ..image = image.toBuilder());
      try {
        final url = await getUrlPushReceipt();
        final response =
            await AuthRequestService.postRequest(
            url, jsonEncode(body));
            
        if (response.statusCode == 200) {
          print('--- PushOneReceipt successful ---');
        } else if (response.statusCode != 200) {
          print(
              'PushOneReceipt Request failed with status: ${response.statusCode}.');
        }
      } catch (e) {
        print(' PushOneReceipt Request failed with error: $e.');
      }
    } else {
      transaction =
          SyncTransaction.newInstance(synchronisation.dataIdentifier!);
      image = SyncImage((b) => b
        ..transactionID = transaction.transactionID
        ..base64image = '');

      final body = SyncPushReceiptBody((b) => b
        ..user = user.toBuilder()
        ..phone = phone.toBuilder()
        ..syncOrder = synchronisation.syncOrder
        ..synchronisation = synchronisation.toBuilder()
        ..transaction = transaction.toBuilder()
        ..products = ListBuilder<SyncProduct>(products)
        ..image = image.toBuilder());
      final url = await getUrlPushReceipt();
      await AuthRequestService.postRequest(url, jsonEncode(body));
    }
  }

  static Future pushOneSearchProduct(
      SyncSync synchronisation, SyncUser user, SyncPhone phone) async {
    SyncSearchProduct searchProduct;
    if (synchronisation.action != deleted) {
      final searchProducts = await SearchSuggestions.querySearchProduct(
          synchronisation.dataIdentifier!);
      searchProduct = SyncSearchProduct.fromJson(searchProducts.first);
    } else {
      searchProduct =
          SyncSearchProduct.newInstance(synchronisation.dataIdentifier!);
    }

    final body = SyncPushSearchProductBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..syncOrder = synchronisation.syncOrder
      ..synchronisation = synchronisation.toBuilder()
      ..searchProduct = searchProduct.toBuilder());
    final url = await getUrlPushProduct();
    await AuthRequestService.postRequest(url, jsonEncode(body));
  }

  static Future pushOneSearchStore(
      SyncSync synchronisation, SyncUser user, SyncPhone phone) async {
    SyncSearchStore searchStore;
    if (synchronisation.action != deleted) {
      final searchStores = await SearchSuggestions.querySearchStore(
          synchronisation.dataIdentifier!);
      searchStore = SyncSearchStore.fromJson(searchStores.first);
    } else {
      searchStore =
          SyncSearchStore.newInstance(synchronisation.dataIdentifier!);
    }

    final body = SyncPushSearchStoreBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..syncOrder = synchronisation.syncOrder
      ..synchronisation = synchronisation.toBuilder()
      ..searchStore = searchStore.toBuilder());
    final url = await getUrlPushStore();
    await AuthRequestService.postRequest(url, jsonEncode(body));
  }

  //pull one datatype

  static Future<SyncReceiptData> pullOneReceipt(
      SyncUser user, SyncPhone phone, int syncOrder) async {
    final body = SyncPullBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..syncOrder = syncOrder);
    final url = await getUrlPullReceipt();
    final response =
        await AuthRequestService.postRequest(url, jsonEncode(body));
    final decodedBody = utf8.decode(response.bodyBytes);
    return SyncReceiptData.fromJson(
        json.decode(decodedBody) as Map<String, dynamic>);
  }

  static Future<SyncSearchProductData> pullOneSearchProduct(
      SyncUser user, SyncPhone phone, int syncOrder) async {
    final body = SyncPullBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..syncOrder = syncOrder);
    final url = await getUrlPullProduct();
    final response =
        await AuthRequestService.postRequest(url, jsonEncode(body));
    final decodedBody = utf8.decode(response.bodyBytes);
    return SyncSearchProductData.fromJson(
        json.decode(decodedBody) as Map<String, dynamic>);
  }

  static Future<SyncSearchStoreData> pullOneSearchStore(
      SyncUser user, SyncPhone phone, int syncOrder) async {
    final body = SyncPullBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..syncOrder = syncOrder);
    final url = await getUrlPullStore();
    final response =
        await AuthRequestService.postRequest(url, jsonEncode(body));
    final decodedBody = utf8.decode(response.bodyBytes);
    Map<String, dynamic> jsonBody =
        jsonDecode(decodedBody) as Map<String, dynamic>;
    jsonBody[r'$'] = 'SyncSearchStoreData';
    return SyncSearchStoreData.fromJson(jsonBody);
  }

  //file path functions

  static String imageName(String path) {
    const prefix = '/Pictures/flutter_test/';
    var i = path.indexOf(prefix);
    if (i < 0) {
      return path;
    } else {
      i = i + prefix.length;
      return path.substring(i);
    }
  }

  static Future<String> imagePath(String name) async {
    final extDir = await getApplicationDocumentsDirectory();
    final dirPath = '${extDir.path}/Pictures/flutter_test';
    await Directory(dirPath).create(recursive: true);
    return '$dirPath/$name';
  }

  //register phone and get phone push status

  static Future<SyncRegisterData> getPhonePushStatus(
      SyncUser user, SyncPhone phone) async {
    final phoneInfos = <SyncPhoneInfo>[];
    final body = SyncRegisterBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..phoneInfos = ListBuilder<SyncPhoneInfo>(phoneInfos));
    final url = await getUrlRegister();
    final response =
        await AuthRequestService.postRequest(url, jsonEncode(body));
    debugPrint('--- GetPhonePushStatus ---');
    final decodedBody = utf8.decode(response.bodyBytes);
    return SyncRegisterData.fromJson(
        json.decode(decodedBody) as Map<String, dynamic>);
  }

  static Future<SyncRegisterData?> registerNewPhone(
      String userName,
      String userPassword,
      String phoneName,
      List<SyncPhoneInfo> phoneInfos) async {
    final user = SyncUser.newInstance(userName, userPassword);
    final phone = SyncPhone.newInstance(phoneName);
    final body = SyncRegisterBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..phoneInfos = ListBuilder<SyncPhoneInfo>(phoneInfos));
    try {
      final url = await getUrlRegister();
      final response =
          await AuthRequestService.postRequest(url, jsonEncode(body))
              .timeout(const Duration(seconds: 5));

      print('--- RegisterNewPhone ---');
      return SyncRegisterData.fromJson(
          json.decode(response.body) as Map<String, dynamic>);
    } catch (e) {
      debugPrint('registerNewPhone error: $e, with body: $body');
      return null;
    }
  }

  // Paradata

  static Future pushAllParadata(
      SyncSync synchronisation, SyncUser user, SyncPhone phone) async {
    final paradatas = <ParaData>[];

    if (synchronisation.action != deleted) {
      final _table = ParaDataTable(await DatabaseHelper.instance.database);
      final _paradatas = await _table.select();

      for (final _paradata in _paradatas) {
        paradatas.add(ParaData.fromJson(_paradata));
      }
    }

    final body = SyncParadataBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..syncOrder = synchronisation.syncOrder
      ..synchronisation = synchronisation.toBuilder()
      ..paradatas = ListBuilder<ParaData>(paradatas));
    final url = await getUrlPushParaData();
    print('--- PushAllParadata ---');
    await AuthRequestService.postRequest(url, jsonEncode(body));
  }
}
