import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';

part 'sync_id.g.dart';

abstract class SyncId implements Built<SyncId, SyncIdBuilder> {
  static Serializer<SyncId> get serializer => _$syncIdSerializer;

  
  String? get userName;

  String? get userPassword;

  String? get phoneName;

  factory SyncId([Function(SyncIdBuilder b) updates]) = _$SyncId;

  SyncId._();

  factory SyncId.newInstance(String userName, String userPassword, String phoneName) {
    return SyncId((b) => b
      ..userName = userName
      ..userPassword = userPassword
      ..phoneName = phoneName);
  }

  factory SyncId.fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(serializer, json)!;
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
