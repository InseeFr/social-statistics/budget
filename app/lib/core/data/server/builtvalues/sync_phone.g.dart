// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_phone.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncPhone> _$syncPhoneSerializer = new _$SyncPhoneSerializer();

class _$SyncPhoneSerializer implements StructuredSerializer<SyncPhone> {
  @override
  final Iterable<Type> types = const [SyncPhone, _$SyncPhone];
  @override
  final String wireName = 'SyncPhone';

  @override
  Iterable<Object?> serialize(Serializers serializers, SyncPhone object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'userId',
      serializers.serialize(object.userId, specifiedType: const FullType(int)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'syncOrder',
      serializers.serialize(object.syncOrder,
          specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  SyncPhone deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncPhoneBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int))! as int;
          break;
        case 'userId':
          result.userId = serializers.deserialize(value,
              specifiedType: const FullType(int))! as int;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'syncOrder':
          result.syncOrder = serializers.deserialize(value,
              specifiedType: const FullType(int))! as int;
          break;
      }
    }

    return result.build();
  }
}

class _$SyncPhone extends SyncPhone {
  @override
  final int id;
  @override
  final int userId;
  @override
  final String name;
  @override
  final int syncOrder;

  factory _$SyncPhone([void Function(SyncPhoneBuilder)? updates]) =>
      (new SyncPhoneBuilder()..update(updates))._build();

  _$SyncPhone._(
      {required this.id,
      required this.userId,
      required this.name,
      required this.syncOrder})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, r'SyncPhone', 'id');
    BuiltValueNullFieldError.checkNotNull(userId, r'SyncPhone', 'userId');
    BuiltValueNullFieldError.checkNotNull(name, r'SyncPhone', 'name');
    BuiltValueNullFieldError.checkNotNull(syncOrder, r'SyncPhone', 'syncOrder');
  }

  @override
  SyncPhone rebuild(void Function(SyncPhoneBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncPhoneBuilder toBuilder() => new SyncPhoneBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncPhone &&
        id == other.id &&
        userId == other.userId &&
        name == other.name &&
        syncOrder == other.syncOrder;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, userId.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, syncOrder.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SyncPhone')
          ..add('id', id)
          ..add('userId', userId)
          ..add('name', name)
          ..add('syncOrder', syncOrder))
        .toString();
  }
}

class SyncPhoneBuilder implements Builder<SyncPhone, SyncPhoneBuilder> {
  _$SyncPhone? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  int? _userId;
  int? get userId => _$this._userId;
  set userId(int? userId) => _$this._userId = userId;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  int? _syncOrder;
  int? get syncOrder => _$this._syncOrder;
  set syncOrder(int? syncOrder) => _$this._syncOrder = syncOrder;

  SyncPhoneBuilder();

  SyncPhoneBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _userId = $v.userId;
      _name = $v.name;
      _syncOrder = $v.syncOrder;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncPhone other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SyncPhone;
  }

  @override
  void update(void Function(SyncPhoneBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SyncPhone build() => _build();

  _$SyncPhone _build() {
    final _$result = _$v ??
        new _$SyncPhone._(
            id: BuiltValueNullFieldError.checkNotNull(id, r'SyncPhone', 'id'),
            userId: BuiltValueNullFieldError.checkNotNull(
                userId, r'SyncPhone', 'userId'),
            name: BuiltValueNullFieldError.checkNotNull(
                name, r'SyncPhone', 'name'),
            syncOrder: BuiltValueNullFieldError.checkNotNull(
                syncOrder, r'SyncPhone', 'syncOrder'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
