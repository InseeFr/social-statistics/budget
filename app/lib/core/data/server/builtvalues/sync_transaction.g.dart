// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_transaction.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncTransaction> _$syncTransactionSerializer =
    new _$SyncTransactionSerializer();

class _$SyncTransactionSerializer
    implements StructuredSerializer<SyncTransaction> {
  @override
  final Iterable<Type> types = const [SyncTransaction, _$SyncTransaction];
  @override
  final String wireName = 'SyncTransaction';

  @override
  Iterable<Object?> serialize(Serializers serializers, SyncTransaction object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'date',
      serializers.serialize(object.date, specifiedType: const FullType(int)),
      'discountAmount',
      serializers.serialize(object.discountAmount,
          specifiedType: const FullType(String)),
      'discountPercentage',
      serializers.serialize(object.discountPercentage,
          specifiedType: const FullType(String)),
      'discountText',
      serializers.serialize(object.discountText,
          specifiedType: const FullType(String)),
      'expenseAbroad',
      serializers.serialize(object.expenseAbroad,
          specifiedType: const FullType(String)),
      'expenseOnline',
      serializers.serialize(object.expenseOnline,
          specifiedType: const FullType(String)),
      'receiptLocation',
      serializers.serialize(object.receiptLocation,
          specifiedType: const FullType(String)),
      'receiptProductType',
      serializers.serialize(object.receiptProductType,
          specifiedType: const FullType(String)),
      'store',
      serializers.serialize(object.store,
          specifiedType: const FullType(String)),
      'storeType',
      serializers.serialize(object.storeType,
          specifiedType: const FullType(String)),
      'totalPrice',
      serializers.serialize(object.totalPrice,
          specifiedType: const FullType(double)),
      'transactionID',
      serializers.serialize(object.transactionID,
          specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.cityName;
    if (value != null) {
      result
        ..add('cityName')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.postalCode;
    if (value != null) {
      result
        ..add('postalCode')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.displayText;
    if (value != null) {
      result
        ..add('displayText')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  SyncTransaction deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncTransactionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'date':
          result.date = serializers.deserialize(value,
              specifiedType: const FullType(int))! as int;
          break;
        case 'discountAmount':
          result.discountAmount = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'discountPercentage':
          result.discountPercentage = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'discountText':
          result.discountText = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'expenseAbroad':
          result.expenseAbroad = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'expenseOnline':
          result.expenseOnline = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'cityName':
          result.cityName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'postalCode':
          result.postalCode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'displayText':
          result.displayText = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'receiptLocation':
          result.receiptLocation = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'receiptProductType':
          result.receiptProductType = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'store':
          result.store = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'storeType':
          result.storeType = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'totalPrice':
          result.totalPrice = serializers.deserialize(value,
              specifiedType: const FullType(double))! as double;
          break;
        case 'transactionID':
          result.transactionID = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
      }
    }

    return result.build();
  }
}

class _$SyncTransaction extends SyncTransaction {
  @override
  final int date;
  @override
  final String discountAmount;
  @override
  final String discountPercentage;
  @override
  final String discountText;
  @override
  final String expenseAbroad;
  @override
  final String expenseOnline;
  @override
  final String? cityName;
  @override
  final String? postalCode;
  @override
  final String? displayText;
  @override
  final String receiptLocation;
  @override
  final String receiptProductType;
  @override
  final String store;
  @override
  final String storeType;
  @override
  final double totalPrice;
  @override
  final String transactionID;

  factory _$SyncTransaction([void Function(SyncTransactionBuilder)? updates]) =>
      (new SyncTransactionBuilder()..update(updates))._build();

  _$SyncTransaction._(
      {required this.date,
      required this.discountAmount,
      required this.discountPercentage,
      required this.discountText,
      required this.expenseAbroad,
      required this.expenseOnline,
      this.cityName,
      this.postalCode,
      this.displayText,
      required this.receiptLocation,
      required this.receiptProductType,
      required this.store,
      required this.storeType,
      required this.totalPrice,
      required this.transactionID})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(date, r'SyncTransaction', 'date');
    BuiltValueNullFieldError.checkNotNull(
        discountAmount, r'SyncTransaction', 'discountAmount');
    BuiltValueNullFieldError.checkNotNull(
        discountPercentage, r'SyncTransaction', 'discountPercentage');
    BuiltValueNullFieldError.checkNotNull(
        discountText, r'SyncTransaction', 'discountText');
    BuiltValueNullFieldError.checkNotNull(
        expenseAbroad, r'SyncTransaction', 'expenseAbroad');
    BuiltValueNullFieldError.checkNotNull(
        expenseOnline, r'SyncTransaction', 'expenseOnline');
    BuiltValueNullFieldError.checkNotNull(
        receiptLocation, r'SyncTransaction', 'receiptLocation');
    BuiltValueNullFieldError.checkNotNull(
        receiptProductType, r'SyncTransaction', 'receiptProductType');
    BuiltValueNullFieldError.checkNotNull(store, r'SyncTransaction', 'store');
    BuiltValueNullFieldError.checkNotNull(
        storeType, r'SyncTransaction', 'storeType');
    BuiltValueNullFieldError.checkNotNull(
        totalPrice, r'SyncTransaction', 'totalPrice');
    BuiltValueNullFieldError.checkNotNull(
        transactionID, r'SyncTransaction', 'transactionID');
  }

  @override
  SyncTransaction rebuild(void Function(SyncTransactionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncTransactionBuilder toBuilder() =>
      new SyncTransactionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncTransaction &&
        date == other.date &&
        discountAmount == other.discountAmount &&
        discountPercentage == other.discountPercentage &&
        discountText == other.discountText &&
        expenseAbroad == other.expenseAbroad &&
        expenseOnline == other.expenseOnline &&
        cityName == other.cityName &&
        postalCode == other.postalCode &&
        displayText == other.displayText &&
        receiptLocation == other.receiptLocation &&
        receiptProductType == other.receiptProductType &&
        store == other.store &&
        storeType == other.storeType &&
        totalPrice == other.totalPrice &&
        transactionID == other.transactionID;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, date.hashCode);
    _$hash = $jc(_$hash, discountAmount.hashCode);
    _$hash = $jc(_$hash, discountPercentage.hashCode);
    _$hash = $jc(_$hash, discountText.hashCode);
    _$hash = $jc(_$hash, expenseAbroad.hashCode);
    _$hash = $jc(_$hash, expenseOnline.hashCode);
    _$hash = $jc(_$hash, cityName.hashCode);
    _$hash = $jc(_$hash, postalCode.hashCode);
    _$hash = $jc(_$hash, displayText.hashCode);
    _$hash = $jc(_$hash, receiptLocation.hashCode);
    _$hash = $jc(_$hash, receiptProductType.hashCode);
    _$hash = $jc(_$hash, store.hashCode);
    _$hash = $jc(_$hash, storeType.hashCode);
    _$hash = $jc(_$hash, totalPrice.hashCode);
    _$hash = $jc(_$hash, transactionID.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SyncTransaction')
          ..add('date', date)
          ..add('discountAmount', discountAmount)
          ..add('discountPercentage', discountPercentage)
          ..add('discountText', discountText)
          ..add('expenseAbroad', expenseAbroad)
          ..add('expenseOnline', expenseOnline)
          ..add('cityName', cityName)
          ..add('postalCode', postalCode)
          ..add('displayText', displayText)
          ..add('receiptLocation', receiptLocation)
          ..add('receiptProductType', receiptProductType)
          ..add('store', store)
          ..add('storeType', storeType)
          ..add('totalPrice', totalPrice)
          ..add('transactionID', transactionID))
        .toString();
  }
}

class SyncTransactionBuilder
    implements Builder<SyncTransaction, SyncTransactionBuilder> {
  _$SyncTransaction? _$v;

  int? _date;
  int? get date => _$this._date;
  set date(int? date) => _$this._date = date;

  String? _discountAmount;
  String? get discountAmount => _$this._discountAmount;
  set discountAmount(String? discountAmount) =>
      _$this._discountAmount = discountAmount;

  String? _discountPercentage;
  String? get discountPercentage => _$this._discountPercentage;
  set discountPercentage(String? discountPercentage) =>
      _$this._discountPercentage = discountPercentage;

  String? _discountText;
  String? get discountText => _$this._discountText;
  set discountText(String? discountText) => _$this._discountText = discountText;

  String? _expenseAbroad;
  String? get expenseAbroad => _$this._expenseAbroad;
  set expenseAbroad(String? expenseAbroad) =>
      _$this._expenseAbroad = expenseAbroad;

  String? _expenseOnline;
  String? get expenseOnline => _$this._expenseOnline;
  set expenseOnline(String? expenseOnline) =>
      _$this._expenseOnline = expenseOnline;

  String? _cityName;
  String? get cityName => _$this._cityName;
  set cityName(String? cityName) => _$this._cityName = cityName;

  String? _postalCode;
  String? get postalCode => _$this._postalCode;
  set postalCode(String? postalCode) => _$this._postalCode = postalCode;

  String? _displayText;
  String? get displayText => _$this._displayText;
  set displayText(String? displayText) => _$this._displayText = displayText;

  String? _receiptLocation;
  String? get receiptLocation => _$this._receiptLocation;
  set receiptLocation(String? receiptLocation) =>
      _$this._receiptLocation = receiptLocation;

  String? _receiptProductType;
  String? get receiptProductType => _$this._receiptProductType;
  set receiptProductType(String? receiptProductType) =>
      _$this._receiptProductType = receiptProductType;

  String? _store;
  String? get store => _$this._store;
  set store(String? store) => _$this._store = store;

  String? _storeType;
  String? get storeType => _$this._storeType;
  set storeType(String? storeType) => _$this._storeType = storeType;

  double? _totalPrice;
  double? get totalPrice => _$this._totalPrice;
  set totalPrice(double? totalPrice) => _$this._totalPrice = totalPrice;

  String? _transactionID;
  String? get transactionID => _$this._transactionID;
  set transactionID(String? transactionID) =>
      _$this._transactionID = transactionID;

  SyncTransactionBuilder();

  SyncTransactionBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _date = $v.date;
      _discountAmount = $v.discountAmount;
      _discountPercentage = $v.discountPercentage;
      _discountText = $v.discountText;
      _expenseAbroad = $v.expenseAbroad;
      _expenseOnline = $v.expenseOnline;
      _cityName = $v.cityName;
      _postalCode = $v.postalCode;
      _displayText = $v.displayText;
      _receiptLocation = $v.receiptLocation;
      _receiptProductType = $v.receiptProductType;
      _store = $v.store;
      _storeType = $v.storeType;
      _totalPrice = $v.totalPrice;
      _transactionID = $v.transactionID;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncTransaction other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SyncTransaction;
  }

  @override
  void update(void Function(SyncTransactionBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SyncTransaction build() => _build();

  _$SyncTransaction _build() {
    final _$result = _$v ??
        new _$SyncTransaction._(
            date: BuiltValueNullFieldError.checkNotNull(
                date, r'SyncTransaction', 'date'),
            discountAmount: BuiltValueNullFieldError.checkNotNull(
                discountAmount, r'SyncTransaction', 'discountAmount'),
            discountPercentage: BuiltValueNullFieldError.checkNotNull(
                discountPercentage, r'SyncTransaction', 'discountPercentage'),
            discountText: BuiltValueNullFieldError.checkNotNull(
                discountText, r'SyncTransaction', 'discountText'),
            expenseAbroad: BuiltValueNullFieldError.checkNotNull(
                expenseAbroad, r'SyncTransaction', 'expenseAbroad'),
            expenseOnline: BuiltValueNullFieldError.checkNotNull(
                expenseOnline, r'SyncTransaction', 'expenseOnline'),
            cityName: cityName,
            postalCode: postalCode,
            displayText: displayText,
            receiptLocation: BuiltValueNullFieldError.checkNotNull(
                receiptLocation, r'SyncTransaction', 'receiptLocation'),
            receiptProductType:
                BuiltValueNullFieldError.checkNotNull(receiptProductType, r'SyncTransaction', 'receiptProductType'),
            store: BuiltValueNullFieldError.checkNotNull(store, r'SyncTransaction', 'store'),
            storeType: BuiltValueNullFieldError.checkNotNull(storeType, r'SyncTransaction', 'storeType'),
            totalPrice: BuiltValueNullFieldError.checkNotNull(totalPrice, r'SyncTransaction', 'totalPrice'),
            transactionID: BuiltValueNullFieldError.checkNotNull(transactionID, r'SyncTransaction', 'transactionID'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
