// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'serializers.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(ParaData.serializer)
      ..add(SyncGroupInfo.serializer)
      ..add(SyncId.serializer)
      ..add(SyncImage.serializer)
      ..add(SyncParadataBody.serializer)
      ..add(SyncParadataData.serializer)
      ..add(SyncPhone.serializer)
      ..add(SyncPhoneInfo.serializer)
      ..add(SyncProduct.serializer)
      ..add(SyncPullBody.serializer)
      ..add(SyncPushReceiptBody.serializer)
      ..add(SyncPushSearchProductBody.serializer)
      ..add(SyncPushSearchStoreBody.serializer)
      ..add(SyncReceiptData.serializer)
      ..add(SyncRegisterBody.serializer)
      ..add(SyncRegisterData.serializer)
      ..add(SyncSearchProduct.serializer)
      ..add(SyncSearchProductData.serializer)
      ..add(SyncSearchStore.serializer)
      ..add(SyncSearchStoreData.serializer)
      ..add(SyncSync.serializer)
      ..add(SyncTransaction.serializer)
      ..add(SyncUser.serializer)
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ParaData)]),
          () => new ListBuilder<ParaData>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ParaData)]),
          () => new ListBuilder<ParaData>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SyncGroupInfo)]),
          () => new ListBuilder<SyncGroupInfo>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SyncPhoneInfo)]),
          () => new ListBuilder<SyncPhoneInfo>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SyncProduct)]),
          () => new ListBuilder<SyncProduct>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SyncProduct)]),
          () => new ListBuilder<SyncProduct>()))
    .build();

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
