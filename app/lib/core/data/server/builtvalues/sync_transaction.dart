import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';

part 'sync_transaction.g.dart';

//Nullable fields are temporary and will be removed once the server is updated
abstract class SyncTransaction
    implements Built<SyncTransaction, SyncTransactionBuilder> {
  static Serializer<SyncTransaction> get serializer =>
      _$syncTransactionSerializer;

  int get date;
  String get discountAmount;
  String get discountPercentage;
  String get discountText;
  String get expenseAbroad;
  String get expenseOnline;
  String? get cityName;
  String? get postalCode;
  String? get displayText;
  String get receiptLocation;
  String get receiptProductType;
  String get store;
  String get storeType;
  double get totalPrice;
  String get transactionID;
  // String get expenseSecondhand;

  factory SyncTransaction([Function(SyncTransactionBuilder b) updates]) =
      _$SyncTransaction;

  SyncTransaction._();

  factory SyncTransaction.newInstance(String transactionId) {
    return SyncTransaction((b) => b
      ..date = 0
      ..discountAmount = ''
      ..discountPercentage = ''
      ..discountText = ''
      ..expenseAbroad = ''
      ..expenseOnline = ''
      ..cityName = ''
      ..postalCode = ''
      ..displayText = ''
      ..receiptLocation = ''
      ..receiptProductType = ''
      ..store = ''
      ..storeType = ''
      ..totalPrice = 0
      ..transactionID = transactionId);
  }

  factory SyncTransaction.fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(serializer, json)!;
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
