// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_search_product_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncSearchProductData> _$syncSearchProductDataSerializer =
    new _$SyncSearchProductDataSerializer();

class _$SyncSearchProductDataSerializer
    implements StructuredSerializer<SyncSearchProductData> {
  @override
  final Iterable<Type> types = const [
    SyncSearchProductData,
    _$SyncSearchProductData
  ];
  @override
  final String wireName = 'SyncSearchProductData';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, SyncSearchProductData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'synchronisation',
      serializers.serialize(object.synchronisation,
          specifiedType: const FullType(SyncSync)),
      'searchProduct',
      serializers.serialize(object.searchProduct,
          specifiedType: const FullType(SyncSearchProduct)),
    ];

    return result;
  }

  @override
  SyncSearchProductData deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncSearchProductDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'synchronisation':
          result.synchronisation.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncSync))! as SyncSync);
          break;
        case 'searchProduct':
          result.searchProduct.replace(serializers.deserialize(value,
                  specifiedType: const FullType(SyncSearchProduct))!
              as SyncSearchProduct);
          break;
      }
    }

    return result.build();
  }
}

class _$SyncSearchProductData extends SyncSearchProductData {
  @override
  final SyncSync synchronisation;
  @override
  final SyncSearchProduct searchProduct;

  factory _$SyncSearchProductData(
          [void Function(SyncSearchProductDataBuilder)? updates]) =>
      (new SyncSearchProductDataBuilder()..update(updates))._build();

  _$SyncSearchProductData._(
      {required this.synchronisation, required this.searchProduct})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        synchronisation, r'SyncSearchProductData', 'synchronisation');
    BuiltValueNullFieldError.checkNotNull(
        searchProduct, r'SyncSearchProductData', 'searchProduct');
  }

  @override
  SyncSearchProductData rebuild(
          void Function(SyncSearchProductDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncSearchProductDataBuilder toBuilder() =>
      new SyncSearchProductDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncSearchProductData &&
        synchronisation == other.synchronisation &&
        searchProduct == other.searchProduct;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, synchronisation.hashCode);
    _$hash = $jc(_$hash, searchProduct.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SyncSearchProductData')
          ..add('synchronisation', synchronisation)
          ..add('searchProduct', searchProduct))
        .toString();
  }
}

class SyncSearchProductDataBuilder
    implements Builder<SyncSearchProductData, SyncSearchProductDataBuilder> {
  _$SyncSearchProductData? _$v;

  SyncSyncBuilder? _synchronisation;
  SyncSyncBuilder get synchronisation =>
      _$this._synchronisation ??= new SyncSyncBuilder();
  set synchronisation(SyncSyncBuilder? synchronisation) =>
      _$this._synchronisation = synchronisation;

  SyncSearchProductBuilder? _searchProduct;
  SyncSearchProductBuilder get searchProduct =>
      _$this._searchProduct ??= new SyncSearchProductBuilder();
  set searchProduct(SyncSearchProductBuilder? searchProduct) =>
      _$this._searchProduct = searchProduct;

  SyncSearchProductDataBuilder();

  SyncSearchProductDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _synchronisation = $v.synchronisation.toBuilder();
      _searchProduct = $v.searchProduct.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncSearchProductData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SyncSearchProductData;
  }

  @override
  void update(void Function(SyncSearchProductDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SyncSearchProductData build() => _build();

  _$SyncSearchProductData _build() {
    _$SyncSearchProductData _$result;
    try {
      _$result = _$v ??
          new _$SyncSearchProductData._(
              synchronisation: synchronisation.build(),
              searchProduct: searchProduct.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'synchronisation';
        synchronisation.build();
        _$failedField = 'searchProduct';
        searchProduct.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'SyncSearchProductData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
