// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_image.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncImage> _$syncImageSerializer = new _$SyncImageSerializer();

class _$SyncImageSerializer implements StructuredSerializer<SyncImage> {
  @override
  final Iterable<Type> types = const [SyncImage, _$SyncImage];
  @override
  final String wireName = 'SyncImage';

  @override
  Iterable<Object?> serialize(Serializers serializers, SyncImage object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'base64image',
      serializers.serialize(object.base64image,
          specifiedType: const FullType(String)),
      'transactionID',
      serializers.serialize(object.transactionID,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  SyncImage deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncImageBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'base64image':
          result.base64image = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'transactionID':
          result.transactionID = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
      }
    }

    return result.build();
  }
}

class _$SyncImage extends SyncImage {
  @override
  final String base64image;
  @override
  final String transactionID;

  factory _$SyncImage([void Function(SyncImageBuilder)? updates]) =>
      (new SyncImageBuilder()..update(updates))._build();

  _$SyncImage._({required this.base64image, required this.transactionID})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        base64image, r'SyncImage', 'base64image');
    BuiltValueNullFieldError.checkNotNull(
        transactionID, r'SyncImage', 'transactionID');
  }

  @override
  SyncImage rebuild(void Function(SyncImageBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncImageBuilder toBuilder() => new SyncImageBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncImage &&
        base64image == other.base64image &&
        transactionID == other.transactionID;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, base64image.hashCode);
    _$hash = $jc(_$hash, transactionID.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SyncImage')
          ..add('base64image', base64image)
          ..add('transactionID', transactionID))
        .toString();
  }
}

class SyncImageBuilder implements Builder<SyncImage, SyncImageBuilder> {
  _$SyncImage? _$v;

  String? _base64image;
  String? get base64image => _$this._base64image;
  set base64image(String? base64image) => _$this._base64image = base64image;

  String? _transactionID;
  String? get transactionID => _$this._transactionID;
  set transactionID(String? transactionID) =>
      _$this._transactionID = transactionID;

  SyncImageBuilder();

  SyncImageBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _base64image = $v.base64image;
      _transactionID = $v.transactionID;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncImage other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SyncImage;
  }

  @override
  void update(void Function(SyncImageBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SyncImage build() => _build();

  _$SyncImage _build() {
    final _$result = _$v ??
        new _$SyncImage._(
            base64image: BuiltValueNullFieldError.checkNotNull(
                base64image, r'SyncImage', 'base64image'),
            transactionID: BuiltValueNullFieldError.checkNotNull(
                transactionID, r'SyncImage', 'transactionID'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
