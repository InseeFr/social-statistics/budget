// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_id.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncId> _$syncIdSerializer = new _$SyncIdSerializer();

class _$SyncIdSerializer implements StructuredSerializer<SyncId> {
  @override
  final Iterable<Type> types = const [SyncId, _$SyncId];
  @override
  final String wireName = 'SyncId';

  @override
  Iterable<Object?> serialize(Serializers serializers, SyncId object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.userName;
    if (value != null) {
      result
        ..add('userName')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.userPassword;
    if (value != null) {
      result
        ..add('userPassword')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.phoneName;
    if (value != null) {
      result
        ..add('phoneName')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  SyncId deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncIdBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'userName':
          result.userName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'userPassword':
          result.userPassword = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'phoneName':
          result.phoneName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$SyncId extends SyncId {
  @override
  final String? userName;
  @override
  final String? userPassword;
  @override
  final String? phoneName;

  factory _$SyncId([void Function(SyncIdBuilder)? updates]) =>
      (new SyncIdBuilder()..update(updates))._build();

  _$SyncId._({this.userName, this.userPassword, this.phoneName}) : super._();

  @override
  SyncId rebuild(void Function(SyncIdBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncIdBuilder toBuilder() => new SyncIdBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncId &&
        userName == other.userName &&
        userPassword == other.userPassword &&
        phoneName == other.phoneName;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, userName.hashCode);
    _$hash = $jc(_$hash, userPassword.hashCode);
    _$hash = $jc(_$hash, phoneName.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SyncId')
          ..add('userName', userName)
          ..add('userPassword', userPassword)
          ..add('phoneName', phoneName))
        .toString();
  }
}

class SyncIdBuilder implements Builder<SyncId, SyncIdBuilder> {
  _$SyncId? _$v;

  String? _userName;
  String? get userName => _$this._userName;
  set userName(String? userName) => _$this._userName = userName;

  String? _userPassword;
  String? get userPassword => _$this._userPassword;
  set userPassword(String? userPassword) => _$this._userPassword = userPassword;

  String? _phoneName;
  String? get phoneName => _$this._phoneName;
  set phoneName(String? phoneName) => _$this._phoneName = phoneName;

  SyncIdBuilder();

  SyncIdBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _userName = $v.userName;
      _userPassword = $v.userPassword;
      _phoneName = $v.phoneName;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncId other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SyncId;
  }

  @override
  void update(void Function(SyncIdBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SyncId build() => _build();

  _$SyncId _build() {
    final _$result = _$v ??
        new _$SyncId._(
            userName: userName,
            userPassword: userPassword,
            phoneName: phoneName);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
