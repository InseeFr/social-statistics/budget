// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_search_store.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncSearchStore> _$syncSearchStoreSerializer =
    new _$SyncSearchStoreSerializer();

class _$SyncSearchStoreSerializer
    implements StructuredSerializer<SyncSearchStore> {
  @override
  final Iterable<Type> types = const [SyncSearchStore, _$SyncSearchStore];
  @override
  final String wireName = 'SyncSearchStore';

  @override
  Iterable<Object?> serialize(Serializers serializers, SyncSearchStore object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.storeName;
    if (value != null) {
      result
        ..add('storeName')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.storeType;
    if (value != null) {
      result
        ..add('storeType')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.lastAdded;
    if (value != null) {
      result
        ..add('lastAdded')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.count;
    if (value != null) {
      result
        ..add('count')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  SyncSearchStore deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncSearchStoreBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'storeName':
          result.storeName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'storeType':
          result.storeType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'lastAdded':
          result.lastAdded = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'count':
          result.count = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
      }
    }

    return result.build();
  }
}

class _$SyncSearchStore extends SyncSearchStore {
  @override
  final String? storeName;
  @override
  final String? storeType;
  @override
  final int? lastAdded;
  @override
  final int? count;

  factory _$SyncSearchStore([void Function(SyncSearchStoreBuilder)? updates]) =>
      (new SyncSearchStoreBuilder()..update(updates))._build();

  _$SyncSearchStore._(
      {this.storeName, this.storeType, this.lastAdded, this.count})
      : super._();

  @override
  SyncSearchStore rebuild(void Function(SyncSearchStoreBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncSearchStoreBuilder toBuilder() =>
      new SyncSearchStoreBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncSearchStore &&
        storeName == other.storeName &&
        storeType == other.storeType &&
        lastAdded == other.lastAdded &&
        count == other.count;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, storeName.hashCode);
    _$hash = $jc(_$hash, storeType.hashCode);
    _$hash = $jc(_$hash, lastAdded.hashCode);
    _$hash = $jc(_$hash, count.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SyncSearchStore')
          ..add('storeName', storeName)
          ..add('storeType', storeType)
          ..add('lastAdded', lastAdded)
          ..add('count', count))
        .toString();
  }
}

class SyncSearchStoreBuilder
    implements Builder<SyncSearchStore, SyncSearchStoreBuilder> {
  _$SyncSearchStore? _$v;

  String? _storeName;
  String? get storeName => _$this._storeName;
  set storeName(String? storeName) => _$this._storeName = storeName;

  String? _storeType;
  String? get storeType => _$this._storeType;
  set storeType(String? storeType) => _$this._storeType = storeType;

  int? _lastAdded;
  int? get lastAdded => _$this._lastAdded;
  set lastAdded(int? lastAdded) => _$this._lastAdded = lastAdded;

  int? _count;
  int? get count => _$this._count;
  set count(int? count) => _$this._count = count;

  SyncSearchStoreBuilder();

  SyncSearchStoreBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _storeName = $v.storeName;
      _storeType = $v.storeType;
      _lastAdded = $v.lastAdded;
      _count = $v.count;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncSearchStore other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SyncSearchStore;
  }

  @override
  void update(void Function(SyncSearchStoreBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SyncSearchStore build() => _build();

  _$SyncSearchStore _build() {
    final _$result = _$v ??
        new _$SyncSearchStore._(
            storeName: storeName,
            storeType: storeType,
            lastAdded: lastAdded,
            count: count);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
