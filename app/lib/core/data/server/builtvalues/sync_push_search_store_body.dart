import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';
import 'sync_phone.dart';
import 'sync_search_store.dart';
import 'sync_sync.dart';
import 'sync_user.dart';

part 'sync_push_search_store_body.g.dart';

abstract class SyncPushSearchStoreBody implements Built<SyncPushSearchStoreBody, SyncPushSearchStoreBodyBuilder> {
  static Serializer<SyncPushSearchStoreBody> get serializer => _$syncPushSearchStoreBodySerializer;

  SyncUser get user;
  SyncPhone get phone;
  int get syncOrder;
  SyncSync get synchronisation;
  SyncSearchStore get searchStore;

  factory SyncPushSearchStoreBody([Function(SyncPushSearchStoreBodyBuilder b) updates]) = _$SyncPushSearchStoreBody;

  SyncPushSearchStoreBody._();

  factory SyncPushSearchStoreBody.fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(serializer, json)!;
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
