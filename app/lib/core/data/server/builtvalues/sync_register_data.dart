import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';
import 'sync_group_info.dart';
import 'sync_phone.dart';
import 'sync_user.dart';

part 'sync_register_data.g.dart';

abstract class SyncRegisterData implements Built<SyncRegisterData, SyncRegisterDataBuilder> {
  static Serializer<SyncRegisterData> get serializer => _$syncRegisterDataSerializer;

  SyncUser get user;
  SyncPhone get phone;
  BuiltList<SyncGroupInfo> get groupInfos;

  factory SyncRegisterData([Function(SyncRegisterDataBuilder b) updates]) = _$SyncRegisterData;

  SyncRegisterData._();

  factory SyncRegisterData.fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(serializer, json)!;
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
