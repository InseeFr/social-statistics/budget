import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';

part 'sync_sync.g.dart';

abstract class SyncSync implements Built<SyncSync, SyncSyncBuilder> {
  static Serializer<SyncSync> get serializer => _$syncSyncSerializer;

  
  int? get dataType;
  
  String? get dataIdentifier;
  
  int? get syncOrder;
  
  int? get syncTime;
  
  int? get action;

  factory SyncSync([Function(SyncSyncBuilder b) updates]) = _$SyncSync;

  SyncSync._();

  factory SyncSync.newInstance() {
    return SyncSync((b) => b
      ..dataType = 0
      ..dataIdentifier = ''
      ..syncOrder = 0
      ..syncTime = 0
      ..action = 0);
  }

  factory SyncSync.fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(serializer, json)!;
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
