// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_search_store_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncSearchStoreData> _$syncSearchStoreDataSerializer =
    new _$SyncSearchStoreDataSerializer();

class _$SyncSearchStoreDataSerializer
    implements StructuredSerializer<SyncSearchStoreData> {
  @override
  final Iterable<Type> types = const [
    SyncSearchStoreData,
    _$SyncSearchStoreData
  ];
  @override
  final String wireName = 'SyncSearchStoreData';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, SyncSearchStoreData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.synchronisation;
    if (value != null) {
      result
        ..add('synchronisation')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(SyncSync)));
    }
    value = object.searchStore;
    if (value != null) {
      result
        ..add('searchStore')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(SyncSearchStore)));
    }
    return result;
  }

  @override
  SyncSearchStoreData deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncSearchStoreDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'synchronisation':
          result.synchronisation.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncSync))! as SyncSync);
          break;
        case 'searchStore':
          result.searchStore.replace(serializers.deserialize(value,
                  specifiedType: const FullType(SyncSearchStore))!
              as SyncSearchStore);
          break;
      }
    }

    return result.build();
  }
}

class _$SyncSearchStoreData extends SyncSearchStoreData {
  @override
  final SyncSync? synchronisation;
  @override
  final SyncSearchStore? searchStore;

  factory _$SyncSearchStoreData(
          [void Function(SyncSearchStoreDataBuilder)? updates]) =>
      (new SyncSearchStoreDataBuilder()..update(updates))._build();

  _$SyncSearchStoreData._({this.synchronisation, this.searchStore}) : super._();

  @override
  SyncSearchStoreData rebuild(
          void Function(SyncSearchStoreDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncSearchStoreDataBuilder toBuilder() =>
      new SyncSearchStoreDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncSearchStoreData &&
        synchronisation == other.synchronisation &&
        searchStore == other.searchStore;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, synchronisation.hashCode);
    _$hash = $jc(_$hash, searchStore.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SyncSearchStoreData')
          ..add('synchronisation', synchronisation)
          ..add('searchStore', searchStore))
        .toString();
  }
}

class SyncSearchStoreDataBuilder
    implements Builder<SyncSearchStoreData, SyncSearchStoreDataBuilder> {
  _$SyncSearchStoreData? _$v;

  SyncSyncBuilder? _synchronisation;
  SyncSyncBuilder get synchronisation =>
      _$this._synchronisation ??= new SyncSyncBuilder();
  set synchronisation(SyncSyncBuilder? synchronisation) =>
      _$this._synchronisation = synchronisation;

  SyncSearchStoreBuilder? _searchStore;
  SyncSearchStoreBuilder get searchStore =>
      _$this._searchStore ??= new SyncSearchStoreBuilder();
  set searchStore(SyncSearchStoreBuilder? searchStore) =>
      _$this._searchStore = searchStore;

  SyncSearchStoreDataBuilder();

  SyncSearchStoreDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _synchronisation = $v.synchronisation?.toBuilder();
      _searchStore = $v.searchStore?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncSearchStoreData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SyncSearchStoreData;
  }

  @override
  void update(void Function(SyncSearchStoreDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SyncSearchStoreData build() => _build();

  _$SyncSearchStoreData _build() {
    _$SyncSearchStoreData _$result;
    try {
      _$result = _$v ??
          new _$SyncSearchStoreData._(
              synchronisation: _synchronisation?.build(),
              searchStore: _searchStore?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'synchronisation';
        _synchronisation?.build();
        _$failedField = 'searchStore';
        _searchStore?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'SyncSearchStoreData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
