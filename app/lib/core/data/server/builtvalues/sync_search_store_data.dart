import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';
import 'sync_search_store.dart';
import 'sync_sync.dart';

part 'sync_search_store_data.g.dart';

abstract class SyncSearchStoreData
    implements Built<SyncSearchStoreData, SyncSearchStoreDataBuilder> {
      
  static Serializer<SyncSearchStoreData> get serializer =>
      _$syncSearchStoreDataSerializer;

  @BuiltValueField(wireName: 'synchronisation')
  SyncSync? get synchronisation;

  @BuiltValueField(wireName: 'searchStore')
  SyncSearchStore? get searchStore;

  factory SyncSearchStoreData(
      [Function(SyncSearchStoreDataBuilder b) updates]) = _$SyncSearchStoreData;

  SyncSearchStoreData._();

  factory SyncSearchStoreData.fromJson(Map<String, dynamic> json) {
    return serializers.deserialize(json,
            specifiedType: const FullType(SyncSearchStoreData))
        as SyncSearchStoreData;
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this)! as Map<String, dynamic>;
  }
}
