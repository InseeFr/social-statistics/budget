// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_pull_body.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncPullBody> _$syncPullBodySerializer =
    new _$SyncPullBodySerializer();

class _$SyncPullBodySerializer implements StructuredSerializer<SyncPullBody> {
  @override
  final Iterable<Type> types = const [SyncPullBody, _$SyncPullBody];
  @override
  final String wireName = 'SyncPullBody';

  @override
  Iterable<Object?> serialize(Serializers serializers, SyncPullBody object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'user',
      serializers.serialize(object.user,
          specifiedType: const FullType(SyncUser)),
      'phone',
      serializers.serialize(object.phone,
          specifiedType: const FullType(SyncPhone)),
      'syncOrder',
      serializers.serialize(object.syncOrder,
          specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  SyncPullBody deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncPullBodyBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'user':
          result.user.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncUser))! as SyncUser);
          break;
        case 'phone':
          result.phone.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncPhone))! as SyncPhone);
          break;
        case 'syncOrder':
          result.syncOrder = serializers.deserialize(value,
              specifiedType: const FullType(int))! as int;
          break;
      }
    }

    return result.build();
  }
}

class _$SyncPullBody extends SyncPullBody {
  @override
  final SyncUser user;
  @override
  final SyncPhone phone;
  @override
  final int syncOrder;

  factory _$SyncPullBody([void Function(SyncPullBodyBuilder)? updates]) =>
      (new SyncPullBodyBuilder()..update(updates))._build();

  _$SyncPullBody._(
      {required this.user, required this.phone, required this.syncOrder})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(user, r'SyncPullBody', 'user');
    BuiltValueNullFieldError.checkNotNull(phone, r'SyncPullBody', 'phone');
    BuiltValueNullFieldError.checkNotNull(
        syncOrder, r'SyncPullBody', 'syncOrder');
  }

  @override
  SyncPullBody rebuild(void Function(SyncPullBodyBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncPullBodyBuilder toBuilder() => new SyncPullBodyBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncPullBody &&
        user == other.user &&
        phone == other.phone &&
        syncOrder == other.syncOrder;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, user.hashCode);
    _$hash = $jc(_$hash, phone.hashCode);
    _$hash = $jc(_$hash, syncOrder.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SyncPullBody')
          ..add('user', user)
          ..add('phone', phone)
          ..add('syncOrder', syncOrder))
        .toString();
  }
}

class SyncPullBodyBuilder
    implements Builder<SyncPullBody, SyncPullBodyBuilder> {
  _$SyncPullBody? _$v;

  SyncUserBuilder? _user;
  SyncUserBuilder get user => _$this._user ??= new SyncUserBuilder();
  set user(SyncUserBuilder? user) => _$this._user = user;

  SyncPhoneBuilder? _phone;
  SyncPhoneBuilder get phone => _$this._phone ??= new SyncPhoneBuilder();
  set phone(SyncPhoneBuilder? phone) => _$this._phone = phone;

  int? _syncOrder;
  int? get syncOrder => _$this._syncOrder;
  set syncOrder(int? syncOrder) => _$this._syncOrder = syncOrder;

  SyncPullBodyBuilder();

  SyncPullBodyBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _user = $v.user.toBuilder();
      _phone = $v.phone.toBuilder();
      _syncOrder = $v.syncOrder;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncPullBody other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SyncPullBody;
  }

  @override
  void update(void Function(SyncPullBodyBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SyncPullBody build() => _build();

  _$SyncPullBody _build() {
    _$SyncPullBody _$result;
    try {
      _$result = _$v ??
          new _$SyncPullBody._(
              user: user.build(),
              phone: phone.build(),
              syncOrder: BuiltValueNullFieldError.checkNotNull(
                  syncOrder, r'SyncPullBody', 'syncOrder'));
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'user';
        user.build();
        _$failedField = 'phone';
        phone.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'SyncPullBody', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
