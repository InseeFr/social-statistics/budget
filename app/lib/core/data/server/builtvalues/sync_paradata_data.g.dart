// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_paradata_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncParadataData> _$syncParadataDataSerializer =
    new _$SyncParadataDataSerializer();

class _$SyncParadataDataSerializer
    implements StructuredSerializer<SyncParadataData> {
  @override
  final Iterable<Type> types = const [SyncParadataData, _$SyncParadataData];
  @override
  final String wireName = 'SyncParadataData';

  @override
  Iterable<Object?> serialize(Serializers serializers, SyncParadataData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'synchronisation',
      serializers.serialize(object.synchronisation,
          specifiedType: const FullType(SyncSync)),
      'paradatas',
      serializers.serialize(object.paradatas,
          specifiedType:
              const FullType(BuiltList, const [const FullType(ParaData)])),
    ];

    return result;
  }

  @override
  SyncParadataData deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncParadataDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'synchronisation':
          result.synchronisation.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncSync))! as SyncSync);
          break;
        case 'paradatas':
          result.paradatas.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ParaData)]))!
              as BuiltList<Object?>);
          break;
      }
    }

    return result.build();
  }
}

class _$SyncParadataData extends SyncParadataData {
  @override
  final SyncSync synchronisation;
  @override
  final BuiltList<ParaData> paradatas;

  factory _$SyncParadataData(
          [void Function(SyncParadataDataBuilder)? updates]) =>
      (new SyncParadataDataBuilder()..update(updates))._build();

  _$SyncParadataData._({required this.synchronisation, required this.paradatas})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        synchronisation, r'SyncParadataData', 'synchronisation');
    BuiltValueNullFieldError.checkNotNull(
        paradatas, r'SyncParadataData', 'paradatas');
  }

  @override
  SyncParadataData rebuild(void Function(SyncParadataDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncParadataDataBuilder toBuilder() =>
      new SyncParadataDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncParadataData &&
        synchronisation == other.synchronisation &&
        paradatas == other.paradatas;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, synchronisation.hashCode);
    _$hash = $jc(_$hash, paradatas.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SyncParadataData')
          ..add('synchronisation', synchronisation)
          ..add('paradatas', paradatas))
        .toString();
  }
}

class SyncParadataDataBuilder
    implements Builder<SyncParadataData, SyncParadataDataBuilder> {
  _$SyncParadataData? _$v;

  SyncSyncBuilder? _synchronisation;
  SyncSyncBuilder get synchronisation =>
      _$this._synchronisation ??= new SyncSyncBuilder();
  set synchronisation(SyncSyncBuilder? synchronisation) =>
      _$this._synchronisation = synchronisation;

  ListBuilder<ParaData>? _paradatas;
  ListBuilder<ParaData> get paradatas =>
      _$this._paradatas ??= new ListBuilder<ParaData>();
  set paradatas(ListBuilder<ParaData>? paradatas) =>
      _$this._paradatas = paradatas;

  SyncParadataDataBuilder();

  SyncParadataDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _synchronisation = $v.synchronisation.toBuilder();
      _paradatas = $v.paradatas.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncParadataData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SyncParadataData;
  }

  @override
  void update(void Function(SyncParadataDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SyncParadataData build() => _build();

  _$SyncParadataData _build() {
    _$SyncParadataData _$result;
    try {
      _$result = _$v ??
          new _$SyncParadataData._(
              synchronisation: synchronisation.build(),
              paradatas: paradatas.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'synchronisation';
        synchronisation.build();
        _$failedField = 'paradatas';
        paradatas.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'SyncParadataData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
