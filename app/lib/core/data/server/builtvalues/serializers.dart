import 'package:bdf_survey/core/data/server/builtvalues/sync_search_store_data.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';

import '../../../../features/para_data/para_data.dart';
import 'sync_group_info.dart';
import 'sync_id.dart';
import 'sync_image.dart';
import 'sync_paradata_body.dart';
import 'sync_paradata_data.dart';
import 'sync_phone.dart';
import 'sync_phone_info.dart';
import 'sync_product.dart';
import 'sync_pull_body.dart';
import 'sync_push_receipt_body.dart';
import 'sync_push_search_product_body.dart';
import 'sync_push_search_store_body.dart';
import 'sync_receipt_data.dart';
import 'sync_register_body.dart';
import 'sync_register_data.dart';
import 'sync_search_product.dart';
import 'sync_search_product_data.dart';
import 'sync_search_store.dart';
import 'sync_sync.dart';
import 'sync_transaction.dart';
import 'sync_user.dart';

part 'serializers.g.dart';

@SerializersFor([
  SyncSync,
  SyncGroupInfo,
  SyncId,
  SyncImage,
  SyncPhone,
  SyncPhoneInfo,
  SyncProduct,
  SyncPullBody,
  SyncPushReceiptBody,
  SyncPushSearchProductBody,
  SyncPushSearchStoreBody,
  SyncReceiptData,
  SyncRegisterBody,
  SyncRegisterData,
  SyncSearchProduct,
  SyncSearchProductData,
  SyncSearchStore,
  SyncSearchStoreData,
  SyncTransaction,
  SyncUser,
  ParaData,
  SyncParadataBody,
  SyncParadataData,
])
final Serializers serializers =
    (_$serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
