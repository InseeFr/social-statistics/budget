import 'package:flutter/material.dart';

import '../../../model/receipt.dart';
import '../../server/builtvalues/sync_transaction.dart';
import '../database_helper.dart';
import 'receipt_product.dart';

class ReceiptTable {
  String tableName = 'tbl_receipt';

  Future<void> createTable() async {
    final db = await DatabaseHelper.instance.database;
    await db.execute('''
          CREATE TABLE $tableName (
            id TEXT,
            date INT,
            storeName TEXT,
            storeCategory TEXT,
            isAbroad INT,
            isOnline INT,
            cityName TEXT,
            postalCode TEXT,
            displayText TEXT,
            discountAmount REAL,
            discountPercentage REAL,
            totalPrice REAL,
            imagePath TEXT)
          ''');
  }

  Future<void> insert(Receipt receipt) async {
    final db = await DatabaseHelper.instance.database;
    await db.insert(tableName, receipt.toMap());
    for (final product in receipt.products.content) {
      await ReceiptProductTable().insert(product, receipt.id);
    }
  }

  Future<void> delete(Receipt receipt) async {
    final db = await DatabaseHelper.instance.database;
    await db.delete(tableName, where: 'id = ?', whereArgs: [receipt.id]);
    for (final product in receipt.products.content) {
      await ReceiptProductTable().delete(product);
    }
  }

  Future<List<Receipt>> query({
    String? id,
    int? startDate,
    int? endDate,
    String? searchInput,
  }) async {
    final db = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> maps = await db.query(tableName);
    final receipts = <Receipt>[];
    for (final map in maps) {
      receipts.add(await Receipt.fromMap(map));
    }
    return receipts;
  }

  Future<List<Receipt>> queryWithDates({
    String? id,
    int? startDate,
    int? endDate,
    String? searchInput,
  }) async {
    final db = await DatabaseHelper.instance.database;
    final query = _createQuery(
      startDate: startDate!,
      endDate: endDate!,
    );
    debugPrint('ReceiptTableQuery: $query');
    final List<Map<String, dynamic>> maps = await db.rawQuery(query);

    final receipts = <Receipt>[];
    for (final map in maps) {
      receipts.add(await Receipt.fromMap(map));
    }
    return receipts;
  }

  Future<Receipt> queryById(String id) async {
    final db = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> maps =
        await db.rawQuery('SELECT * FROM $tableName WHERE id = "$id"');
    return Receipt.fromMap(maps.first);
  }

  String _createQuery({
    String? id,
    int? startDate,
    int? endDate,
    String? searchInput,
  }) {
    final buffer = StringBuffer('SELECT * FROM $tableName');
    if (id != null) {
      buffer.write(' WHERE id = "$id"');
    }
    if (startDate != null && endDate != null) {
      buffer.write(' WHERE date >= $startDate AND date <= $endDate');
    } else if (startDate != null) {
      buffer.write(' WHERE date >= $startDate');
    } else if (endDate != null) {
      buffer.write(' WHERE date <= $endDate');
    }
    if (searchInput != null) {
      buffer.write(' WHERE name LIKE "%$searchInput%"');
    }
    buffer.write(' ORDER BY date DESC');
    return buffer.toString();
  }

  static Future<List<Map>> syncQuery(String id) async {
    final db = await DatabaseHelper.instance.database;
    final maps =
        await db.rawQuery('SELECT * FROM tbl_receipt WHERE id = "$id"');
    final results = <Map<String, dynamic>>[];
    for (final Map row in maps) {
      final _row = <String, dynamic>{};
      _row['date'] = (row['date'] / 1000.0).round();
      _row['discountAmount'] = (row['discountAmount'] as double).toString();
      _row['discountPercentage'] =
          (row['discountPercentage'] as double).toString();
      _row['discountText'] = '';
      _row['expenseAbroad'] = row['isAbroad'] == 0 ? 'false' : 'true';
      _row['expenseOnline'] = row['isOnline'] == 0 ? 'false' : 'true';
      _row['cityName'] = row['cityName'] as String;
      _row['postalCode'] = row['postalCode'] as String;
      _row['displayText'] = row['displayText'] as String;
      _row['receiptLocation'] = row['imagePath'] ?? '';
      _row['receiptProductType'] = '';
      _row['store'] = row['storeName'] as String;
      _row['storeType'] = row['storeCategory'] as String;
      _row['totalPrice'] = row['totalPrice'] as double;
      _row['transactionID'] = row['id'] as String;
      results.add(_row);
    }
    return results;
  }

  Future<void> insertFromBackend(SyncTransaction syncTransaction) async {
    final db = await DatabaseHelper.instance.database;
    final backendData = syncTransaction.toJson();
    final insert = <String, dynamic>{};
    insert['date'] = backendData['date'] * 1000;
    insert['discountAmount'] =
        double.tryParse(backendData['discountAmount'] as String) ?? 0.0;
    insert['discountPercentage'] =
        double.tryParse(backendData['discountPercentage'] as String) ?? 0.0;
    insert['isAbroad'] = backendData['expenseAbroad'] == 'false' ? 0 : 1;
    insert['isOnline'] = backendData['expenseOnline'] == 'false' ? 0 : 1;
    insert['cityName'] = backendData['cityName'];
    insert['postalCode'] = backendData['postalCode'];
    insert['displayText'] = backendData['displayText'];
    insert['imagePath'] = backendData['receiptLocation'] == ''
        ? null
        : backendData['receiptLocation'];
    insert['storeName'] = backendData['store'];
    insert['storeCategory'] = backendData['storeType'];
    insert['id'] = backendData['transactionID'];
    insert['totalPrice'] = backendData['totalPrice'] as double;
    await db.insert('tbl_receipt', insert);
  }
}
