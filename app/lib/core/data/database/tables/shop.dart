import 'package:bdf_survey/core/data/database/database_helper.dart';

class TableShop {
  Future<void> insert(String store) async {
    final db = await DatabaseHelper.instance.database;
    final data = <String, dynamic>{};
    data['shop'] = store.toLowerCase();
    data['shoptype'] = 'Ajout utilisateur';
    await db.insert('tblShop_fr', data);
  }
}
