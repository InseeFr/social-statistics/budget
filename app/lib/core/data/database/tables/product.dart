import 'package:flutter/material.dart';

import '../database_helper.dart';

class TableProduct {
  Future<List<Map<String, dynamic>>> query(
      String productName, String category) async {
    final db = await DatabaseHelper.instance.database;
    debugPrint('SearchProductTableQuery: $productName and $category');
    return db.query('tblProduct_fr',
        where: 'product = ? AND coicop = ?',
        whereArgs: [productName.toLowerCase(), category]);
  }

  Future<void> insert(String productName) async {
    final db = await DatabaseHelper.instance.database;
    final data = <String, dynamic>{};
    data['code'] = '14.1.1.1';
    data['frequency'] = '0';
    data['product'] = productName.toLowerCase();
    data['coicop'] = 'Ajout Utilisateur';
    print('Inserting product in localDB: $data');
    await db.insert('tblProduct_fr', data);
  }
}
