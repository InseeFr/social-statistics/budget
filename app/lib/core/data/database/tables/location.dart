import 'package:flutter/material.dart';

import '../database_helper.dart';

class TableLocation {
  Future<List<Map<String, dynamic>>> query(
      String cityName, String postalCode, String displayText) async {
    final db = await DatabaseHelper.instance.database;
    debugPrint('SearchLocationTableQuery: $cityName and $postalCode');
    return db.query('tblLocation_fr',
        where: 'cityName = ? AND postalCode = ? AND displayText = ?',
        whereArgs: [cityName.toUpperCase(), postalCode, displayText]);
  }

  Future<void> insert(
      String cityName, String postalCode, String displayText) async {
    final db = await DatabaseHelper.instance.database;
    final data = <String, dynamic>{};
    data['cityName'] = cityName.toUpperCase();
    data['postalCode'] = postalCode;
    data['displayText'] = displayText;
    await db.insert('tblLocation_fr', data);
  }
}
