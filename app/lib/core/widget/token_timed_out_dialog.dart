import 'package:bdf_survey/core/controller/util/responsive_ui.dart';
import 'package:bdf_survey/core/data/server/auth_pkce.dart';
import 'package:bdf_survey/core/model/color_pallet.dart';
import 'package:bdf_survey/core/state/translations.dart';
import 'package:bdf_survey/screens/onboarding/login_implicit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';

class TokenTimedOutDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Connection');
    return PopScope(
        canPop: false, // Prevents the dialog from being popped
        child: AlertDialog(
      titlePadding: const EdgeInsets.all(0),
      backgroundColor: ColorPallet.white,
      surfaceTintColor: Colors.transparent,
      title: Container(
        color: ColorPallet.orange,
        padding: EdgeInsets.symmetric(horizontal: 20 * x, vertical: 20 * y),
        child: Row(
          children: <Widget>[
            Text(
                  translations.text('information'),
              style: TextStyle(
                  fontSize: 20 * f,
                  fontWeight: FontWeight.w500,
                  color: Colors.white),
            ),
          ],
        ),
      ),
          content: Text(translations.text('expiredSession'),
          style: TextStyle(
              fontSize: 17 * f,
              fontWeight: FontWeight.w400,
              color: ColorPallet.darkTextColor)),
      actions: <Widget>[
        TextButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text('Fermer',
              style: const TextStyle(color: ColorPallet.midGray)),
        ),
        TextButton(
          onPressed: () async {
            SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
              statusBarColor: ColorPallet.primaryColor,
            ));
            await AuthPKCE.logout();
            Navigator.of(context).pop();
            Navigator.of(context).pop();
            Navigator.push(
              context,
              MaterialPageRoute(
                settings: const RouteSettings(name: 'LoginScreen'),
                builder: (context) => LoginImplicitScreen(),
              ),
            );
            Fluttertoast.showToast(
                  msg: translations.text('deconnectionOK'),
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
            );
          },
          child: Text('Confirmer',
              style: const TextStyle(color: ColorPallet.orange)),
        ),
      ],
        ));
  }
}
