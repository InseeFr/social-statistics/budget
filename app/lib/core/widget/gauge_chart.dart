import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class PieChartSample2 extends StatefulWidget {
  final List<PieChartSectionData> chartData;

  const PieChartSample2({required Key key, required this.chartData})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => PieChart2State();
}

class PieChart2State extends State<PieChartSample2> {
  int touchedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        const SizedBox(
          height: 1,
        ),
        Expanded(
          child: AspectRatio(
            aspectRatio: 0.8,
            child: PieChart(
              PieChartData(
                borderData: FlBorderData(
                  show: false,
                ),
                sectionsSpace: 0,
                centerSpaceRadius: 10,
                sections: widget.chartData,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
