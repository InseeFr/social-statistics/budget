// ignore_for_file: unnecessary_null_comparison

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../screens/manual_entry/controller/add_receipt.dart';
import '../../screens/manual_entry/controller/delete_receipt.dart';
import '../../screens/manual_entry/controller/duplicate_receipt.dart';
import '../../screens/manual_entry/manual_entry_screen.dart';
import '../../screens/receipt_list/controller/product_controller.dart';
import '../../screens/receipt_list/state/receipt_list_state.dart';
import '../controller/util/currency_formatter.dart';
import '../controller/util/responsive_ui.dart';
import '../model/color_pallet.dart';
import '../model/receipt.dart';
import '../model/receipt_product.dart';
import '../state/translations.dart';
import 'product_tile_widget.dart';

class ReceiptTileWidget extends StatefulWidget {
  final Receipt receipt;

  const ReceiptTileWidget(this.receipt);

  @override
  _ReceiptTileWidgetState createState() => _ReceiptTileWidgetState();
}

class _ReceiptTileWidgetState extends State<ReceiptTileWidget> {
  late ReceiptListState receiptListState;
  void showMySnackBar(Receipt receipt) async {
    final snackBar = SnackBar(
      duration: const Duration(seconds: 10),
      content:
          Text(Translations.textStatic('expenseDeleted', 'Manual_Entry', null)),
      action: SnackBarAction(
        textColor: ColorPallet.primaryColor,
        label: Translations.textStatic('cancel', 'Search', null),
        onPressed: () async {
          await addReceipt(receiptListState, receipt);
          receiptListState.notify();
        },
      ),
    );

    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    receiptListState = ReceiptListState.of(context);
    final translations = Translations(context, 'Manual_Entry');
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(horizontal: 5.0 * x, vertical: 6.0 * y),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: ColorPallet.veryLightGray,
                  offset: Offset(0, 0),
                  blurRadius: 2.0 * x,
                  spreadRadius: 3.0 * x)
            ],
          ),
          child: Theme(
            data: ThemeData(
                colorScheme: ColorScheme.fromSwatch()
                    .copyWith(secondary: ColorPallet.primaryColor)),
            child: ExpansionTile(
              title: Row(
                children: <Widget>[
                  SizedBox(
                    width: 175 * x,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: 2 * y, horizontal: 6.0 * x),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(bottom: 4.0),
                            child: Text(
                              widget.receipt.store.name,
                              style: TextStyle(
                                fontSize: 18 * f,
                                color: ColorPallet.darkTextColor,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                          if (widget.receipt.store.category != '')
                            Text(
                              widget.receipt.store.category,
                              style: TextStyle(
                                  fontSize: 14 * f,
                                  color: ColorPallet.midGray,
                                  fontWeight: FontWeight.w600),
                            ),
                        ],
                      ),
                    ),
                  ),
                  if (widget.receipt.imagePath == null)
                    Container()
                  else
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: 6 * x),
                        child: Icon(
                          File(widget.receipt.imagePath).existsSync()
                              ? Icons.camera_alt
                              : Icons.keyboard,
                          color: ColorPallet.darkTextColor,
                          size: 24 * x,
                        )),
                  if (widget.receipt.imagePath == null)
                    Container()
                  else
                    SizedBox(width: 10 * x),
                  Expanded(child: Container()),
                  Text(
                    widget.receipt.totalPrice
                        .toStringAsFixed(2)
                        .addCurrencyFormat(),
                    style: TextStyle(
                        fontSize: 16.5 * f,
                        color: ColorPallet.pink,
                        fontWeight: FontWeight.w700),
                  ),
                ],
              ),
              //leading: ReceiptUtil().getStoreIcon(widget.receipt.store.category),
              children: <Widget>[
                SizedBox(height: 5 * y),
                SizedBox(
                  height: 65 * y,
                  width: MediaQuery.of(context).size.width - 40 * x,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '${widget.receipt.location.displayText}',
                          style: TextStyle(
                            fontSize: 14 * f,
                            color: ColorPallet.midGray,
                            fontWeight: FontWeight.w600,
                          ),
                          textAlign: TextAlign.left,
                        ),
                        SizedBox(height: 5 * y),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            InkWell(
                              onTap: () async {
                                await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    settings: const RouteSettings(
                                        name: 'ManualEntryScreen'),
                                    builder: (context) => ManualEntryScreen(
                                        widget.receipt,
                                        true,
                                        File(widget.receipt.imagePath)
                                            .existsSync(),
                                        widget.receipt.date),
                                  ),
                                );
                              },
                              child: Container(
                                width: 115 * x,
                                height: 25 * y,
                                margin: EdgeInsets.only(right: 5 * x),
                                decoration: BoxDecoration(
                                  color: ColorPallet.darkTextColor,
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Icon(
                                      Icons.mode_edit,
                                      color: Colors.white,
                                      size: 16 * x,
                                    ),
                                    Text(
                                        Translations.textStatic(
                                            'modify', 'Manual_Entry', null),
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 13 * f)),
                                    SizedBox(width: 1 * y),
                                  ],
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () async {
                                await duplicateReceipt(context, widget.receipt);
                                ReceiptListState.of(context).notify();
                              },
                              child: Container(
                                width: 110 * x,
                                height: 25 * y,
                                decoration: BoxDecoration(
                                  color: ColorPallet.darkTextColor,
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Icon(
                                      Icons.content_copy,
                                      color: Colors.white,
                                      size: 16 * x,
                                    ),
                                    Text(
                                        Translations.textStatic(
                                            'duplicate', 'Manual_Entry', null),
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 13 * f)),
                                    SizedBox(width: 1 * y),
                                  ],
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () async {
                                final confirmDelete = await showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      titlePadding: const EdgeInsets.all(0),
                                      backgroundColor: ColorPallet.white,
                                      surfaceTintColor: Colors.transparent,
                                      title: Container(
                                          color: ColorPallet.orange,
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 20 * x,
                                              vertical: 20 * y),
                                          child: Row(children: <Widget>[
                                            Text(
                                                translations.text('deleteItem'),
                                                style: TextStyle(
                                                    fontSize: 20 * f,
                                                    fontWeight: FontWeight.w500,
                                                    color: Colors.white))
                                          ])),
                                      content: Text(translations
                                          .text('removalWarningText')),
                                      actions: <Widget>[
                                        TextButton(
                                          onPressed: () =>
                                              Navigator.of(context).pop(true),
                                          child: Text(
                                              translations.text('confirm'),
                                              style: const TextStyle(
                                                  color: ColorPallet
                                                      .darkGreyColor)),
                                        ),
                                        TextButton(
                                          onPressed: () =>
                                              Navigator.of(context).pop(false),
                                          child: Text(
                                              Translations.textStatic(
                                                  'cancel', 'Search', null),
                                              style: const TextStyle(
                                                  color: ColorPallet.orange)),
                                        ),
                                      ],
                                    );
                                  },
                                );

                                if (confirmDelete) {
                                  await deleteReceipt(context, widget.receipt);
                                  Fluttertoast.showToast(
                                    msg: translations.text('expenseDeleted'),
                                    toastLength: Toast.LENGTH_LONG,
                                    gravity: ToastGravity.BOTTOM,
                                  );
                                }
                              },
                              child: Container(
                                width: 130 * x,
                                height: 25 * y,
                                margin: EdgeInsets.only(left: 5 * x),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      color: ColorPallet.darkTextColor,
                                      width: 2),
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Icon(
                                      Icons.delete,
                                      color: ColorPallet.darkTextColor,
                                      size: 16 * x,
                                    ),
                                    Text(
                                        Translations.textStatic(
                                            'delete', 'Manual_Entry', null),
                                        style: TextStyle(
                                            color: ColorPallet.darkTextColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 13 * f)),
                                    SizedBox(width: 1 * y),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )
                      ]),
                ),
                if (widget.receipt.imagePath == null)
                  Container()
                else
                  File(widget.receipt.imagePath).existsSync()
                      ? Image.file(File(widget.receipt.imagePath))
                      : Container(),
                FutureBuilder<List<ReceiptProduct>>(
                  future: ProductController().getProducts(widget.receipt.id),
                  builder: (context, products) {
                    if (products.data == null) {
                      return Container();
                    }
                    return Column(
                      children: products.data!.map((ReceiptProduct product) {
                        return ProductTileWidget(product);
                      }).toList(),
                    );
                  },
                ),
                //SizedBox(height: 5 * y),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
