import 'package:uuid/uuid.dart';

import '../../screens/insights/controller/util/type_converter.dart';

class ReceiptProduct {
  String? id;
  int? count;
  bool? isReturn;
  String? productDate;
  bool? hasDiscount;
  String? name;
  String? category;
  String? coicop;
  double? price;
  String? receiptId;
  double? discount;

  ReceiptProduct({
    this.id,
    this.count,
    this.isReturn,
    this.productDate,
    this.hasDiscount,
    this.name,
    this.category,
    this.coicop,
    this.price,
    this.receiptId,
    this.discount,
  })  : assert(category != null),
        assert(coicop != null),
        //assert(discount != null),
        assert(name != null);
  //assert(price != null),
  //assert(productDate != null),
  //assert(receiptId != null);

  ReceiptProduct.empty()
      : id = const Uuid().v1(),
        count = 1,
        isReturn = false,
        hasDiscount = false,
        productDate = dateTimeToString(DateTime.now()),
        name = '',
        category = '',
        coicop = '',
        price = 0,
        receiptId = '',
        discount = 0;

  double getTotalPrice() {
    if (isReturn!) {
      return count! * getPrice() * -1;
    }
    return count! * getPrice();
  }

  double getPrice() {
    if (price == null) {
      return 0;
    }
    if (discount == null) {
      return price!;
    }
    if (hasDiscount == false) {
      return price!;
    }
    if (discount! > price!) {
      return 0;
    }
    return price! - discount!;
  }

  void subtractItem() {
    if (count != null && count! > 1) {
      count = count! - 1;
    }
  }

  void addItem() {
    if (count != null) {
      count = count! + 1;
    }
    count = count! + 1;
  }
  

  bool isComplete() {
    if (price == null) {
      return false;
    }
    return price!.toDouble() > 0;
  }

  int getCount() {
    return count ?? 1;
  }

  Map<String, dynamic> toMap(String receiptId) {
    return {
      'id': id,
      'count': count,
      'isReturn': isReturn,
      'productDate': productDate,
      'hasDiscount': hasDiscount,
      'name': name,
      'category': category,
      'coicop': coicop,
      'price': price,
      'receiptId': receiptId,
      'discount': discount,
    };
  }
  static ReceiptProduct fromMap(Map<String, dynamic> map) {
    return ReceiptProduct(
      id: map['id'],
      count: map['count'],
      isReturn: map['isReturn'] == 0 ? false : true,
      productDate: map['productDate'],
      hasDiscount: map['hasDiscount'] == 0 ? false : true,
      name: map['name'],
      category: map['category'],
      coicop: map['coicop'],
      price: map['price'],
      receiptId: map['receiptId'],
      discount: map['discount'],
    );
  }

  @override
  String toString() {
    return 'ReceiptProduct(id: $id, count: $count, isReturn: $isReturn, productDate: $productDate, hasDiscount: $hasDiscount, name: $name, category: $category, coicop: $coicop, price: $price, receiptId: $receiptId, discount: $discount)';
  }
}
