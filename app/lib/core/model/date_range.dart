class DateRange {
  DateTime start;
  DateTime end;

  DateRange(this.start, this.end);

  DateRange.empty()
      : start = DateTime.now(),
        end = DateTime.now();
}
