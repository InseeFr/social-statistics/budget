import 'dart:ui';

import 'package:uuid/uuid.dart';

import '../data/database/tables/receipt_product.dart';
import 'location.dart';
import 'products.dart';
import 'store.dart';

class Receipt {
  String id;
  DateTime date;
  Location location;
  Store store;
  Products products;
  double totalPrice;
  String imagePath;

  static List<VoidCallback> _listeners = [];

  Receipt(this.id, this.date, this.location, this.store, this.products,
      this.totalPrice,
      [this.imagePath = '']);

  Receipt.empty()
      : id = Uuid().v1(),
        date = DateTime.now(),
        location = Location.empty(),
        store = Store.empty(),
        products = Products.empty(),
        totalPrice = 0.0,
        imagePath = '';

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'date': date.millisecondsSinceEpoch,
      'storeName': store.name,
      'storeCategory': store.category,
      'isAbroad': location.isAbroad ? 1 : 0,
      'isOnline': location.isOnline ? 1 : 0,
      'cityName': location.cityName,
      'postalCode': location.postalCode,
      'displayText': location.displayText,
      'discountAmount': products.discountAmount,
      'discountPercentage': products.discountPercentage,
      'totalPrice': products.getTotalPrice(),
      'imagePath': imagePath,
      // 'isSecondhand': location.isSecondhand ? 1 : 0,
    };
  }

  static Future<Receipt> fromMap(Map<String, dynamic> map) async {
    final location = Location(
      map['isAbroad'] == 0 ? false : true,
      map['isOnline'] == 0 ? false : true,
      map['cityName'],
      map['postalCode'],
      map['displayText'],
    );
    final content = await ReceiptProductTable().query(receiptId: map['id']);
    final products = Products(
      map['discountAmount'],
      map['discountPercentage'],
      content,
    );
    final store = Store(
      map['storeName'],
      map['storeCategory'],
    );
    final totalPrice = products.getTotalPrice();
    var receipt = await Receipt(
      map['id'],
      DateTime.fromMillisecondsSinceEpoch(map['date']),
      location,
      store,
      products,
      totalPrice,
      map['imagePath'] ?? '',
    );
    return receipt;
  }

  static void addListener(VoidCallback listener) {
    _listeners.add(listener);
  }

  static void removeListener(VoidCallback listener) {
    _listeners.remove(listener);
  }

  static void notifyListeners() {
    for (var listener in _listeners) {
      listener();
    }
  }

  void updateLocation(Location newLocation) {
    location = newLocation;
    notifyListeners();
  }

  void updateStore(Store newStore) {
    store = newStore;
    notifyListeners();
  }

  void updateShopOrLocation(Store newStore, Location newLocation) {
    store = newStore;
    location = newLocation;
    notifyListeners();
  }

  void copyFrom(Receipt otherReceipt) {
    id = otherReceipt.id;
    date = otherReceipt.date;
    location = otherReceipt.location;
    store = otherReceipt.store;
    products = otherReceipt.products;
    totalPrice = otherReceipt.totalPrice;
    imagePath = otherReceipt.imagePath;
  }

  @override
  String toString() {
    return 'Receipt(id: $id, date: $date, location: $location, store: $store, products: $products, totalPrice: $totalPrice, imagePath: $imagePath)';
  }
}
