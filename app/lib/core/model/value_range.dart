class ValueRange {
  double min;
  double max;

  ValueRange(this.min, this.max);

  ValueRange.empty()
      : min = 0,
        max = 0;
}
