class Location {
  bool isAbroad = false;
  bool isOnline = false;
  String cityName = "";
  String postalCode = "";
  // Display Text for city input
  String displayText = "";

  Location(this.isAbroad, this.isOnline, this.cityName, this.postalCode,
      this.displayText);

  Location.empty();
}
