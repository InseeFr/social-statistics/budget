import 'package:flutter/material.dart';

class Language {
  String id;
  String name;
  String languageCode;
  String countryCode;

  Language(this.id, this.name, this.languageCode, this.countryCode);

  Locale get locale {
    if (countryCode.isEmpty) {
      return Locale(languageCode);
    } else {
      return Locale(languageCode, countryCode);
    }
  }

  String get localeKey {
    if (countryCode.isEmpty) {
      return languageCode;
    } else {
      return '${languageCode}_$countryCode';
    }
  }
}

class Country {
  String id;
  String name;
  String languageId;
  String flagImage;
  String topbarImage;

  Country(this.id, this.name, this.languageId, this.flagImage, this.topbarImage);
}

class International {
  //The chosen language of the HBS-app is independent of the chosen country (= coicop/product/shop lists).
  static final List<Language> _languages = [
    Language('be_nl', 'Nederlands (BE)', 'nl', 'BE'),
    Language('en', 'English', 'en', 'US'),
    Language('be_fr', 'Français (BE)', 'fr', 'BE'),
    Language('fr', 'Français (FR)', 'fr', 'FR'),
    Language('nl', 'Nederlands', 'nl', ''),
    Language('pageKey', 'pageKey', 'pageKey', ''),
    Language('itemKey', 'itemKey', 'itemKey', '')
  ];

  //The chosen country of the HBS-app refers to the applied coicop/product/shop lists.
  //This country is chosen only once, at the welcom page of the app.
  //Luxembourg uses other countries until it supplies it own set of coicop/product/shop lists.
  //With the choice of country a default language will be set, if required this can be changed in the settings page afterwards.
  static final List<Country> _countries = [
    Country('be_nl', 'Belgie (NL)', 'be_nl', 'flag_round_be.png', 'topbar_be_nl.png'),
    Country('be_fr', 'Belgique (FR)', 'be_fr', 'flag_round_be.png', 'topbar_be_fr.png'),
    Country('fr', 'France', 'fr', 'flag_round_fr.png', 'topbar_fr.png'),
    Country('nl', 'Nederland', 'nl', 'flag_round_nl.png', 'topbar_nl.png'),
    Country('pageKey', 'pageKey', 'pageKey', 'round_key.png', 'topbar_nl.png'),
    Country('itemKey', 'itemKey', 'itemKey', 'round_key.png', 'topbar_nl.png'),
  ];

  static final Map<String, String> _countryCodes = {
    // 'de': 'de',
    // 'es': 'es',
    'nl': 'nl',
    // 'no': 'no',
    // 'nb': 'no',
    // 'nn': 'no',
    // 'pl': 'pl',
    // 'sk': 'sl',
    // 'sl': 'sl',
    // 'fi': 'fi',
    'en': 'en',
    // 'hu': 'hu',
    'fr': 'fr',
    'pageKey': 'pageKey',
    'itemKey': 'itemKey',
  };

  //### get item ############################################################

  static Language languageFromId(String id) {
    return _languages.firstWhere((x) => x.id == id);
  }

  static Language languageFromName(String name) {
    return _languages.firstWhere((x) => x.name == name);
  }

  static Country countryFromId(String id) {
    return _countries.firstWhere((x) => x.id == id);
  }

  static Country countryFromName(String name) {
    return _countries.firstWhere((x) => x.name == name);
  }

  static Country countryFromCode(String code) {
    if (_countryCodes.keys.contains(code)) {
      return countryFromId(_countryCodes[code]!);
    } else {
      return countryFromId(_countryCodes['fr']!);
    }
  }


  //### get list ############################################################

  static List<String> languages() {
    final result = <String>[];
    for (final l in _languages) {
      result.add(l.name);
    }
    return result;
  }

  static List<String> countries() {
    final result = <String>[];
    for (final c in _countries) {
      result.add(c.name);
    }
    return result;
  }

  static List<Locale> locales() {
    final result = <Locale>[];
    for (final l in _languages) {
      result.add(l.locale);
    }
    return result;
  }
}
