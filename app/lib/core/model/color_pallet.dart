import 'package:flutter/material.dart';

class ColorPallet {
  static const Color darkGreen = Color(0xFF488225); // Only used for charts
  static const Color darkTextColor = Color(0xFF33425B);
  static const Color darkGreyColor = Color(0xFF515457);
  static const Color lightBlueWithOpacity =
      Color(0xFF4F80C5); //Lightblue, used only for login screen
  static const veryLightBlueWithOpacity = Color.fromARGB(255, 109, 158, 226);
  static const Color lightGray = Color(0xFFDDDDDD);
  static const Color lightGreen =
      Color(0xFFAFCB05); // Used in calendar and login
  static const Color yellow = Color(0xFFFFC400);
  static const Color midblue = Color(0xFF271D6C); //Only used for the charts
  static const Color midGray = Color.fromARGB(255, 142, 142, 142);
  static const Color orange =
      Color.fromARGB(255, 205, 95, 21); // Used in calendar
  static const Color pink =
      Color.fromARGB(255, 222, 41, 80); //Pink used for cost and add screens
  static const Color primaryColor =
      Color(0xFF3467AE); //Lightblue, used for navigation, topbar, etc.
  static const Color veryLightBlue =
      Color(0xFFE5F6FA); //Very lightblue used for calendar, and search text
  static const Color veryLightGray = Color(0xFFEFEFEF);
  static const Color white = Color(0xFFFFFFFF);
}

MaterialColor primarySwatchColor = const MaterialColor(
  0xFF3467AE,
  {
    50: Color(0xFF3467AE),
    100: Color(0xFF3467AE),
    200: Color(0xFF3467AE),
    300: Color(0xFF3467AE),
    400: Color(0xFF3467AE),
    500: Color(0xFF3467AE),
    600: Color(0xFF3467AE),
    700: Color(0xFF3467AE),
    800: Color(0xFF3467AE),
    900: Color(0xFF3467AE),
  },
);

ColorScheme colorScheme = ColorScheme(
  brightness: Brightness.light,
  primary: Color(0xFF3467AE),
  onPrimary: Color(0xFFFFFFFF),
  secondary: Color(0xFF625B71),
  onSecondary: Color(0xFFFFFFFF),
  tertiary: Color(0xFF7D5260),
  onTertiary: Color(0xFFFFFFFF),
  error: Color(0xFFB3261E),
  onError: Color(0xFFFFFFFF),
  background: Color(0xFFFFFBFE),
  onBackground: Color(0xFF1C1B1F),
  surface: Color(0xFFFFFBFE),
  onSurface: Color(0xFF1C1B1F),
  surfaceTint: Colors.transparent,
);
