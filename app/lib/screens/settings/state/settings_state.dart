
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';



class SettingState extends Model {
  String hour = '';
  String minute = '';
  bool mostRecentSearchSuggestionProducts = true;
  bool mostRecentSearchSuggestionStore = true;
  bool status = false;


  Future<void> loadSettings() async {
    final prefs = await SharedPreferences.getInstance();
    hour = prefs.getString('notificationHour') ?? '20';
    minute = prefs.getString('notificationMinute') ?? '00';
    status = prefs.getBool('notificationStatus') ?? false;

    notifyListeners();
  }


  void changeSearchSuggestionTypeProducts() {
    mostRecentSearchSuggestionProducts = !mostRecentSearchSuggestionProducts;
    notifyListeners();
  }

  void changeSearchSuggestionTypeStore() {
    mostRecentSearchSuggestionStore = !mostRecentSearchSuggestionStore;
    notifyListeners();
  }

  static SettingState of(BuildContext context) =>
      ScopedModel.of<SettingState>(context);
}
