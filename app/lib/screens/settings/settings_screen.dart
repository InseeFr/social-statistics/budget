// ignore_for_file: unnecessary_null_comparison


import 'package:bdf_survey/core/controller/user_progress.dart';
import 'package:bdf_survey/core/data/server/auth_pkce.dart';
import 'package:bdf_survey/screens/onboarding/login_implicit.dart';
import 'package:bdf_survey/screens/search/controller/add_search_suggestion.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';

import '../../core/controller/util/responsive_ui.dart';
import '../../core/model/color_pallet.dart';
import '../../core/state/translations.dart';
import '../../features/para_data/para_data_name.dart';
import '../../features/spotlight_tutorial/controller/settings.dart';
import 'state/settings_state.dart';

late Translations translations;

TextStyle headerText =
    TextStyle(color: Colors.black, fontSize: 18.0, fontWeight: FontWeight.w700);
TextStyle settingText =
    TextStyle(color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.w600);
TextStyle valueText =
    TextStyle(color: Colors.grey, fontSize: 16.0, fontWeight: FontWeight.w600);
TextStyle valueTextBlue =
    TextStyle(color: Colors.blue, fontSize: 16.0, fontWeight: FontWeight.w600);
GlobalKey keyButton1 = GlobalKey();
GlobalKey keyButton2 = GlobalKey();
GlobalKey keyButton3 = GlobalKey();

class SettingsScreen extends StatefulWidget with ParaDataName {
  @override
  String get name => 'SettingsScreen';

  @override
  State<StatefulWidget> createState() {
    return _SettingsScreenState();
  }
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  void initState() {
    initTargets(keyButton1, keyButton2, keyButton3);
    super.initState();
    // Future.delayed(Duration(milliseconds: 500), () {
    //   showInitialTutorial(context);
    // });
  }

  Future<void> showInitialTutorial(BuildContext context) async {
    final prefs = await SharedPreferences.getInstance();
    final status = prefs.getBool('mainSettingsTutorial') ?? true;
    if (!status) {
      await prefs.setBool('mainSettingsTutorial', true);
      showTutorial(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Settings');
    headerText = TextStyle(
        color: ColorPallet.darkTextColor,
        fontSize: 18.0 * f,
        fontWeight: FontWeight.w700);
    settingText = TextStyle(
        color: ColorPallet.darkTextColor,
        fontSize: 16.0 * f,
        fontWeight: FontWeight.w600);
    valueText = TextStyle(
        color: ColorPallet.midGray,
        fontSize: 16.0 * f,
        fontWeight: FontWeight.w600);
    valueTextBlue = TextStyle(
        color: ColorPallet.primaryColor,
        fontSize: 16.0 * f,
        fontWeight: FontWeight.w600);

    return Column(
      children: <Widget>[
        _TopBarWidget(),
        Expanded(
          child: Container(
            margin: EdgeInsets.only(bottom: 8.0 * y),
            child: ListView(
              children: <Widget>[
                SizedBox(height: 8.0 * y),
                _CalendarWidget(),
                SizedBox(height: 8.0 * y),
                _SuggestionWidget(),
                SizedBox(height: 8.0 * y),
                _LoggoutWidget(),
                SizedBox(height: 8.0 * y),
                _ContactWidget(),
                SizedBox(height: 8.0 * y),
                _VersionWidget(),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class _TopBarWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorPallet.primaryColor,
      height: 50 * y,
      child: Row(
        children: <Widget>[
          SizedBox(width: 22.0 * x),
          ScopedModelDescendant<SettingState>(builder: (context, child, model) {
            return Text(
              translations.text('settings'),
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 22.0 * f,
                  fontWeight: FontWeight.w600),
            );
          }),
          SizedBox(width: 10 * x),
          InkWell(
              onTap: () {
                showTutorial(context);
              },
              child: Icon(Icons.info, color: Colors.white, size: 28.0 * x)),
        ],
      ),
    );
  }
}

class _ContactWidget extends StatelessWidget {
  String? encodeQueryParameters(Map<String, String> params) {
    return params.entries
        .map((MapEntry<String, String> e) =>
            '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}')
        .join('&');
  }
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<SettingState>(
        builder: (context, child, model) {
      return _SettingsBoxWidget(
        settingsWidget: Container(
          margin:
              EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20.0 * y),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.forum,
                      color: ColorPallet.darkTextColor, size: 24.0 * x),
                  SizedBox(width: 10.0 * x),
                  Text(translations.text('contact'), style: headerText),
                ],
              ),
              SizedBox(height: 24.0 * x),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(translations.text('phoneNumber'), style: settingText),
                  InkWell(
                      onTap: () async {
                        final url =
                            'tel:+33-9-72-72-40-00';
                        if (await canLaunchUrlString(url)) {
                          await launchUrlString(url);
                        } else {
                          throw 'Could not launch $url';
                        }
                      },

                      child: Padding(
                        padding: EdgeInsets.only(right: 9.5 * x),
                        child: Text('09 72 72 40 00',
                            style: valueTextBlue),
                      ))
                ],
              ),
              SizedBox(height: 18.0 * x),
              Container(height: 1.0 * y, color: ColorPallet.lightGray),
              SizedBox(height: 8.0 * y),
              Row(
                //crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(translations.text('assistance'), style: settingText),
                  IconButton(
                    icon: Icon(
                      Icons.open_in_new,
                      color: ColorPallet.primaryColor,
                    ), // Use any icon you like
                    onPressed: () async {
                      await launchUrl(Uri.parse(
                          'https://enquetes.stat-publique.fr/bdf/contacter-assistance'));
                    },
                  )
                ],
              ),
              SizedBox(height: 8.0 * x),
              Container(height: 1.0 * y, color: ColorPallet.lightGray),
              SizedBox(height: 8.0 * y),
              Row(
                //crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(translations.text('dataCollectionPortal'),
                      style: settingText),
                  IconButton(
                    icon: Icon(
                      Icons.open_in_new,
                      color: ColorPallet.primaryColor,
                    ), // Use any icon you like
                    onPressed: () async {
                      await launchUrl(
                          Uri.parse('https://enquetes.stat-publique.fr/bdf/'));
                    },
                  )
                ],
              ),
              SizedBox(height: 4.0 * y),
              Container(height: 1.0 * y, color: ColorPallet.lightGray),
            ],
          ),
        ),
      );
    });
  }
}

class _SettingsBoxWidget extends StatelessWidget {
  final Widget settingsWidget;

  const _SettingsBoxWidget({required this.settingsWidget});

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<SettingState>(
        builder: (context, child, model) {
      return Container(
        margin: EdgeInsets.symmetric(horizontal: 8.0 * x),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: ColorPallet.veryLightGray,
                offset: Offset(0, 0),
                blurRadius: 2.0 * x,
                spreadRadius: 3.0 * x)
          ],
        ),
        child: settingsWidget,
      );
    });
  }
}

class _SuggestionWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<SettingState>(
        builder: (context, child, model) {
      return _SettingsBoxWidget(
          settingsWidget: Container(
        margin: EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20.0 * y),
        child: Column(
          key: keyButton2,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.storage,
                    color: ColorPallet.darkTextColor, size: 24.0 * x),
                SizedBox(width: 10.0 * x),
                Text(translations.text('dataSuggestions'), style: headerText),
              ],
            ),
            SizedBox(height: 8.0 * x),
            Align(
              alignment: Alignment.centerLeft,
              child: TextButton(
                onPressed: () async {
                  try {
                    await SearchSuggestions.removeAllStores();

                    Fluttertoast.showToast(
                        msg: translations.text('storeDataCleared'),
                        toastLength: Toast.LENGTH_LONG,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIosWeb: 1);
                  } catch (e) {
                    // Handle error
                    Fluttertoast.showToast(
                        msg: translations.text('clearenceFailure'),
                        toastLength: Toast.LENGTH_LONG,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIosWeb: 1);
                  }
                },
                child: Text(translations.text('clearStoreData'),
                    style: settingText),
              ),
            ),
            Container(height: 1.0 * y, color: ColorPallet.lightGray),
            SizedBox(height: 24.0 * x),
            Align(
              alignment: Alignment.centerLeft,
              child: TextButton(
                onPressed: () async {
                  try {
                    await SearchSuggestions.removeAllProducts();

                    Fluttertoast.showToast(
                        msg: translations.text('productDataCleared'),
                        toastLength: Toast.LENGTH_LONG,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIosWeb: 1);
                  } catch (e) {
                    // Handle error
                    Fluttertoast.showToast(
                        msg: translations.text('clearenceFailure'),
                        toastLength: Toast.LENGTH_LONG,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIosWeb: 1);
                  }
                },
                child: Text(translations.text('clearProductData'),
                    style: settingText),
              ),
            ),
            Container(height: 1.0 * y, color: ColorPallet.lightGray),
          ],
        ),
      ));
    });
  }
}

class _CalendarWidget extends StatefulWidget {
  @override
  _CalendarWidgetState createState() => _CalendarWidgetState();
}

class _CalendarWidgetState extends State<_CalendarWidget> {
  late DateTime firstDate;
  late DateTime lastDate;
  
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<SettingState>(
        builder: (context, child, model) {
      return _SettingsBoxWidget(
        settingsWidget: Container(
          key: keyButton1,
          margin:
              EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20.0 * y),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.calendar_today,
                      color: ColorPallet.darkTextColor, size: 24.0 * x),
                  SizedBox(width: 10.0 * x),
                  Text(translations.text('collectionDates'), style: headerText),
                ],
              ),
              SizedBox(height: 24.0 * x),
              FutureBuilder<List<DateTime>>(
                future: UserProgressController().getDaysInExperiment(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return CircularProgressIndicator();
                  } else if (snapshot.hasError) {
                    return Text('Error: ${snapshot.error}');
                  } else {
                    DateTime firstDate =
                        (snapshot.data != null && snapshot.data!.isNotEmpty)
                            ? snapshot.data![0]
                            : DateTime.now();
                    DateTime lastDate =
                        (snapshot.data != null && snapshot.data!.isNotEmpty)
                            ? snapshot.data![snapshot.data!.length - 1]
                            : DateTime.now();
                    return InkWell(
                        onTap: () async {
                              final DateTime? pickedDate = await showDatePicker(
                                context: context,
                                initialDate: firstDate,
                            firstDate: DateTime(2020, 1, 1),
                            initialEntryMode: DatePickerEntryMode.calendarOnly,
                            lastDate: DateTime(2100),
                                helpText:
                                translations.text('resetStartDate'),
                                builder: (context, child) {
                                  return Theme(
                                    data: ThemeData.light().copyWith(
                                      colorScheme: ColorScheme.light(
                                        surfaceTint: Colors.transparent,
                                        primary: ColorPallet.primaryColor,
                                        surface: Colors.white,
                                        onSurface: Colors.black,
                                      ),
                                    ),
                                    child: child!,
                                  );
                                },
                              );
                              if (pickedDate != null) {
                                try {
                                  await UserProgressController()
                                      .resetDaysOfExperiment();
                                  await UserProgressController()
                                      .initDaysOfExperimentWithStartDay(
                                      pickedDate, 6);
                                  Fluttertoast.showToast(
                                    msg:
                                        translations
                                    .text('collectionDatesModification'),
                                    toastLength: Toast.LENGTH_LONG,
                                    gravity: ToastGravity.BOTTOM,
                                  );
                              setState(() {
                                firstDate = pickedDate;
                                lastDate = pickedDate.add(Duration(days: 6));
                              });
                                } catch (e) {
                                  Fluttertoast.showToast(
                                    msg: "Une erreur s'est produite: $e",
                                    toastLength: Toast.LENGTH_LONG,
                                    gravity: ToastGravity.BOTTOM,
                                  );
                                }
                              }
                            },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              (snapshot.data != null &&
                                      snapshot.data!.isNotEmpty)
                                  ? '${firstDate.day.toString().padLeft(2, '0')}/${firstDate.month.toString().padLeft(2, '0')}/${firstDate.year} - ${lastDate.day.toString().padLeft(2, '0')}/${lastDate.month.toString().padLeft(2, '0')}/${lastDate.year}'
                                  : 'Aucune date sélectionnée',
                              style: settingText,
                            ),
                            Padding(
                              padding: EdgeInsets.only(right: 9.5 * x),
                              child: InkWell(
                            child: Icon(Icons.edit,
                                color: ColorPallet.primaryColor,
                                size: 24.0 * x),
                          ),
                        ),
                      ],
                        )
                    );
                  }
                },
              ),
              SizedBox(height: 8.0 * x),
              Container(height: 1.0 * y, color: ColorPallet.lightGray),
            ],
          ),
        ),
      );
    });
  }
}

class _LoggoutWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<SettingState>(
        builder: (context, child, model) {
      return _SettingsBoxWidget(
        settingsWidget: Container(
          margin:
              EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20.0 * y),
          child: Column(
            key: keyButton3,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.account_circle,
                      color: ColorPallet.darkTextColor, size: 24.0 * x),
                  SizedBox(width: 10.0 * x),
                  Text(translations.text('accountParameters'),
                      style: headerText),
                ],
              ),
              SizedBox(height: 24.0 * x),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Se déconnecter', style: settingText),
                  Padding(
                    padding: EdgeInsets.only(right: 9.5 * x),
                    child: InkWell(
                      onTap: () async {
                        await AuthPKCE.logout();

                        Navigator.of(context).pop();
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            settings: const RouteSettings(name: 'LoginScreen'),
                            builder: (context) => LoginImplicitScreen(),
                          ),
                        );
                      },
                      child: Icon(Icons.logout,
                          color: ColorPallet.primaryColor, size: 24.0 * x),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 8.0 * x),
              Container(height: 1.0 * y, color: ColorPallet.lightGray),
              
            ],
          ),
        ),
      );
    });
  }
}


class _VersionWidget extends StatelessWidget {
  Future<String> getVersionNumber() async {
    final packageInfo = await PackageInfo.fromPlatform();
    final versionName = packageInfo.version;
    final versionCode = packageInfo.buildNumber;
    return '$versionName+$versionCode';
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<SettingState>(
        builder: (context, child, model) {
      return _SettingsBoxWidget(
        settingsWidget: Container(
          margin:
              EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20.0 * y),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.info_outline,
                      color: ColorPallet.darkTextColor, size: 24.0 * x),
                  SizedBox(width: 10.0 * x),
                  Text(translations.text('information'), style: headerText),
                ],
              ),
              SizedBox(height: 24.0 * x),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(translations.text('versionNumber'), style: settingText),
                  Padding(
                    padding: EdgeInsets.only(right: 9.5 * x),
                    child: FutureBuilder(
                      future: getVersionNumber(),
                      builder: (BuildContext context,
                          AsyncSnapshot<String> snapshot) {
                        if (!snapshot.hasData) {
                          return const Text('');
                        } else {
                          return Text(snapshot.data!, style: valueText);
                        }
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: 8.0 * x),
              Container(height: 1.0 * y, color: ColorPallet.lightGray),
            ],
          ),
        ),
      );
    });
  }
}
