import 'package:bdf_survey/core/controller/util/responsive_ui.dart';
import 'package:bdf_survey/core/data/server/auth_pkce.dart';
import 'package:bdf_survey/core/model/color_pallet.dart';
import 'package:bdf_survey/core/state/translations.dart';
import 'package:bdf_survey/screens/onboarding/login_implicit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';           

class LoggoutDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      titlePadding: const EdgeInsets.all(0),
      backgroundColor: ColorPallet.white,
      surfaceTintColor: Colors.transparent,
      title: Container(
        color: ColorPallet.orange,
        padding: EdgeInsets.symmetric(horizontal: 20 * x, vertical: 20 * y),
        child: Row(
          children: <Widget>[
            Text(
              'Se déconnecter',
              style: TextStyle(
                  fontSize: 20 * f,
                  fontWeight: FontWeight.w500,
                  color: Colors.white),
            ),
          ],
        ),
      ),
      content: Text(
          'La déconnexion se fait automatiquement une fois l\'application fermée. \nVoulez vous vraiment vous déconnecter de l\'application ?',
          style: TextStyle(
              fontSize: 17 * f,
              fontWeight: FontWeight.w400,
              color: ColorPallet.darkTextColor)),
      actions: <Widget>[
        TextButton(
          onPressed: () async {
            SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
              statusBarColor: ColorPallet.primaryColor,
            ));

            await AuthPKCE.logout();

            Navigator.of(context).pop();
            Navigator.push(
              context,
              MaterialPageRoute(
                settings: const RouteSettings(name: 'LoginScreen'),
                builder: (context) => LoginImplicitScreen(),
              ),
            );
            Fluttertoast.showToast(
              msg:
                  Translations.textStatic('deconnectionOK', 'Connection', null),
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
            );
          },
          child: Text('Confirmer',
              style: const TextStyle(color: ColorPallet.orange)),
        ),
        TextButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text('Annuler',
              style: const TextStyle(color: ColorPallet.orange)),
        )
      ],
    );
  }
}
