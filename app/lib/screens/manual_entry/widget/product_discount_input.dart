import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';


import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/state/translations.dart';
import '../state/product_state.dart';

class ProductDiscountInput extends StatefulWidget {
  @override
  _ProductDiscountInputState createState() => _ProductDiscountInputState();
}

class _ProductDiscountInputState extends State<ProductDiscountInput> {
  late FocusNode myFocusNode;

  @override
  void initState() {
    super.initState();
    myFocusNode = FocusNode();
  }

  @override
  void dispose() {
    myFocusNode.dispose();
    super.dispose();
  }

  bool isValidInput(String newPrice) {
    try {
      final _price = double.parse(newPrice);
      if (_price > 0 && _price < 10000000) {
        return true;
      }
    } on Exception {
      print('Input error: $Exception');
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    final translations = Translations(context, 'Manual_Entry');
    final productState = ProductState.of(context);
    final product = productState.product;

    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 7 * y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.8,
            decoration: BoxDecoration(border: Border.all(color: ColorPallet.lightGray, width: 1.7 * x), borderRadius: BorderRadius.circular(12.0 * x)),
            child: Row(
              children: <Widget>[
                SizedBox(width: 10.0 * x),
                Padding(
                  padding: EdgeInsets.only(top: 7.0 * y),
                  child: Image.asset(
                    'assets/images/discountIcon.png',
                    height: 30.0 * y,
                    width: 30.0 * x,
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  width: 284 * x,
                  height: 42 * y,
                  margin: EdgeInsets.only(top: 2 * y),
                  child: TextField(
                    autofocus: true,
                    focusNode: myFocusNode,
                    style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w500, fontSize: 18 * f),
                    autocorrect: false,
                    keyboardType: const TextInputType.numberWithOptions(decimal: true),
                    onChanged: (String discount) {
                      setState(
                        () {
                          discount = discount.replaceAll(',', '.');
                          if (isValidInput(discount)) {
                            product.discount = double.parse(discount);
                            productState.notify();
                          } else {
                            product.discount = null;
                            productState.notify();
                            if (discount.isNotEmpty) {
                              if (discount != '0' && discount != '0.' && discount != '0,' && discount != ',' && discount != '.') {
                                Fluttertoast.showToast(
                                  msg: translations.text('invalidInput'),
                                  toastLength: Toast.LENGTH_LONG,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIosWeb: 1,
                                );
                              }
                            }
                          }
                        },
                      );
                    },
                    decoration: InputDecoration(
                        hintText: product.discount.toString(),
                        hintStyle: TextStyle(color: ColorPallet.midGray, fontWeight: FontWeight.w500, fontSize: 18 * f),
                        filled: true,
                        fillColor: Colors.transparent,
                        border: InputBorder.none,
                        prefix: product.discount == null
                            ? const SizedBox()
                            : Padding(
                                padding: EdgeInsets.only(right: 2.0 * x),
                                child: Text(Translations.textStatic(
                                    'currencySymbol', 'CurrencySetting', null)),
                              ),
                        prefixStyle: const TextStyle(color: ColorPallet.darkTextColor),
                        contentPadding: EdgeInsets.symmetric(horizontal: 10 * x)),
                  ),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          left: 48,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(
              '${translations.text('productDiscount')}*',
              style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w700, fontSize: 14 * f),
            ),
          ),
        ),
      ],
    );
  }
}
