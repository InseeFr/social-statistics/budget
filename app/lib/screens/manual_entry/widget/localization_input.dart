// ignore_for_file: unnecessary_null_comparison

import 'package:bdf_survey/core/model/receipt.dart';
import 'package:bdf_survey/screens/search/location_search.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/state/translations.dart';
import '../state/receipt_state.dart';

GlobalKey keyButton9 = GlobalKey();

class LocationInput extends StatefulWidget {
  @override
  _LocationInputState createState() => _LocationInputState();
}

class _LocationInputState extends State<LocationInput> {
  bool isEnabled = false;

  @override
  void initState() {
    super.initState();
    final receiptModel = ReceiptState.of(context).receipt;
    _updateIsEnabled(receiptModel);

    Receipt.addListener(_onReceiptModelChanged);
  }

  @override
  void dispose() {
    Receipt.removeListener(_onReceiptModelChanged);
    super.dispose();
  }

  void _onReceiptModelChanged() {
    final receiptModel = ReceiptState.of(context).receipt;
    setState(() {
      _updateIsEnabled(receiptModel);
    });
  }

  void _updateIsEnabled(receiptModel) {
    isEnabled = !receiptModel.location.isAbroad &&
        !receiptModel.location.isOnline;
    if (!isEnabled) {
      receiptModel.location.displayText = '';
      receiptModel.location.cityName = '';
      receiptModel.location.postalCode = '';
    }
  }

  @override
  Widget build(BuildContext context) {
    final translations = Translations(context, 'Manual_Entry');
    final receiptModel = ReceiptState.of(context).receipt;
    return Stack(
      key: keyButton9,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0 * x, vertical: 8 * y),
          child: isEnabled
              ? Container(
                  width: MediaQuery.of(context).size.width * 0.9,
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: ColorPallet.lightGray, width: 1.7 * x),
                      color: Colors.transparent,
                      borderRadius: BorderRadius.circular(12.0 * x)),
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: 10.0 * x, vertical: 10 * y),
                    child: InkWell(
                      onTap: () async {
                        if (isEnabled) {
                          final result = await Navigator.push(
                            context,
                            // TODO: Add new option for search widget to search for places
                            MaterialPageRoute(
                              settings: const RouteSettings(
                                  name: 'SearchLocationWidget'),
                              builder: (context) =>
                                  const SearchLocationWidget(),
                            ),
                          );
                          print('Result: $result');
                          if (result != null) {
                            receiptModel.location.cityName = result[0];
                            receiptModel.location.postalCode = result[1];
                            receiptModel.location.displayText = result[2];
                            ReceiptState.of(context).notify();
                          }
                        }
                      },
                      child: Column(
                        children: <Widget>[
                          LocationDisplay(
                            isEnabled: isEnabled,
                            displayText: receiptModel.location.displayText,
                            x: x,
                            f: f,
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              : DottedBorder(
                  color: ColorPallet.lightGray,
                  strokeWidth: 1.5,
                  dashPattern: const [9, 6],
                  radius: const Radius.circular(10),
                  strokeCap: StrokeCap.round,
                  borderType: BorderType.RRect,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.88,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 10.0 * x, vertical: 10 * y),
                      child: InkWell(
                        child: Column(
                          children: <Widget>[
                            LocationDisplay(
                              isEnabled: isEnabled,
                              displayText: receiptModel.location.displayText,
                              x: x,
                              f: f,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
        ),
        Positioned(
          left: 48 * x,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(
              translations.text('city'),
              style: TextStyle(
                  color: isEnabled
                      ? ColorPallet.darkTextColor
                      : ColorPallet.lightGray,
                  fontWeight: FontWeight.w700,
                  fontSize: 14 * f),
            ),
          ),
        ),
      ],
    );
  }
}

class LocationDisplay extends StatelessWidget {
  final bool isEnabled;
  final String displayText;
  final double x;
  final double f;

  const LocationDisplay({
    Key? key,
    required this.isEnabled,
    required this.displayText,
    required this.x,
    required this.f,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Icon(
              Icons.location_on,
              color:
                  isEnabled ? ColorPallet.darkTextColor : ColorPallet.lightGray,
              size: 27 * x,
            ),
            SizedBox(width: 10.0 * x),
            SizedBox(
              width: 305 * x,
              child: Text(
                displayText.length > 1 ? displayText : '',
                style: TextStyle(
                  color: displayText.length > 1
                      ? ColorPallet.darkTextColor
                      : ColorPallet.lightGray,
                  fontWeight: FontWeight.w500,
                  fontSize: 18 * f,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
