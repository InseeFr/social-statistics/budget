import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/model/receipt_product.dart';
import '../../../core/state/translations.dart';
import '../../insights/controller/util/type_converter.dart';
import '../../search/controller/add_search_suggestion.dart';
import '../../search/search.dart';
import '../state/receipt_state.dart';
import 'add_product_dialog.dart';

GlobalKey keyButton5 = GlobalKey();

class AddProductButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translations = Translations(context, 'Manual_Entry');

    return ScopedModelDescendant<ReceiptState>(
        builder: (context, _, receiptState) {
      return InkWell(
        key: keyButton5,
        onTap: () async {
          final result = await Navigator.push(
            context,
            MaterialPageRoute(
              settings: const RouteSettings(name: 'SearchWidget'),
              builder: (context) => const SearchWidget(true),
            ),
          );

          if (result != null) {
            final date = receiptState.receipt.date;
            final productDate = dateTimeToString(date);
            var product = ReceiptProduct.empty();
            product.category = result[1];
            if (result.length > 2) {
              product.coicop = result[2];
            } else if (result.length > 1) {
              product.coicop = result[1];
            } else {
              product.coicop = null;
            }
            product.name = result[0];
            product.price = 0;
            product.productDate = productDate;
            await SearchSuggestions.addProduct(
                product.name!, product.category!,
                product.coicop!, product.count ?? 1);

            await showDialog(
              context: context,
              routeSettings: const RouteSettings(name: 'ProductDialog'),
              builder: (BuildContext context) {
                return ProductDialog(product, receiptState);
              },
            );
          }
        },
        child: DottedBorder(
          color: ColorPallet.darkTextColor,
          strokeWidth: 1.8,
          dashPattern: const [9, 3],
          radius: const Radius.circular(10),
          strokeCap: StrokeCap.round,
          borderType: BorderType.RRect,
          child: ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(10)),
            child: SizedBox(
              height: 50 * y,
              width: 360 * x,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(width: 10 * x),
                  SizedBox(
                    width: 260.0,
                    child: Text(
                      translations.text('addNewServiceProductOrDiscount'),
                      //maxLines: 1,
                      //overflow: TextOverflow.fade,
                      softWrap: false,
                      style: TextStyle(
                          color: ColorPallet.darkTextColor,
                          fontWeight: FontWeight.w600,
                          fontSize: 15 * f),
                    ),
                  ),
                  SizedBox(width: 15 * x),
                  const Icon(
                    Icons.add,
                    color: ColorPallet.darkTextColor,
                  )
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}
