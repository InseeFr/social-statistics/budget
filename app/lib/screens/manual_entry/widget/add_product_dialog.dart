import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../../core/controller/util/currency_formatter.dart';
import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/model/receipt_product.dart';
import '../../../core/state/translations.dart';
import '../state/product_state.dart';
import '../state/receipt_state.dart';
import 'price_input.dart';
import 'product_name_input.dart';

class ProductDialog extends StatelessWidget {
  final ReceiptProduct product;
  final ReceiptState receiptState;

  const ProductDialog(this.product, this.receiptState);

  @override
  Widget build(BuildContext context) {
    final translations = Translations(context, 'Manual_Entry');
    return ScopedModel<ProductState>(
      model: ProductState(product: product),
      child: ScopedModelDescendant<ProductState>(
        builder: (context, _, productState) {
          return AlertDialog(
            surfaceTintColor: Colors.transparent,
            insetPadding:
                EdgeInsets.symmetric(horizontal: 10 * x, vertical: 10 * y),
            contentPadding: const EdgeInsets.all(0),
            content: SizedBox(
              width: 500 * x,
              height: product.hasDiscount! ? 440 * y : 370 * y,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(width: 35 * x),
                        Expanded(child: Container()),
                        Text(translations.text('addingNewItems'),
                            style: TextStyle(
                              fontSize: 22 * f,
                              fontWeight: FontWeight.w700,
                              color: ColorPallet.darkTextColor,
                            )),
                        Expanded(child: Container()),
                        InkWell(
                            onTap: () {
                              Navigator.pop(context, false);
                            },
                            child: const Icon(Icons.close,
                                color: ColorPallet.darkTextColor)),
                        SizedBox(width: 15 * x)
                      ]),
                  Column(children: <Widget>[
                    ProductNameInput(productState),
                    PriceInput(),
                    
                  ]),
                  Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          product
                              .getTotalPrice()
                              .toStringAsFixed(2)
                              .addCurrencyFormat(),
                          style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 18 * f,
                              color: ColorPallet.darkTextColor),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 31 * y,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: product.isComplete()
                            ? ColorPallet.darkTextColor
                            : ColorPallet.midGray,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6.5 * x),
                        ),
                      ),
                      onPressed: () async {
                        if (product.isComplete()) {
                          receiptState.receipt.products.content.add(product);
                          receiptState.notify();
                          FocusScope.of(context).requestFocus(FocusNode());
                          Navigator.pop(context, true);
                        }
                      },
                      child: Text(
                        translations.text('add'),
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 17.0 * f),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
