import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../../core/controller/util/currency_formatter.dart';
import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/state/translations.dart';
import '../state/receipt_state.dart';

GlobalKey keyButton7 = GlobalKey();

class BottomBarWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translations = Translations(context, 'Manual_Entry');

    return ScopedModelDescendant<ReceiptState>(builder: (context, _, receipState) {
      return Column(
        children: <Widget>[
          Container(height: 1 * y, color: ColorPallet.darkTextColor.withOpacity(0.27)),
          SizedBox(height: 10 * y),
          Row(
            children: <Widget>[
              SizedBox(width: 18 * x),
              SizedBox(width: 17 * x),
              Column(
                key: keyButton7,
                children: <Widget>[
                  Text(
                    translations.text('totalAmount') +
                        ': ' +
                        receipState.receipt.products
                            .getTotalPrice()
                            .toStringAsFixed(2)
                            .addCurrencyFormat(),
                    style: TextStyle(
                      color: ColorPallet.darkTextColor,
                      fontWeight: FontWeight.w700,
                      fontSize: 24 * f,
                    ),
                  ),
                  SizedBox(height: 10 * y),
                  
                ],
              )
            ],
          ),
          SizedBox(height: 5 * y)
        ],
      );
    });
  }
}
