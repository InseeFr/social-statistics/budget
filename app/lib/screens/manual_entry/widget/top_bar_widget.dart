import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/state/translations.dart';
import '../../../features/spotlight_tutorial/controller/manual_entry_tutorial.dart';
import '../../receipt_list/state/receipt_list_state.dart';
import '../controller/add_receipt.dart';
import '../controller/update_receipt.dart';
import '../state/receipt_state.dart';

GlobalKey keyButton1 = GlobalKey();
GlobalKey keyButton8 = GlobalKey();

class TopBarWidget extends StatefulWidget {
  final ScrollController _scrollControllerPage;
  final bool isUpdate;
  const TopBarWidget(this._scrollControllerPage, this.isUpdate);

  @override
  _TopBarWidgetState createState() => _TopBarWidgetState();
}

class _TopBarWidgetState extends State<TopBarWidget> {
  bool _isButtonPressed = false;

  @override
  Widget build(BuildContext context) {
    final translations = Translations(context, 'Manual_Entry');
    return Row(
      children: <Widget>[
        Text(
          translations.text('addReceipt'),
          style: TextStyle(
              color: Colors.white,
              fontSize: 21 * x,
              fontWeight: FontWeight.w600),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 7 * x),
          child: InkWell(
            onTap: () {
              showTutorial(context);
            },
            child: Icon(Icons.info, color: Colors.white, size: 25.0 * x),
          ),
        ),
        ScopedModelDescendant<ReceiptState>(
          builder: (context, child, state) => SizedBox(
            height: 32 * y,
            child: _isButtonPressed
                ? Transform.scale(
                    scale: 0.7,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 50 * x), 
                      child: CircularProgressIndicator(
                      color: Colors.white,
                    ),
                    ))
                : ElevatedButton(
              key: keyButton8,
              style: ElevatedButton.styleFrom(
                backgroundColor: state.isReceiptComplete()
                    ? ColorPallet.darkTextColor
                    : ColorPallet.midGray,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0 * x),
                ),
              ),
                    onPressed: _isButtonPressed
                        ? null
                        : () async {
                            if (state.isReceiptComplete()) {
                              setState(() {
                                _isButtonPressed = true;
                              });
                              try {
                                if (widget.isUpdate) {
                                  await updateReceipt(context,
                                          ReceiptState.of(context).receipt)
                                      .then((_) {
                                   
                                    WidgetsBinding.instance
                                        .addPostFrameCallback((_) {
                                      Fluttertoast.cancel(); 
                                      Fluttertoast.showToast(
                                        msg: 'Le reçu a été enregistré.',
                                        toastLength: Toast.LENGTH_LONG,
                                        gravity: ToastGravity.BOTTOM,
                                      );
                                    });
                                    Navigator.pop(context);
                                  });
                                } else {
                                  await addReceipt(
                                    ReceiptListState.of(context),
                                    ReceiptState.of(context).receipt,
                                  ).then((_) {
                                    
                                    WidgetsBinding.instance
                                        .addPostFrameCallback((_) {
                                      Fluttertoast.cancel(); 
                                      Fluttertoast.showToast(
                                        msg: 'Le reçu a été enregistré.',
                                        toastLength: Toast.LENGTH_LONG,
                                        gravity: ToastGravity.BOTTOM,
                                      );
                                    });
                                    Navigator.pop(context);
                                  });
                                }
                                //Navigator.of(context).pop();
                              } catch (e) {
                                print('Error: ' + e.toString());
                                Navigator.of(context).pop();
                              }
                            } else {
                              var warning = '';
                              if (!state.hasProducts() &&
                                  !state.isstoreComplete()) {
                                warning =
                                    translations.text('warningNoShopInfo');
                              } else {
                                if (!state.hasProducts()) {
                                  warning =
                                      translations.text('warningNoProduct');
                                }
                                if (!state.isstoreComplete()) {
                                  warning =
                                      translations.text('warningNoShopInfo');
                                  await widget._scrollControllerPage.animateTo(
                                    0,
                                    curve: Curves.easeOut,
                                    duration: const Duration(milliseconds: 300),
                                  );
                                }
                              }
                              Fluttertoast.showToast(
                                msg: warning,
                                toastLength: Toast.LENGTH_LONG,
                                textColor: Colors.white,
                              );
                            }
                          },
              child: Text(
                translations.text('complete'),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16 * x,
                ),
              ),
            ),
          ),
        ),
        SizedBox(width: 20 * x),
      ],
    );
  }
}
