import 'package:flutter/material.dart';

import '../../../core/data/database/tables/receipt.dart';
import '../../../core/data/server/sync.dart';
import '../../../core/data/server/sync_db.dart';
import '../../../core/model/receipt.dart';
import '../../receipt_list/state/receipt_list_state.dart';

Future<void> deleteReceipt(BuildContext context, Receipt receipt) async {
  final _syncDatabase = SyncDatabase();
  await _syncDatabase.updateSyncSync(dataReceiptType, receipt.id, deleted);
  await ReceiptTable().delete(receipt);
  ReceiptListState.of(context).notify();
  await Synchronise.synchronise();
}
