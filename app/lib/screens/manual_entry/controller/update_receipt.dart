import 'package:flutter/material.dart';

import '../../../core/data/database/tables/receipt.dart';
import '../../../core/data/server/sync.dart';
import '../../../core/data/server/sync_db.dart';
import '../../../core/model/receipt.dart';
import '../../receipt_list/state/receipt_list_state.dart';
import 'delete_receipt.dart';

Future<void> updateReceipt(BuildContext context, Receipt receipt) async {
  await deleteReceipt(context, receipt);
  await ReceiptTable().insert(receipt);
  final _syncDatabase = SyncDatabase();
  await _syncDatabase.updateSyncSync(dataReceiptType, receipt.id, updated);
  await Synchronise.synchronise();
  ReceiptListState.of(context).notify();
}
