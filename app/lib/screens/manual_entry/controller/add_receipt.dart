import 'package:bdf_survey/core/state/translations.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../../core/data/database/tables/receipt.dart';
import '../../../core/data/server/sync.dart';
import '../../../core/data/server/sync_db.dart';
import '../../../core/model/receipt.dart';
import '../../receipt_list/state/receipt_list_state.dart';

Future<void> addReceipt(
    ReceiptListState receiptListState, Receipt receipt) async {
  print('-------- AddReceipt to database and server -----');
  receipt.totalPrice = receipt.products.getTotalPrice();
  await _addReceiptToDatabase(receipt);
  await _addReceiptToServer(receipt);
  receiptListState.notify();
}

Future<void> _addReceiptToDatabase(Receipt receipt) async {
  try {
    await ReceiptTable().insert(receipt);
  } catch (e) {
    print('Error: ' + e.toString());
    Fluttertoast.showToast(
      msg: Translations.textStatic('addDatabaseError', 'Manual_Entry', null),
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
    );
    throw e;
  }
}

Future<void> _addReceiptToServer(Receipt receipt) async {
  try {
    final _syncDatabase = SyncDatabase();
    await _syncDatabase.createSyncSync(dataReceiptType, receipt.id, created);
    await Synchronise.synchronise();
  } catch (e) {
    print('Error: ' + e.toString());
    Fluttertoast.showToast(
      msg:
          Translations.textStatic('addSyncError', 'Manual_Entry', null),
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
    );
    throw e;
  }
}
