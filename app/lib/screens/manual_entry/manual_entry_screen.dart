import 'package:bdf_survey/core/data/database/tables/receipt.dart';
import 'package:bdf_survey/screens/manual_entry/widget/localization_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../core/controller/util/responsive_ui.dart';
import '../../core/model/color_pallet.dart';
import '../../core/model/receipt.dart';
import '../../core/widget/close_page_warning_dialog.dart';
import '../../features/para_data/para_data_name.dart';
import '../../features/spotlight_tutorial/controller/manual_entry_tutorial.dart';
import 'state/receipt_state.dart';
import 'widget/add_product_button.dart';
import 'widget/bottom_bar_widget.dart';
import 'widget/date_input.dart';
import 'widget/receipt_products_list.dart';
import 'widget/shop_attributes_input.dart';
import 'widget/shop_input.dart';
import 'widget/top_bar_widget.dart';

class ManualEntryScreen extends StatefulWidget with ParaDataName {
  Receipt receiptModel;
  final bool isUpdate;
  final bool isPhoto;
  final DateTime date;

  @override
  String get name => 'ManualEntryScreen';

  ManualEntryScreen(
    this.receiptModel,
    this.isUpdate,
    this.isPhoto,
    this.date,
  );

  @override
  _ManualEntryScreenState createState() => _ManualEntryScreenState();
}

class _ManualEntryScreenState extends State<ManualEntryScreen> {
  String receiptId = '';
  DateTime date = DateTime.now();
  @override
  void initState() {
    super.initState();
    initTargets(keyButton2, keyButton3, keyButton4, keyButton5, keyButton7,
        keyButton8, keyButton9);
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: ColorPallet.primaryColor,
    ));
    Future.delayed(Duration(milliseconds: 500), () {
      showInitialTutorial(context);
    });
    receiptId = widget.receiptModel.id;
  }

  Future<void> showInitialTutorial(BuildContext context) async {
    final prefs = await SharedPreferences.getInstance();
    final status = prefs.getBool('manualEntryTutorial') ?? true;
    if (!status) {
      await prefs.setBool('manualEntryTutorial', true);
      showTutorial(context);
    }
  }

  Future<void> resetReceiptModel() async {
    final Receipt initialReceipt = await ReceiptTable().queryById(receiptId);
    setState(() {
      widget.receiptModel = initialReceipt;
    });
  }

  @override
  Widget build(BuildContext context) {
    final scrollController = ScrollController();
    return ScopedModel<ReceiptState>(
      model: ReceiptState(widget.receiptModel),
      child: Scaffold(
        appBar: AppBar(
          leading: InkWell(
            onTap: () async {
              resetReceiptModel();
              if (widget.isUpdate) {
                Navigator.of(context).pop();
              } else {
                await showDialog(
                  context: context,
                  routeSettings:
                      const RouteSettings(name: 'ClosingScreenWarning'),
                  builder: (BuildContext context) {
                    return ClosingScreenWarning();
                  },
                );
              }
            },
            child: Icon(Icons.close, color: Colors.white, size: 28 * x),
          ),
          titleSpacing: 0,
          title: TopBarWidget(scrollController, widget.isUpdate),
          backgroundColor: ColorPallet.primaryColor,
          systemOverlayStyle: SystemUiOverlayStyle.light,
        ),
        body: Container(
          color: Colors.white,
          child: ScopedModelDescendant<ReceiptState>(
            builder: (context, child, receiptState) {
              return Column(
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      controller: scrollController,
                      child: MiddleSection(
                          scrollController, receiptState, widget.isPhoto, date),
                    ),
                  ),
                  BottomBarWidget(),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}

class MiddleSection extends StatelessWidget {
  final ScrollController scrollController;
  final ReceiptState receiptState;
  final bool isPhoto;
  final DateTime date;

  const MiddleSection(
      this.scrollController, this.receiptState, this.isPhoto, this.date);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 10 * y),
        DateInput(date),
        SizedBox(height: 10 * y),
        ShopAttributesInput(),
        SizedBox(height: 10 * y),
        ShopInput(),
        SizedBox(height: 10 * y),
        LocationInput(),
        SizedBox(height: 10 * y),
        SizedBox(
          height: 230 * y,
          child: ListView(
            children: <Widget>[
              ReceiptProductsList(scrollController),
              if (!isPhoto)
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 22.0 * x),
                  child: AddProductButton(),
                ),
              SizedBox(height: 7 * y),
            ],
          ),
        ),
        SizedBox(height: 20 * y),
      ],
    );
  }
}
