import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../../core/model/receipt.dart';

class ReceiptState extends Model {
  final Receipt receipt;

  ReceiptState(this.receipt);

  static ReceiptState of(BuildContext context) => ScopedModel.of<ReceiptState>(context);

  bool isstoreComplete() {
    if (receipt.store.name.isEmpty || receipt.store.category.isEmpty) {
      return false;
    }
    return true;
  }

  bool hasProducts() {
    if (receipt.products.content.isEmpty) {
      return false;
    }
    return true;
  }

  bool shouldContainLocation() {
    if (receipt.location.isAbroad || receipt.location.isOnline) {
      return false;
    }
    return true;
  }

  bool isReceiptComplete() {
    if (shouldContainLocation()) {
      if (isstoreComplete() &&
          hasProducts() &&
          receipt.location.displayText.length > 2) {
        return true;
      }
    } else {
      if (isstoreComplete() && hasProducts()) {
        return true;
      }
    }
    return false;
  }

  void notify() {
    notifyListeners();
  }
}
