import 'dart:io' show Platform;

import 'package:bdf_survey/core/controller/user_progress.dart';
import 'package:bdf_survey/core/data/server/builtvalues/sync_id.dart';
import 'package:bdf_survey/core/data/server/builtvalues/sync_phone_info.dart';
import 'package:bdf_survey/core/data/server/builtvalues/sync_register_data.dart';
import 'package:bdf_survey/core/data/server/sync.dart';
import 'package:bdf_survey/core/data/server/sync_db.dart';
import 'package:bdf_survey/core/state/configuration.dart';
import 'package:bdf_survey/core/state/translations.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';

late Translations translations;

class Login {
  Future<bool> initializeExperiment(
      BuildContext context, DateTime startDate) async {
    var daysOfExperiment = 6;
    await UserProgressController()
        .initDaysOfExperimentWithStartDay(startDate, daysOfExperiment);
    await saveLanguagePreference();
    await saveTablePreference();
    return true;
  }

  Future<SyncRegisterData?> _registerPhone(
      String username, String password) async {
    final phoneName = DateTime.now().millisecondsSinceEpoch.toString();
    final phoneInfos = <SyncPhoneInfo>[];
    final deviceInfo = DeviceInfoPlugin();

    phoneInfos.add(SyncPhoneInfo.newInstance(
        'country', LanguageSetting.tablePreference.toString()));
    phoneInfos.add(
        SyncPhoneInfo.newInstance('language', LanguageSetting.key.toString()));

    if (Platform.isAndroid) {
      final androidInfo = await deviceInfo.androidInfo;
      phoneInfos.add(SyncPhoneInfo.newInstance('phoneType', 'android'));
      phoneInfos.add(
          SyncPhoneInfo.newInstance('model', androidInfo.model.toString()));
      phoneInfos.add(SyncPhoneInfo.newInstance(
          'manufacturer', androidInfo.manufacturer.toString()));
      phoneInfos.add(
          SyncPhoneInfo.newInstance('brand', androidInfo.brand.toString()));
      phoneInfos.add(
          SyncPhoneInfo.newInstance('product', androidInfo.product.toString()));
      phoneInfos
          .add(SyncPhoneInfo.newInstance('type', androidInfo.type.toString()));
    } else if (Platform.isIOS) {
      final iosInfo = await deviceInfo.iosInfo;
      phoneInfos.add(SyncPhoneInfo.newInstance('phoneType', 'ios'));
      phoneInfos
          .add(SyncPhoneInfo.newInstance('model', iosInfo.model.toString()));
      phoneInfos
          .add(SyncPhoneInfo.newInstance('name', iosInfo.name.toString()));
      phoneInfos.add(SyncPhoneInfo.newInstance(
          'systemName', iosInfo.systemName.toString()));
      phoneInfos.add(SyncPhoneInfo.newInstance(
          'systemVersion', iosInfo.systemVersion.toString()));
      phoneInfos.add(SyncPhoneInfo.newInstance(
          'identifierForVendor', iosInfo.identifierForVendor.toString()));
    }
    final syncRegisterData = await Synchronise.registerNewPhone(
        username, password, phoneName, phoneInfos);
    return syncRegisterData;
  }

  Future<void> _configureApp(
      BuildContext context, SyncRegisterData syncRegisterData) async {
    late String insights;
    late String questionnaire;
    late String paradata;
    //late String ocr;

    for (final gi in syncRegisterData.groupInfos) {
      switch (gi.key) {
        case 'InsightsConfiguration':
          insights = gi.value;
          break;
        case 'QuestionnaireConfiguration':
          questionnaire = gi.value;
          break;
        case 'ParadataConfiguration':
          paradata = gi.value;
          break;
      }
    }
    await Configuration.of(context).save(
        insightsString: insights,
        questionnaireString: questionnaire,
        paradataString: paradata,
        //ocrString: ocr,
        ocrString: 'disabled',
        env: 'dev');
  }

  Future<void> _updateSyncId(
      SyncRegisterData syncRegisterData, String password) async {
    final _syncDatabase = SyncDatabase();
    var syncId = await _syncDatabase.getSyncId();
    debugPrint(syncRegisterData.user.name);
    syncId = SyncId((b) => b
      ..userName = syncRegisterData.user.name
      ..userPassword = password
      ..phoneName = syncRegisterData.phone.name);
    debugPrint('updateSyncId, syncId: $syncId');
    await _syncDatabase.updateSyncId(syncId);
  }

  static Future<void> saveLanguagePreference() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('languagePreference', LanguageSetting.key);
  }

  static Future<void> saveTablePreference() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('tablePreference', LanguageSetting.tablePreference);
  }

  Future<String> getUsername() async {
    final prefs = await SharedPreferences.getInstance();
    final username = prefs.getString('login_username') ?? '';
    return username;
  }

  Future<void> _saveUsername(String username) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('login_username', username);
  }

  Future<bool> authenticate(
    BuildContext context,
  ) async {
    final username = await getUsername();
    print('>> Login user $username');
    final prefs = await SharedPreferences.getInstance();
//TEMP (backend request)

    final syncRegisterData =
        await _registerPhone(username.toLowerCase(), username.toLowerCase());

    if (syncRegisterData == null) {
      Fluttertoast.showToast(
        msg: Translations.textStatic('serverError', 'Login', null),
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
      );
      prefs.remove('refresh_token');
      prefs.remove('access_token');
      prefs.remove('offline_token');
      prefs.remove('login_username');
      return false;
    }

    if (syncRegisterData.user.id == -1) {
      Fluttertoast.showToast(
        msg: Translations.textStatic('incorrectPassword', 'Login', null),
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
      );
      prefs.remove('refresh_token');
      prefs.remove('access_token');
      prefs.remove('offline_token');
      prefs.remove('login_username');
      return false;
    }
    await _saveUsername(username.toLowerCase());
    await _configureApp(context, syncRegisterData);
    await _updateSyncId(syncRegisterData, username.toLowerCase());

    return true;
  }
}
