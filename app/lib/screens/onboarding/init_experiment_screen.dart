import 'package:bdf_survey/core/model/international.dart';
import 'package:bdf_survey/features/para_data/para_data_scoped_model.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

import '../../core/controller/util/responsive_ui.dart';
import '../../core/model/color_pallet.dart';
import '../../core/state/translations.dart';
import '../../features/menu/menu.dart';
import 'controller/util/login_setup.dart';

late Translations translations;

class InitExperimentScreen extends StatefulWidget {
  @override
  String get name => 'InitExperimentScreen';

  @override
  _InitExperimentScreenState createState() => _InitExperimentScreenState();
}

class _InitExperimentScreenState extends State<InitExperimentScreen> {
  late DateTime startDate;

  @override
  void initState() {
    super.initState();
    startDate = DateTime.now();
  }

  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Login');
    final ParaDataScopedModel model = ParaDataScopedModel.of(context);

    return Scaffold(
        body: Container(
      height: MediaQuery.of(context).size.height * 14.5 / 10,
      color: ColorPallet.primaryColor,
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 80 * y,
            ),
            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: 40.0 * x, vertical: 20.0 * y),
              child: Text(
                "Sélectionnez la date de début de l'enquête, vous pourrez la modifier plus tard",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 22 * f,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 40 * x),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: 12 * x, vertical: 12 * y),
                  child: SfDateRangePicker(
                    initialSelectedDate: startDate,
                    allowViewNavigation: true,
                    showNavigationArrow: true,
                    enablePastDates: true,
                    backgroundColor: Colors.white,
                    onSelectionChanged:
                        (DateRangePickerSelectionChangedArgs args) {
                      setState(() {
                        startDate = args.value;
                      });
                    },
                    headerHeight: 40 * y,
                    selectionColor: ColorPallet.primaryColor,
                    selectionTextStyle: TextStyle(
                      color: Colors.white,
                      fontSize: 12 * f,
                      fontWeight: FontWeight.w700,
                    ),
                    todayHighlightColor: ColorPallet.primaryColor,
                    monthCellStyle: DateRangePickerMonthCellStyle(
                      todayTextStyle: TextStyle(
                        color: ColorPallet.primaryColor,
                        fontSize: 12 * f,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    monthViewSettings:
                        DateRangePickerMonthViewSettings(firstDayOfWeek: 1),
                    selectionMode: DateRangePickerSelectionMode.single,
                    headerStyle: DateRangePickerHeaderStyle(
                      textAlign: TextAlign.center,
                      textStyle: TextStyle(
                        color: ColorPallet.primaryColor,
                        fontSize: 20 * f,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  )),
            ),
            SizedBox(
              height: 100 * y,
              child: Center(
                child: ButtonTheme(
                  minWidth: 330.0 * x,
                  height: 35.0 * y,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.symmetric(
                          horizontal: 135 * x, vertical: 10 * y),
                      backgroundColor: ColorPallet.yellow,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    onPressed: () async {
                      final isStarted = await Login().initializeExperiment(
                        context,
                        startDate,
                      );
                      if (isStarted) {
                        model.openScreen('InitExperimentDay',
                            '${DateFormat('dd/MM/yyyy', International.languageFromId(LanguageSetting.key).localeKey).format(startDate)}');
                        await Navigator.push(
                          context,
                          MaterialPageRoute(
                            settings: const RouteSettings(name: 'MenuScreen'),
                            builder: (context) => Menu(),
                          ),
                        );
                      }
                    },
                    child: Text(
                      "Valider",
                      style: TextStyle(
                        color: ColorPallet.darkTextColor,
                        fontSize: 17 * f,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Align(
                alignment: Alignment.bottomCenter,
                child: DotsIndicator(
                  dotsCount: 2,
                  position: 1,
                  decorator: DotsDecorator(
                    size: const Size.square(9.0),
                    activeSize: const Size(18.0, 9.0),
                    activeColor: ColorPallet.yellow,
                    color: ColorPallet.veryLightBlueWithOpacity,
                    activeShape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0)),
                  ),
                ))
          ],
        ),
      ),
    ));
  }
}
