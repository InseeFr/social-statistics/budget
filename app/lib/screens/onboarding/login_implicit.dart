import 'package:bdf_survey/core/controller/start_app.dart';
import 'package:bdf_survey/core/data/server/auth_pkce.dart';
import 'package:bdf_survey/core/data/server/sync.dart';
import 'package:bdf_survey/screens/onboarding/init_experiment_screen.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../core/controller/util/responsive_ui.dart';
import '../../core/data/database/database_helper.dart';
import '../../core/model/color_pallet.dart';
import '../../core/state/translations.dart';
import '../../features/menu/menu.dart';
import '../../features/para_data/para_data_name.dart';
import 'controller/util/login_setup.dart';

late Translations translations;

class LoginImplicitScreen extends StatefulWidget with ParaDataName {
  @override
  String get name => 'LoginScreen';

  @override
  _LoginImplicitScreenState createState() => _LoginImplicitScreenState();
}

class _LoginImplicitScreenState extends State<LoginImplicitScreen> {
  String username = '';

  @override
  void initState() {
    super.initState();
    initDatabase();
    initializeTranslations();
    initToken();
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
          statusBarColor: ColorPallet.lightBlueWithOpacity),
    );
  }

  Future<void> initDatabase() async {
    await DatabaseHelper.instance.database;
  }

  Future<void> initToken() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('access_token', '');
    prefs.setString('refresh_token', '');
  }

  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Login');
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              _LoginManualWidget(),
            ],
          ),
        ),
      ),
    );
  }
}

class _LoginManualWidget extends StatefulWidget {
  const _LoginManualWidget();
  @override
  __LoginManualWidgetState createState() => __LoginManualWidgetState();
}

class __LoginManualWidgetState extends State<_LoginManualWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 14.5 / 10,

      /// 2,
      color: ColorPallet.primaryColor,
      child: Column(
        children: <Widget>[
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 150 * y,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 40 * x, vertical: 25 * y),
            child: Text(
              'Enquête budget des familles',
              style: TextStyle(
                color: Colors.white,
                fontSize: 30 * f,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          SizedBox(
            height: 100 * y,
            child: Center(
              child: ButtonTheme(
                minWidth: 330.0 * x,
                height: 37.0 * y,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.symmetric(
                        horizontal: 40 * x, vertical: 10 * y),
                    backgroundColor: ColorPallet.yellow,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(11),
                    ),
                  ),
                  onPressed: () async {
                    List<ConnectivityResult> connectivityResult =
                        await (Connectivity().checkConnectivity());
                    bool isNotConnected =
                        connectivityResult.contains(ConnectivityResult.none);
                    final isBackendActive = await Synchronise.isBackendActive();

                    if (!isNotConnected && isBackendActive) {
                      try {
                        final prefs = await SharedPreferences.getInstance();

                        await AuthPKCE.signIn();

                        final isAuthenticated =
                            await Login().authenticate(context);

                        if (isAuthenticated) {
                          await prefs.setBool('isAuthenticated', true);
                          final initialized = await isInitialized();

                          !initialized
                              ? Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    settings: const RouteSettings(
                                        name: 'InitExperimentScreen'),
                                    builder: (context) =>
                                        InitExperimentScreen(),
                                  ),
                                )
                              : Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    settings:
                                        const RouteSettings(name: 'MenuScreen'),
                                    builder: (context) => Menu(),
                                  ),
                                );
                        }
                      } catch (e) {
                        Fluttertoast.showToast(
                          msg:
                              "Le serveur n'est pas parvenu à vous authentifier",
                          toastLength: Toast.LENGTH_LONG,
                          gravity: ToastGravity.BOTTOM,
                        );
                      }
                    } else {
                      Fluttertoast.showToast(
                        msg: isNotConnected
                            ? "Veuillez vérifier votre connexion internet"
                            : "Le serveur n'est pas accessible",
                        toastLength: Toast.LENGTH_LONG,
                        gravity: ToastGravity.BOTTOM,
                      );
                    }
                  },
                  child: Text(
                    "Connexion",
                    style: TextStyle(
                      color: ColorPallet.darkTextColor,
                      fontSize: 24 * f,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
            ),
          ),
          // TextButton(
          //   onPressed: () async {
          //     final Uri _url = Uri.parse('https://www.insee.fr/fr/accueil');
          //     if (!await launchUrl(_url)) {
          //       throw Exception('Could not launch $_url');
          //     }
          //   },
          //   child: Text(
          //     'Première connexion ?',
          //     style: TextStyle(
          //       color: Colors.white,
          //       fontSize: 15 * f,
          //       fontWeight: FontWeight.w500,
          //       decoration: TextDecoration.underline,
          //       decorationColor: Colors.white,
          //       decorationThickness: 1.5,
          //     ),
          //   ),
          // ),
          Container(
              margin: EdgeInsets.symmetric(vertical: 10.0),
              child: Align(
                  alignment: Alignment.bottomCenter,
                  child: DotsIndicator(
                    dotsCount: 2,
                    position: 0,
                    decorator: DotsDecorator(
                      size: const Size.square(9.0),
                      activeSize: const Size(18.0, 9.0),
                      activeColor: ColorPallet.yellow,
                      color: ColorPallet.veryLightBlueWithOpacity,
                      activeShape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    ),
                  ))),
        ],
      ),
    );
  }
}
