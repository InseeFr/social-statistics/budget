import 'package:bdf_survey/screens/receipt_list/controller/receipt_controller.dart';
import 'package:flutter/material.dart';

import '../../features/filter_drawer/state/filter.dart';
import '../../features/para_data/para_data_name.dart';
import '../../features/spotlight_tutorial/controller/receipt_list_tutorial.dart';
import 'widget/receipt_list_widget.dart';
import 'widget/top_bar_widget.dart';

class ReceiptListScreen extends StatefulWidget {
  @override
  _ReceiptListScreenState createState() => _ReceiptListScreenState();
}

class _ReceiptListScreenState extends State<ReceiptListScreen> {
  @override
  void initState() {
    super.initState();
    ReceiptController().getAnnotatedReceiptWithoutFilter();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    ReceiptController().getAnnotatedReceiptWithoutFilter();
  }

  @override
  Widget build(BuildContext context) {
    initTargets(keyButton1, keyButton2);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      key: FilterState.of(context).scaffoldKey,
      //endDrawer: FliterDrawer(),
      body: Column(
        children: <Widget>[
          TopBarWidget(),
          ReceiptListWidget(),
        ],
      ),
    );
  }
}
