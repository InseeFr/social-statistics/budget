import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/state/translations.dart';
import '../../../features/filter_drawer/state/filter.dart';
import '../../../features/spotlight_tutorial/controller/receipt_list_tutorial.dart';

GlobalKey keyButton1 = GlobalKey();
GlobalKey keyButton2 = GlobalKey();
//GlobalKey keyButton3 = GlobalKey();

    
class TopBarWidget extends StatefulWidget {
  const TopBarWidget();

  @override
  State<StatefulWidget> createState() {
    return _TopBarWidgetState();
  }
}

class _TopBarWidgetState extends State<TopBarWidget> {
  @override
  void initState() {
    initTargets(keyButton1, keyButton2);
    super.initState();
    // Future.delayed(Duration(milliseconds: 500), () {
    //   showInitialTutorial(context);
    // });
  }

  Future<void> showInitialTutorial(BuildContext context) async {
    final prefs = await SharedPreferences.getInstance();
    final status = prefs.getBool('receiptListTutorial') ?? true;
    if (!status) {
      await prefs.setBool('receiptListTutorial', true);
      showTutorial(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorPallet.primaryColor,
      height: 50.0 * x,
      child: ScopedModelDescendant<FilterState>(
        builder: (_, __, filterState) {
          if (filterState.isSearchMode) {
            return SearchEnabled();
          }
          return SearchDisabled();
        },
      ),
    );
  }
}

class SearchDisabled extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(width: 22.0 * x),
        Text(
          Translations.textStatic('expensesPage', 'Spending', null),
          style: TextStyle(
            color: Colors.white,
            fontSize: 22.0 * f,
            fontWeight: FontWeight.w600,
          ),
        ),
        SizedBox(width: 10 * x),
        InkWell(
            key: keyButton1,
            onTap: () {
              showTutorial(context);
            },
            child: Icon(Icons.info, color: Colors.white, size: 28.0 * x)),
        Expanded(child: Container()),
        InkWell(
          onTap: () {
            FilterState.of(context).enableSearchMode();
          },
          child: Icon(
            Icons.search,
            key: keyButton2,
            size: 30.0 * f,
            color: Colors.white,
          ),
        ),
        SizedBox(width: 6.0 * x),
        // ScopedModelDescendant<FilterState>(
        //   builder: (_, __, filterState) => InkWell(
        //     key: keyButton3,
        //     onTap: () {
        //       filterState.showDrawer();
        //     },
        //     child: FilterUtil().getIcon(filterState),
        //   ),
        // ),
        SizedBox(width: 15.0 * x),
      ],
    );
  }
}

class SearchEnabled extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(width: 12.0 * x),
        SizedBox(
          width: 350.0 * x,
          child: TextField(
            cursorColor: Colors.white,
            onChanged: FilterState.of(context).setSearchInput,
            autofocus: true,
            style: TextStyle(color: Colors.white, fontSize: 20.0 * f),
            decoration: InputDecoration(
              border: InputBorder.none,
              prefixIcon: Icon(
                Icons.search,
                color: ColorPallet.veryLightBlue,
                size: 24 * x,
              ),
              hintText:
                  Translations.textStatic('findTransactions', 'Spending', null),
              hintStyle: TextStyle(color: ColorPallet.veryLightBlue, fontSize: 18.0 * f),
            ),
          ),
        ),
        InkWell(
          onTap: FilterState.of(context).disableSearchMode,
          child: Icon(
            Icons.close,
            color: Colors.white,
            size: 33.0 * x,
          ),
        ),
      ],
    );
  }
}
