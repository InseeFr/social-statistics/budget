import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../core/controller/util/date.dart';
import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/international.dart';
import '../../../core/state/translations.dart';

class DateHeaderWidget extends StatelessWidget {
  const DateHeaderWidget(this.date);

  final DateTime date;

  String getHeaderTitle() {
    if (DateUtil.isSameDay(date, DateTime.now())) {
      return Translations.textStatic('today', 'Manual_Entry', null);
    } else if (DateUtil.isSameDay(date, DateTime.now().subtract(const Duration(days: 1)))) {
      return Translations.textStatic('yesterday', 'Manual_Entry', null);
    } else {
      return DateFormat('EEEE, d MMMM', International.languageFromId(LanguageSetting.key).localeKey).format(date);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10.0 * x, vertical: 10 * y),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(getHeaderTitle(), style: TextStyle(color: Colors.grey, fontSize: 15.0 * f, fontWeight: FontWeight.w500)),
        ],
      ),
    );
  }
}
