// ignore_for_file: unnecessary_null_comparison

import 'package:badges/badges.dart' as badges;
import 'package:flutter/material.dart';

import '../../../../core/controller/util/responsive_ui.dart';
import '../../../../core/model/color_pallet.dart';
import '../../../../features/filter_drawer/state/filter.dart';

class FilterUtil {
  Widget getIcon(FilterState filterState) {
    var filterCounter = 0;
    if (filterState.endDate != null || filterState.startDate != null) {
      filterCounter = filterCounter + 1;
    }
    if (filterState.minValue != null || filterState.maxValue != null) {
      filterCounter = filterCounter + 1;
    }
    if (filterCounter == 0) {
      return Icon(Icons.filter_list, size: 37.0 * f, color: Colors.white);
    } else {
      return badges.Badge(
        position: badges.BadgePosition.topEnd(top: 0 * y),
        badgeStyle: badges.BadgeStyle(badgeColor: ColorPallet.pink),
        badgeContent: Text(
          filterCounter.toString(),
          style: const TextStyle(color: Colors.white),
        ),
        child: Icon(Icons.filter_list, size: 37.0 * f, color: Colors.white),
      );
    }
  }
}
