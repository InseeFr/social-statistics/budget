import 'package:flutter/material.dart';

import '../../../core/data/database/tables/receipt.dart';
import '../../../core/model/receipt.dart';
import '../../../core/widget/receipt_tile_widget.dart';
import '../../../features/filter_drawer/state/filter.dart';
import '../widget/date_header_widget.dart';

class ReceiptController {
  Future<List<Widget>> getAnnotatedReceipts(FilterState filterState) async {
    final receipts = await ReceiptTable().query();
    receipts.sort((a, b) => a.date.compareTo(b.date));
    final reversedReceipts = receipts.reversed.toList();
    final annotatedReceipts = <Widget>[];
    int lastDay = -1;
    final minValue = filterState.minValue;
    final maxValue = filterState.maxValue;

    for (final receipt in reversedReceipts) {
      final currentDay = receipt.date.day;
      if (lastDay != currentDay) {
        lastDay = currentDay;
        annotatedReceipts.add(DateHeaderWidget(receipt.date));
        //debugPrint('Added DateHeaderWidget with lastDay != currentDay');
      }
      final totalReceiptValue = receipt.products.getTotalPrice();
      if (totalReceiptValue >= minValue &&
          totalReceiptValue <= maxValue &&
          isNotFiltered(filterState.searchInput, receipt)) {
        annotatedReceipts.add(ReceiptTileWidget(receipt));
      }
    }

    print(
        'Retrieved ${receipts.length} receipts, returning ${annotatedReceipts.length} annotated receipts');
    return annotatedReceipts;
  }

  Future<List<Widget>> getAnnotatedReceiptWithoutFilter() async {
    final receipts = await ReceiptTable().query();
    receipts.sort((a, b) => a.date.compareTo(b.date));
    final reversedReceipts = receipts.reversed.toList();
    final annotatedReceipts = <Widget>[];
    int lastDay = -1;

    for (final receipt in reversedReceipts) {
      final currentDay = receipt.date.day;
      if (lastDay != currentDay) {
        lastDay = currentDay;
        annotatedReceipts.add(DateHeaderWidget(receipt.date));
      }
      annotatedReceipts.add(ReceiptTileWidget(receipt));
    }

    print(
        'Retrieved ${receipts.length} receipts, returning ${annotatedReceipts.length} annotated receipts');
    return annotatedReceipts;
  }
}

bool isNotFiltered(String searchInput, Receipt receipt) {
  if (searchInput == '') {
    return true;
  }

  searchInput = searchInput.toLowerCase();

  if (receipt.store.name.toLowerCase().contains(searchInput)) {
    return true;
  }

  for (final product in receipt.products.content) {
    if (product.name!.toLowerCase().contains(searchInput)) {
      return true;
    }
  }
  return false;
}
