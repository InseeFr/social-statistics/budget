import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'package:bdf_survey/features/para_data/para_data_name.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_mlkit_text_recognition/google_mlkit_text_recognition.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:image/image.dart' as img;
import 'package:shared_preferences/shared_preferences.dart';

import '../../core/controller/util/responsive_ui.dart';
import '../../core/model/color_pallet.dart';
import '../../core/state/translations.dart';
import '../../features/spotlight_tutorial/controller/camera_entry_tutorial.dart';

GlobalKey keyButton1 = GlobalKey();
GlobalKey keyButton2 = GlobalKey();
GlobalKey keyButton3 = GlobalKey();
GlobalKey keyButton4 = GlobalKey();
late Translations translations;

class CameraPage extends StatefulWidget with ParaDataName {
  final bool isEditing;

  @override
  String get name => 'CameraEntryScreen';
  const CameraPage(this.isEditing);
  @override
  __CameraPageState createState() {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: ColorPallet.primaryColor,
    ));
    return __CameraPageState();
  }
}

class __CameraPageState extends State<CameraPage> {
  late CameraController _controller;
  late String _dirPath;
  final ImagePicker _picker = ImagePicker();
  late List<XFile> _images = [];
  late bool _isLoading = false;
  bool _isButtonPressed = false;

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    translations = Translations(context, 'Camera');
    initTargets(keyButton1, keyButton2, keyButton3, keyButton4);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    Future.delayed(Duration(milliseconds: 500), () {
      showInitialTutorial(context);
    });

    availableCameras().then((cameras) {
      _controller = CameraController(cameras[0], ResolutionPreset.low,
          enableAudio: false);
      _controller.initialize().then((_) {
        if (!mounted) {
          return;
        }
        setState(() {
          _images = [];
          _isLoading = false;
        });
      }).catchError((e) {
        print('Error initializing camera: $e');
        setState(() {
          _isLoading = false;
        });
      });
    });
  }

  Future<void> showInitialTutorial(BuildContext context) async {
    final prefs = await SharedPreferences.getInstance();
    final status = prefs.getBool('mainCamerasTutorial') ?? true;
    if (!status) {
      await prefs.setBool('mainCamerasTutorial', true);
      showTutorial(context);
    }
  }

  Future<void> _getImageFromCamera() async {
    final XFile? image =
        await _picker.pickImage(source: ImageSource.camera, imageQuality: 25);
    if (image == null) {
      setState(() {
        _isLoading = false;
      });
      return;
    }
    setState(() {
      _isLoading = true;
    });
    if (!await _isTextRecognized(XFile(image.path))) {
      setState(() {
        _isLoading = false;
      });
      return;
    }
    CroppedFile? croppedFile = await ImageCropper().cropImage(
      sourcePath: image.path,
      uiSettings: [
        AndroidUiSettings(
            toolbarTitle: 'Recadrer le ticket',
            toolbarColor: ColorPallet.primaryColor,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        IOSUiSettings(
          title: 'Recadrer le ticket',
        ),
        WebUiSettings(
          context: context,
        ),
      ],
    );
    if (croppedFile != null) {
      setState(() {
        _images = [..._images, XFile(croppedFile.path)];
        _isLoading = false;
      });
    } else {
      setState(() {
        _images = [..._images, XFile(image.path)];
        _isLoading = false;
      });
    }
  }

  Future<void> _getImageFromGallery() async {
    final XFile? image = await _picker.pickImage(
      source: ImageSource.gallery,
      imageQuality: 25,
    );
    if (image == null) {
      setState(() {
        _isLoading = false;
      });
      return;
    }
    setState(() {
      _isLoading = true;
    });
    if (!await _isTextRecognized(XFile(image.path))) {
      setState(() {
        _isLoading = false;
      });
      return;
    }
    CroppedFile? croppedFile = await ImageCropper().cropImage(
      sourcePath: image.path,
      uiSettings: [
        AndroidUiSettings(
            toolbarTitle: 'Recadrer le ticket',
            toolbarColor: ColorPallet.primaryColor,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        IOSUiSettings(
          title: 'Recadrer le ticket',
        ),
        WebUiSettings(
          context: context,
        ),
      ],
    );
    if (croppedFile != null) {
      setState(() {
        _images = [..._images, XFile(croppedFile.path)];
        _isLoading = false;
      });
    } else {
      setState(() {
        _images = [..._images, XFile(image.path)];
        _isLoading = false;
      });
    }
  }

  Future<bool> _isTextRecognized(XFile image) async {
    final TextRecognizer textRecognizer =
        TextRecognizer(script: TextRecognitionScript.latin);

    final RecognizedText recognizedText =
        await textRecognizer.processImage(InputImage.fromFilePath(image.path));
    if (recognizedText.text.isEmpty && recognizedText.blocks.isEmpty) {
      Fluttertoast.showToast(
        msg: translations.text('noTextInPhoto'),
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
      );
      return false;
    }

    return true;
  }

  Future<File?> _combineImages() async {
    try {
      List<img.Image> images = [];

      for (var imageFile in _images) {
        var image = img.decodeImage(await imageFile.readAsBytes());
        if (image != null) {
          var resizedImage = img.copyResize(image, width: 800);
          images.add(img.bakeOrientation(resizedImage));
        }
      }

      if (images.isEmpty) return null;

      int totalHeight = images.fold(0, (prev, image) => prev + image.height);
      int maxWidth = images.map((image) => image.width).reduce(max);

      img.Image combinedImage = img.Image(width: maxWidth, height: totalHeight);

      int offsetY = 0;
      for (var image in images) {
        img.compositeImage(combinedImage, image, dstX: 0, dstY: offsetY);
        offsetY += image.height;
      }

      final outputFile =
          File('${Directory.systemTemp.path}/${Random().nextInt(10000)}.jpeg');
      await outputFile.writeAsBytes(img.encodeJpg(combinedImage, quality: 50));

      return outputFile;
    } catch (e) {
      print('Error combining images: $e');
      return null;
    }
  }

  void _handleValidate() {
    if (!_controller.value.isInitialized || _images.isEmpty) {
      setState(() {
        _isButtonPressed = false;
      });
      return;
    }
    getApplicationDocumentsDirectory().then((extDir) async {
      _dirPath = '${extDir.path}/Pictures/receipts';
      await Directory(_dirPath).create(recursive: true);

      final result = await _combineImages();

      if (!mounted) return;

      setState(() {
        _isButtonPressed = false;
      });

      if (result != null) {
        Navigator.pop(context, result.path);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('Erreur lors du traitement des images'),
            duration: Duration(seconds: 2),
          ),
        );
      }
    }).catchError((error) {
      if (!mounted) return;

      setState(() {
        _isButtonPressed = false;
      });

      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Une erreur est survenue'),
          duration: Duration(seconds: 2),
        ),
      );
      print('Error in validation: $error');
    });
  }

  @override
  Widget build(BuildContext context) {
    final scrollController = ScrollController();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorPallet.primaryColor,
        iconTheme: const IconThemeData(color: Colors.white),
        title: Row(
          children: <Widget>[
            Text(
              'Photo du ticket',
              style: TextStyle(
                color: Colors.white,
                fontSize: 20.0 * f,
                fontWeight: FontWeight.w600,
              ),
            ),
            SizedBox(width: 10 * x),
            InkWell(
                onTap: () {
                  showTutorial(context);
                },
                child: Icon(Icons.info, color: Colors.white, size: 25.0 * x)),
          ],
        ),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 20 * x),
            child: _isButtonPressed
                ? const SizedBox(
                    width: 20,
                    height: 20,
                    child: CircularProgressIndicator(
                      color: Colors.white,
                      strokeWidth: 2,
                    ),
                  )
                : ElevatedButton(
                    key: keyButton4,
                    style: ElevatedButton.styleFrom(
                      backgroundColor: _images.isEmpty
                          ? ColorPallet.midGray
                          : ColorPallet.darkTextColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0 * x),
                      ),
                    ),
                    onPressed: _images.isEmpty ? null : _handleValidate,
                    child: Text(
                      'Valider',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16 * x,
                      ),
                    ),
                  ),
          )
        ],
      ),
      body: SizedBox(
        width: double.infinity,
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 15 * y, bottom: 10 * y),
                  height: 460 * y,
                  child: _images.isNotEmpty
                      ? Padding(
                          padding: EdgeInsets.symmetric(horizontal: 12 * x),
                          child: Scrollbar(
                            controller: scrollController,
                            thumbVisibility: true,
                            child: ListView.builder(
                              controller: scrollController,
                              scrollDirection: Axis.vertical,
                              itemCount: _images.length,
                              itemBuilder: (context, index) {
                                return Stack(
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.symmetric(
                                          horizontal: 20 * x, vertical: 7 * y),
                                      decoration: BoxDecoration(
                                        border: Border.all(width: 4),
                                        borderRadius: BorderRadius.circular(10),
                                        color: ColorPallet.midGray,
                                      ),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(6),
                                        child: Image.file(
                                            File(_images[index].path)),
                                      ),
                                    ),
                                    Positioned(
                                      top: -10,
                                      right: -20,
                                      child: Padding(
                                        padding: EdgeInsets.all(5.0),
                                        child: RawMaterialButton(
                                          onPressed: () {
                                            setState(() {
                                              _images.removeAt(index);
                                            });
                                          },
                                          child: Padding(
                                            padding: EdgeInsets.all(1.0),
                                            child: Icon(
                                              Icons.cancel,
                                              size: 22.0,
                                              color: Colors.white,
                                            ),
                                          ),
                                          shape: CircleBorder(),
                                          fillColor: ColorPallet.pink,
                                        ),
                                      ),
                                    ),
                                  ],
                                );
                              },
                            ),
                          ),
                        )
                      : _isLoading
                          ? Center(
                              child: CircularProgressIndicator(),
                            )
                          : Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                  key: keyButton3,
                                  'assets/images/no_items_found_icon.png',
                                  height: 85.0 * y,
                                  width: 85.0 * x,
                                ),
                                SizedBox(height: 10.0 * y),
                                Text(
                                  Translations.textStatic(
                                      'addInfo', 'Camera_Entry', null),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: ColorPallet.midGray,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14.0 * f,
                                  ),
                                ),
                              ],
                            ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: Stack(
                    alignment: Alignment.center,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.bottomLeft,
                        child: Padding(
                          padding: EdgeInsets.only(left: 60 * x),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              SizedBox(
                                key: keyButton2,
                                width: 60 * x,
                                height: 60 * x,
                                child: ElevatedButton(
                                  onPressed: _images.length > 2
                                      ? null
                                      : _getImageFromGallery,
                                  style: ElevatedButton.styleFrom(
                                    backgroundColor: _images.length > 2
                                        ? ColorPallet.midGray
                                        : Theme.of(context).primaryColor,
                                    shape: const CircleBorder(),
                                    padding: EdgeInsets.zero,
                                  ),
                                  child: const Icon(Icons.photo_library,
                                      size: 25, color: Colors.white),
                                ),
                              ),
                              SizedBox(height: 8.0),
                              Text(
                                'Galerie',
                                style: TextStyle(
                                    fontSize: 12.0,
                                    color: ColorPallet.darkTextColor),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            SizedBox(
                              key: keyButton1,
                              width: 90 * x,
                              height: 90 * x,
                              child: ElevatedButton(
                                onPressed: _images.length > 2
                                    ? null
                                    : _getImageFromCamera,
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: _images.length > 2
                                      ? ColorPallet.midGray
                                      : Theme.of(context).primaryColor,
                                  shape: const CircleBorder(),
                                  padding: EdgeInsets.zero,
                                ),
                                child: const Icon(Icons.photo_camera,
                                    size: 45, color: Colors.white),
                              ),
                            ),
                            SizedBox(height: 8.0),
                            Text(
                              'Camera',
                              style: TextStyle(
                                  fontSize: 16.0,
                                  color: ColorPallet.darkTextColor),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
