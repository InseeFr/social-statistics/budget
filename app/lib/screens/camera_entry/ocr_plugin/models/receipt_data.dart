import 'dart:typed_data';

import 'package:bdf_survey/screens/camera_entry/ocr_plugin/models/product_group.dart';

class ReceiptData {
  Uint8List originalImageData;
  Uint8List croppedImageData;

  ProductGroup totalValueContentLine;
  List<ProductGroup> products;

  ReceiptData(
    this.originalImageData,
    this.croppedImageData,
    this.totalValueContentLine,
    this.products,
  );
}
