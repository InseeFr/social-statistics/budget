// ignore_for_file: unnecessary_null_comparison

import 'package:bdf_survey/core/controller/util/responsive_ui.dart';
import 'package:bdf_survey/core/model/color_pallet.dart';
import 'package:bdf_survey/core/model/receipt.dart';
import 'package:bdf_survey/core/state/translations.dart';
import 'package:bdf_survey/screens/camera_entry/camera_screen.dart';
import 'package:bdf_survey/screens/camera_entry/controller/setup_receipt.dart';
import 'package:bdf_survey/screens/camera_entry/receipt_total_price.dart';
import 'package:bdf_survey/screens/manual_entry/manual_entry_screen.dart';
import 'package:bdf_survey/screens/receipt_list/state/receipt_list_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:permission_handler/permission_handler.dart';

MethodChannel channel = const MethodChannel('receipt_scanner');

Future<void> openReceiptScanner(BuildContext context, DateTime date,
    {double qualityTreshold = 0.2}) async {
  final translations = Translations(context, 'ReceiptOCR');

  var status = await Permission.camera.status;
  if (status.isDenied) {
    status = await Permission.camera.request();
  }
  if (status.isPermanentlyDenied) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Permission manquante'),
          content: Text(
              'L\'application a besoin d\'accéder à la caméra pour scanner les reçus.',
              style: TextStyle(fontSize: 16 * f)),
          actions: <Widget>[
            TextButton(
              child: Text('Annuler',
                  style: TextStyle(color: ColorPallet.darkGreyColor)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('Accéder aux paramètres'),
              onPressed: () {
                openAppSettings();
              },
            ),
          ],
        );
      },
    );
    return;
  }

  if (!status.isGranted) {
    _showToast(translations.text('noPermission'));
    return;
  }

  disabledOcr(context, translations, date);
}

void disabledOcr(
    BuildContext context, Translations translations, DateTime date) async {
  final imagePath = await getBasicReceiptPhoto(context);
  if (imagePath == null) {
    _showToast(translations.text('canceledOcr'));
    return null;
  }
  final receiptCategory = ProductCategory('Ticket de caisse', '16');

  final totalReceiptPrice = await _getReceiptTotalPrice(context, imagePath);
  if (totalReceiptPrice == null) {
    _showToast(translations.text('canceledOcr'));
    return null;
  }

  final receipt =
      await getBasicReceipt(totalReceiptPrice, imagePath, receiptCategory);

  receipt.date = date;

  _pushModificationScreen(context, receipt);

  ReceiptListState.of(context).notifyListeners();
}

void _pushModificationScreen(BuildContext context, Receipt receipt) {
  Navigator.push(
    context,
    MaterialPageRoute(
      settings: const RouteSettings(name: 'ManualEntryScreen'),
      builder: (context) =>
          ManualEntryScreen(receipt, false, true, receipt.date),
    ),
  );
}

void _showToast(String msg) {
  Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      fontSize: 16.0 * f);
}

class ProductCategory {
  String category;
  String coicop;
  ProductCategory(this.category, this.coicop);
}

Future<String> getBasicReceiptPhoto(BuildContext context) async {
  final imagePath = await Navigator.push(
    context,
    MaterialPageRoute(
      settings: const RouteSettings(name: 'RegularCameraScreen'),
      builder: (context) => const CameraPage(false),
    ),
  );
  return imagePath;
}

Future<double> _getReceiptTotalPrice(
    BuildContext context, String imagePath) async {
  final receiptTotalPrice = await Navigator.push(
    context,
    MaterialPageRoute(
      settings: const RouteSettings(name: 'TotalReceiptPriceScreen'),
      builder: (context) => ReceiptTotalPriceScreen(
        imagePath: imagePath,
      ),
    ),
  );
  return receiptTotalPrice;
}
