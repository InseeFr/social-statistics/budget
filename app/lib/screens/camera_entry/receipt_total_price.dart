import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_mlkit_text_recognition/google_mlkit_text_recognition.dart';
import '../../core/controller/util/responsive_ui.dart';
import '../../core/model/color_pallet.dart';
import '../../core/state/translations.dart';

class ReceiptTotalPriceScreen extends StatefulWidget {
  final String imagePath;

  const ReceiptTotalPriceScreen({required this.imagePath});

  @override
  _ReceiptTotalPriceScreenState createState() =>
      _ReceiptTotalPriceScreenState();
}

class _ReceiptTotalPriceScreenState extends State<ReceiptTotalPriceScreen> {
  double _price = 0.0;
  bool _isValidRecipe = false;

  final controller = ScrollController();
  @override
  void initState() {
    super.initState();
    _isTextRecognized().then((value) {
      setState(() {
        _isValidRecipe = value;
      });
    });
  }

  String getDescription() {
    switch (LanguageSetting.key) {
      case 'nl':
        return 'Voer het totaal bedrag in:';

      case 'be_fr':
        return 'Saisissez le montant total:';

      case 'fr':
        return 'Saisissez le montant total:';

      default:
        return 'Enter the total amount:';
    }
  }

  Future<bool> _isTextRecognized() async {
    final InputImage inputImage = InputImage.fromFilePath(widget.imagePath);
    final TextRecognizer textRecognizer =
        TextRecognizer(script: TextRecognitionScript.latin);

    final RecognizedText recognizedText =
        await textRecognizer.processImage(inputImage);

    if (recognizedText.text.isEmpty && recognizedText.blocks.isEmpty) {
      Fluttertoast.showToast(
        msg: Translations.textStatic('noTextInPhoto', 'Camera', null),
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
      );
      return false;
    } else {
      return true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: ColorPallet.primaryColor,
          iconTheme: const IconThemeData(color: Colors.white),
          title: Row(
            children: <Widget>[
              Text(
                Translations(context, 'Manual_Entry').text('totalAmount'),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 22.0 * f,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
        ),
        body: SingleChildScrollView(
          controller: controller,
          child: Center(
            child: SizedBox(
              height: 600 * y,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                      height: 480 * y,
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 12 * x),
                        child: Scrollbar(
                          controller: controller,
                          thumbVisibility: true,
                          child: SingleChildScrollView(
                            child: Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: 20 * x, vertical: 7 * y),
                              decoration: BoxDecoration(
                                border: Border.all(width: 4),
                                borderRadius: BorderRadius.circular(10),
                                color: ColorPallet.midGray,
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(6),
                                child: Image.file(File(widget.imagePath)),
                              ),
                            ),
                            scrollDirection: Axis.vertical,
                          ),
                        ),
                      )),
                  Visibility(
                    visible: _isValidRecipe,
                    child: Column(
                      children: [
                        Text(
                          getDescription(),
                          style: TextStyle(
                            fontSize: 18 * f,
                            color: ColorPallet.darkTextColor,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        SizedBox(height: 2 * y),
                        PriceInput(price: _price),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }
}

class PriceInput extends StatefulWidget {
  final double price;
  //final Function(double) onChanged;

  const PriceInput({Key? key, required this.price}) : super(key: key);
  @override
  _PriceInputState createState() => _PriceInputState();
}

class _PriceInputState extends State<PriceInput> {
  late double _price;
  late TextEditingController _controller;

  @override
  void initState() {
    super.initState();
    _price = widget.price;
    _controller = TextEditingController(text: widget.price.toString());
  }

  @override
  void didUpdateWidget(covariant PriceInput oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.price != widget.price) {
      _controller.text = widget.price.toString();
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  bool isValidInput(double newPrice) {
    debugPrint('newPrice: $newPrice');
    try {
      if (newPrice > 0 && newPrice < 10000000) {
        return true;
      }
    } on Exception {
      print('Input error: $Exception');
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    final translations = Translations(context, 'Manual_Entry');

    return Column(
      children: [
        Container(
          width: MediaQuery.of(context).size.width * 2,
          margin: EdgeInsets.symmetric(horizontal: 18 * x, vertical: 5 * y),
          decoration: BoxDecoration(
              border: Border.all(color: ColorPallet.lightGray, width: 1.7 * x),
              borderRadius: BorderRadius.circular(12.0 * x)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              //SizedBox(width: 10.0 * x),
              Icon(Icons.local_offer,
                  color: ColorPallet.darkTextColor, size: 27 * x),
              Container(
                width: 190 * x,
                height: 42 * y,
                child: TextField(
                  controller: _controller,
                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                  style: TextStyle(
                      color: ColorPallet.darkTextColor,
                      fontWeight: FontWeight.w500,
                      fontSize: 18 * f),
                  autocorrect: false,
                  onTap: () {
                    _controller.clear();
                  },
                  onChanged: (String newPrice) {
                    newPrice = newPrice.replaceAll(',', '.');
                    setState(() {
                      _price = double.parse(newPrice);
                    });
                  },
                  decoration: InputDecoration(
                    hintStyle: TextStyle(
                        color: ColorPallet.midGray,
                        fontWeight: FontWeight.w500,
                        fontSize: 18 * f),
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.all(10 * x),
                    prefix: Padding(
                      padding: EdgeInsets.only(right: 5.0 * x),
                      child: Text(Translations.textStatic(
                          'currencySymbol', 'CurrencySetting', null)),
                    ),
                    prefixStyle:
                        const TextStyle(color: ColorPallet.darkTextColor),
                  ),
                ),
              ),
              Container(
                child: ElevatedButton(
                  onPressed: () {
                    print('final price: $_price');
                    if (isValidInput(_price)) {
                      Navigator.pop(context, _price);
                    } else {
                      Fluttertoast.showToast(
                        msg: translations.text('invalidInput'),
                        toastLength: Toast.LENGTH_LONG,
                        gravity: ToastGravity.BOTTOM,
                      );
                    }
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: ColorPallet.darkTextColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12.0 * x),
                    ),
                  ),
                  child: Text(
                    Translations(context, 'Questionnaire')
                        .text('confirmButtonText'),
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16 * x,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
