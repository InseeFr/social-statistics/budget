import 'package:bdf_survey/screens/camera_entry/ocr_plugin/models/receipt_data.dart';
import 'package:bdf_survey/screens/camera_entry/ocr_plugin/receipt_scanner.dart';


import '../../../core/model/receipt.dart';
import '../../../core/model/receipt_product.dart';
import '../../../core/state/translations.dart';
import '../../insights/controller/util/type_converter.dart';

import 'util/save_image_bytes.dart';

Future<Receipt?> initialize_receipt(ReceiptData receiptData,
    ProductCategory receiptCategory, double receiptQualityScore) async {
  final receipt = Receipt.empty();
  final croppedImageLocation = await saveImageBytes(receiptData.croppedImageData, receiptQualityScore);
  receipt.imagePath = croppedImageLocation;
  receipt.date = DateTime.now();
  for (final _product in receiptData.products) {
    final product = ReceiptProduct(
      name: _product.getMainProduct()!.text,
      price: _product.getMainPrice(),
      category: receiptCategory.category,
      coicop: receiptCategory.coicop,
    );
    if (product.name is! String && product.price is! double && product.category is! String && product.coicop is! String) {
      return null;
    }
    receipt.products.content.add(product);
  }
  return receipt;
}

Future<Receipt> getBasicReceipt(double price, String imagePath, ProductCategory productCategory) async {
  final receipt = Receipt.empty();
  receipt.imagePath = imagePath;

  final receiptProduct = ReceiptProduct(
    name: '',
    price: 0,
    category: '',
    coicop: '',
    isReturn: false,
    hasDiscount: false,
    count: 1,
    productDate: dateTimeToString(DateTime.now()),
  );
  receiptProduct.name = productCategory.category;
  receiptProduct.price = price;
  receiptProduct.category = productCategory.category;
  receiptProduct.coicop = productCategory.coicop;

  receipt.products.content.add(receiptProduct);

  return receipt;
}

String getTranslationReceipt() {
  switch (LanguageSetting.key) {
    case 'nl':
      return 'Foto van kassabon';
    
    case 'be_fr':
      return 'Photo du reçu';
      
    case 'fr':
      return 'Photo du reçu';
      
    default:
      return 'Photo of receipt';
  }
}
