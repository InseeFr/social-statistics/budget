import 'dart:io';
import 'dart:typed_data';

import 'package:path_provider/path_provider.dart';
import 'package:uuid/uuid.dart';

Future<String> saveImageBytes(Uint8List imageBytes, double receiptQualityScore) async {
  final imageName = '${const Uuid().v1()}&score:${receiptQualityScore.toString()}';
  final dir = (await getApplicationDocumentsDirectory()).path;
  final fullPath = '$dir/$imageName.png';
  final file = File(fullPath);

  await file.writeAsBytes(imageBytes);
  return file.path;
}
