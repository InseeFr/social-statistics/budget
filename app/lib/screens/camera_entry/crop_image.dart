import 'dart:io';
import 'dart:math';

import 'package:bdf_survey/core/model/color_pallet.dart';
import 'package:bdf_survey/core/state/translations.dart';
import 'package:bdf_survey/screens/camera_entry/camera_screen.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_mlkit_text_recognition/google_mlkit_text_recognition.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image/image.dart' as img;
import '../../core/controller/util/responsive_ui.dart';

class CropImagePage extends StatefulWidget {
  final String imagePath;
  final File imageFile;

  CropImagePage({required this.imagePath, required this.imageFile});

  @override
  _CropImagePageState createState() => _CropImagePageState();
}

class _CropImagePageState extends State<CropImagePage> {
  File? _croppedImageFile;
  bool _isValidRecipe = false;
  bool _showHint = false;

  @override
  void initState() {
    super.initState();
    _isTextRecognized().then((value) {
      setState(() {
        _isValidRecipe = value;
      });
    });
  }

  void _handleInfoPressed(Size size) {
    setState(() {
      _showHint = !_showHint;
    });
    if (_showHint) {
      setState(() {
        _appBarHeight = size.height;
      });
    } else {
      setState(() {
        _appBarHeight = kToolbarHeight;
      });
    }
  }

  double _appBarHeight = kToolbarHeight;

  Future<bool> _isTextRecognized() async {
    final InputImage inputImage = InputImage.fromFilePath(widget.imagePath);
    final TextRecognizer textRecognizer =
        TextRecognizer(script: TextRecognitionScript.latin);

    final RecognizedText recognizedText =
        await textRecognizer.processImage(inputImage);

    if (recognizedText.text.isEmpty && recognizedText.blocks.isEmpty) {
      Fluttertoast.showToast(
        msg: Translations.textStatic('noTextInPhoto', 'Camera', null),
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb:
            2147483647, // Set to a large number to make it permanent
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0,
      );
      return false;
    } else {
      return true;
    }
  }

  Future<void> _cropImage() async {
    CroppedFile? croppedFile = await ImageCropper().cropImage(
      sourcePath: _croppedImageFile?.path ?? widget.imageFile.path,
      uiSettings: [
        AndroidUiSettings(
            toolbarTitle: Translations.textStatic('cropPhoto', 'Camera', null),
            toolbarColor: ColorPallet.primaryColor,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        IOSUiSettings(
          title: Translations.textStatic('cropPhoto', 'Camera', null),
        ),
        WebUiSettings(
          context: context,
        ),
      ],
    );

    if (croppedFile != null) {
      setState(() {
        _croppedImageFile = File(croppedFile.path);
      });
    } else {
      setState(() {
        _croppedImageFile =
            File(_croppedImageFile?.path ?? widget.imageFile.path);
      });
    }
  }

  Future<File> _combineImages(File image1File, File image2File) async {
    // Decode the images to img.Image objects and correct their orientation
    img.Image? image1 =
        img.bakeOrientation(img.decodeImage(await image1File.readAsBytes())!);
    img.Image? image2 =
        img.bakeOrientation(img.decodeImage(await image2File.readAsBytes())!);

    image1 = img.copyRotate(image1, angle: 90);
    image2 = img.copyRotate(image2, angle: 90);

    img.Image combinedImage = img.Image(
        width: max(image1.width, image2.width),
        height: image1.height + image2.height);

    img.compositeImage(combinedImage, image1, dstX: 0, dstY: 0);
    img.compositeImage(combinedImage, image2, dstX: 0, dstY: image1.height);

    File outputFile =
        File('${Directory.systemTemp.path}/${Random().nextInt(10000)}.png');

    await outputFile.writeAsBytes(img.encodePng(combinedImage));

    return outputFile;
  }

  Future<void> _addImage() async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => CameraPage(false)),
    );

    if (result != null) {
      final outputImage = await _combineImages(
          _croppedImageFile ?? widget.imageFile, File(result));
      setState(() {
        _croppedImageFile = outputImage;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CropImageAppBar(
        onInfoPressed: _handleInfoPressed,
        appBarHeight: _appBarHeight,
      ),
      body: Column(
        children: [
          Expanded(
            child: Center(
              child: _croppedImageFile != null
                  ? Image.file(_croppedImageFile!)
                  : Image.file(widget.imageFile),
            ),
          ),
          Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: ColorPallet.darkTextColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0 * x),
                  ),
                ),
                onPressed: _addImage,
                child: Container(
                  margin: EdgeInsets.all(8),
                  child: Text(
                    Translations.textStatic('addPhoto', 'Camera', null),
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18 * x,
                    ),
                  ),
                ),
              )),
          Visibility(
            visible: _isValidRecipe,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: ColorPallet.darkTextColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0 * x),
                    ),
                  ),
                  onPressed: _cropImage,
                  child: Container(
                    margin: EdgeInsets.all(8),
                    child: Text(
                      Translations.textStatic('cropPhoto', 'Camera', null),
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18 * x,
                      ),
                    ),
                  ),
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: ColorPallet.darkTextColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0 * x),
                    ),
                  ),
                  onPressed: () {
                    if (mounted) {
                      _croppedImageFile != null
                          ? Navigator.pop(context, _croppedImageFile)
                          : Navigator.pop(context, widget.imageFile);
                    }
                  },
                  child: Container(
                    margin: EdgeInsets.all(8),
                    child: Text(
                      'Valider',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18 * x,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 10 * y),
        ],
      ),
    );
  }
}

class CropImageAppBar extends StatefulWidget implements PreferredSizeWidget {
  final void Function(Size) onInfoPressed;
  final double appBarHeight;

  const CropImageAppBar({
    Key? key,
    required this.onInfoPressed,
    required this.appBarHeight,
  }) : super(key: key);

  @override
  _CropImageAppBarState createState() => _CropImageAppBarState();

  @override
  Size get preferredSize => Size.fromHeight(appBarHeight);
}

class _CropImageAppBarState extends State<CropImageAppBar> {
  bool _showHint = false;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: ColorPallet.primaryColor,
      iconTheme: const IconThemeData(color: Colors.white),
      title: Text(
        Translations.textStatic('cropPhoto', 'Camera', null),
        style: TextStyle(
          color: Colors.white,
          fontSize: 22.0 * f,
          fontWeight: FontWeight.w600,
        ),
      ),
      actions: [
        IconButton(
          onPressed: () {
            setState(() {
              _showHint = !_showHint;
            });
            if (_showHint) {
              widget.onInfoPressed(Size.fromHeight(kToolbarHeight + 20.0));
            } else {
              widget.onInfoPressed(Size.fromHeight(kToolbarHeight));
            }
          },
          icon: Icon(Icons.info, color: Colors.white, size: 28.0 * x),
        ),
      ],
      bottom: _showHint
          ? PreferredSize(
              preferredSize: Size.fromHeight(20.0),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                child: Text(
                  Translations.textStatic('cameraHint7', 'Camera', null),
                  style: TextStyle(
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                ),
              ),
            )
          : null,
    );
  }
}
