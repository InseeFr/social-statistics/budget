import 'package:bdf_survey/screens/search/controller/add_location.dart';
import 'package:bdf_survey/screens/search/controller/add_search_suggestion.dart';
import 'package:bdf_survey/screens/settings/state/settings_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../core/controller/util/input.dart';
import '../../core/controller/util/responsive_ui.dart';
import '../../core/model/color_pallet.dart';
import '../../core/state/translations.dart';
import '../../core/widget/draggable_scrollbar.dart';
import '../../features/para_data/para_data_name.dart';
import 'controller/string_match.dart';
import 'controller/util/sort_results.dart';
import 'model/match.dart';

late Translations translations;

class SearchLocationWidget extends StatefulWidget with ParaDataName {
  const SearchLocationWidget();

  @override
  String get name => 'SearchLocationWidget';

  @override
  State<StatefulWidget> createState() {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: ColorPallet.primaryColor,
    ));
    return __SearchWidgetState();
  }
}

class __SearchWidgetState extends State<SearchLocationWidget> {
  late FocusNode myFocusNode;
  late List<SearchMatch> results;
  String searchString = '';

  final _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    myFocusNode = FocusNode();
  }

  bool inputChecker() {
    searchString = InputUtil.textSanitizer(searchString);
    if (!InputUtil.textValidator(searchString) || searchString.isEmpty) {
      Fluttertoast.showToast(
        msg: translations.text('invalidInput'),
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
      );
      setState(() {
        _controller.clear();
        FocusScope.of(context).requestFocus(myFocusNode);
      });
      return false;
    } else {
      return true;
    }
  }

  String formatSearchString(String searchStringTest) {
    if (searchStringTest.isNotEmpty) {
      return searchString[0].toUpperCase() + searchString.substring(1);
    } else {
      return '';
    }
  }

  Future<List<Map<String, dynamic>>> getSearchResults(
      BuildContext context) async {
    return SearchSuggestions.getMostRecentLocation();
  }

  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Search');
    final _arrowsController = ScrollController();
    return PopScope(
      canPop: true,
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            Positioned(
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      color: ColorPallet.primaryColor,
                    ),
                  ),
                  Expanded(
                    child: Container(
                      color: Colors.white,
                    ),
                  )
                ],
              ),
            ),
            Positioned(
              child: SafeArea(
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 73.0 * y,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          border: Border.all(
                              width: 13.0 * x, color: ColorPallet.primaryColor),
                        ),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                alignment: Alignment.centerLeft,
                                child: TextField(
                                  autocorrect: false,
                                  focusNode: myFocusNode,
                                  keyboardType: TextInputType.text,
                                  onSubmitted: (value) {},
                                  onChanged: (value) {
                                    setState(() {
                                      searchString = value;
                                    });
                                  },
                                  autofocus: true,
                                  controller: _controller,
                                  style: TextStyle(
                                      color: ColorPallet.darkTextColor,
                                      fontSize: 20.0 * f,
                                      fontWeight: FontWeight.w500),
                                  decoration: InputDecoration(
                                    hintText:
                                        'Rechercher une commune par nom ou code postal',
                                    hintStyle: TextStyle(
                                        color: ColorPallet.midGray,
                                        fontSize: 18.0 * f),
                                    prefixIcon: InkWell(
                                      onTap: () {
                                        SystemChrome.setSystemUIOverlayStyle(
                                          const SystemUiOverlayStyle(
                                            statusBarColor:
                                                ColorPallet.primaryColor,
                                          ),
                                        );
                                        Navigator.of(context).pop();
                                      },
                                      child: Icon(
                                        Icons.arrow_back,
                                        color: ColorPallet.midGray,
                                        size: 24 * x,
                                      ),
                                    ),
                                    border: InputBorder.none,
                                  ),
                                ),
                              ),
                            ),
                            if (searchString.isEmpty)
                              Container()
                            else
                              InkWell(
                                onTap: () {
                                  if (inputChecker()) {
                                    if (searchString.isNotEmpty) {
                                      showDialog(
                                          context: context,
                                          routeSettings: const RouteSettings(
                                              name: 'SearchNotFoundDialog'),
                                          builder: (BuildContext context) {
                                            return _SearchNotFoundDialog(
                                                searchString);
                                          });
                                    }
                                  }
                                },
                                child: Container(
                                  width: 110 * x,
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 10 * x, vertical: 10 * y),
                                  decoration: BoxDecoration(
                                      color: ColorPallet.primaryColor,
                                      borderRadius:
                                          BorderRadius.circular(10 * x),
                                      boxShadow: [
                                        BoxShadow(
                                          color: ColorPallet.lightGray,
                                          offset: Offset(1.0 * x, 1.0 * y),
                                          blurRadius: 1.5 * x,
                                          spreadRadius: 1.5 * x,
                                        )
                                      ]),
                                  child: Center(
                                    child: Text(
                                        '${translations.text('notFound')}?',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 13 * f,
                                            fontWeight: FontWeight.w500)),
                                  ),
                                ),
                              ),
                          ],
                        ),
                      ),
                      SizedBox(height: 4 * y),
                      Expanded(
                        child: Listener(
                          onPointerDown: (v) {
                            myFocusNode.unfocus();
                          },
                          child: FutureBuilder(
                            future: getLocationMatches(
                                searchString.isNotEmpty ? searchString : '',
                                SortOrder.wordLengthThenFrequency,
                                1000,
                                20),
                            builder: (BuildContext context,
                                AsyncSnapshot<List<SearchMatch>> snapshot) {
                              if (!snapshot.hasData ||
                                  snapshot.data!.isEmpty ||
                                  searchString.isEmpty) {
                                //debugPrint('No search results');
                                return _QuickSuggestionList();
                              }
                              results = snapshot.data!;
                              if (!snapshot.hasData) {
                                return const Center(
                                  child: CircularProgressIndicator(),
                                );
                              } else {
                                return DraggableScrollbar.rrect(
                                  alwaysVisibleScrollThumb: true,
                                  heightScrollThumb: 50,
                                  backgroundColor: Colors.grey,
                                  padding: EdgeInsets.only(right: 4.0 * x),
                                  controller: _arrowsController,
                                  child: ListView.builder(
                                    controller: _arrowsController,
                                    itemCount: snapshot.data!.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return _SearchResultTile(
                                        //CityName
                                        snapshot.data![index].code,
                                        //DisplayText
                                        snapshot.data![index].name,
                                        //PostalCode
                                        snapshot.data![index].category,
                                      );
                                    },
                                  ),
                                );
                              }
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _QuickSuggestionList extends StatelessWidget {
  const _QuickSuggestionList();

  Future<List<Map<String, dynamic>>> getSearchResults(
      BuildContext context) async {
    return SearchSuggestions.getMostRecentLocation();
  }

  @override
  Widget build(BuildContext context) {
    final _arrowsController = ScrollController();
    return ScopedModelDescendant<SettingState>(
      builder: (context, child, model) => FutureBuilder(
        future: getSearchResults(context),
        builder: (BuildContext context,
            AsyncSnapshot<List<Map<String, dynamic>>> snapshot) {
          if (!snapshot.hasData || snapshot.data!.isEmpty) {
            return Container();
          }

          return Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    translations.text('recentCities'),
                    style: TextStyle(
                      color: ColorPallet.primaryColor,
                      fontWeight: FontWeight.w600,
                      fontSize: 16 * f,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: DraggableScrollbar.rrect(
                  alwaysVisibleScrollThumb: true,
                  heightScrollThumb: 50,
                  backgroundColor: Colors.grey,
                  padding: EdgeInsets.only(right: 4.0 * x),
                  controller: _arrowsController,
                  child: ListView.builder(
                    controller: _arrowsController,
                    itemCount: snapshot.data!.length,
                    itemBuilder: (BuildContext context, int index) {
                      return _SearchResultTile(
                        snapshot.data![index]['cityName'],
                        snapshot.data![index]['displayText'],
                        snapshot.data![index]['postalCode'],
                      );
                    },
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}

class _SearchResultTile extends StatelessWidget {
  const _SearchResultTile(this.cityName, this.displayText, this.postalCode);

  final String cityName;
  final String displayText;
  final String postalCode;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(),
        InkWell(
          onTap: () {
            SearchSuggestions.addLocation(cityName, postalCode, displayText);
            Navigator.pop(context, [cityName, postalCode, displayText]);
          },
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 5,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.symmetric(
                          horizontal: 17 * x, vertical: 2 * y),
                      padding: EdgeInsets.symmetric(vertical: 5 * y),
                      child: Text(
                        displayText,
                        style: TextStyle(
                            fontSize: 16 * f,
                            fontWeight: FontWeight.w700,
                            color: ColorPallet.darkTextColor),
                      ),
                    ),
                    SizedBox(height: 3 * y),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 17 * x, vertical: 2 * y),
          width: MediaQuery.of(context).size.width,
          height: 1 * y,
          color: ColorPallet.lightGray,
        )
      ],
    );
  }
}

class _SearchNotFoundDialog extends StatefulWidget {
  final String searchString;

  const _SearchNotFoundDialog(this.searchString);

  @override
  __SearchNotFoundDialogState createState() => __SearchNotFoundDialogState();
}

class __SearchNotFoundDialogState extends State<_SearchNotFoundDialog> {
  String? cityName;
  String postalCode = '';

  late TextEditingController _cityController;
  late TextEditingController _postalCodeController;

  @override
  void initState() {
    super.initState();
    setState(() {
      cityName = widget.searchString;
    });
    _cityController = TextEditingController(text: cityName);
    _postalCodeController = TextEditingController(text: postalCode);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      titlePadding: const EdgeInsets.all(0),
      backgroundColor: ColorPallet.white,
      surfaceTintColor: Colors.transparent,
      title: Container(
        color: ColorPallet.primaryColor,
        padding: EdgeInsets.symmetric(horizontal: 25 * x, vertical: 20 * y),
        child: Center(
          child: Text(translations.text('addYourself'),
              style: TextStyle(
                  fontSize: 20 * f,
                  fontWeight: FontWeight.w500,
                  color: Colors.white)),
        ),
      ),
      content: Container(
        height: 150 * y,
        child: Column(
          children: <Widget>[
            SizedBox(height: 20 * y),
            TextField(
              controller: _cityController,
              autocorrect: false,
              onChanged: (value) {
                setState(() {
                  cityName = value;
                });
              },
              decoration: InputDecoration(
                labelText: 'Nom de commune',
                border: OutlineInputBorder(),
                floatingLabelBehavior: FloatingLabelBehavior.always,
              ),
            ),
            SizedBox(
              height: 10 * y,
            ),
            TextField(
              controller: _postalCodeController,
              autocorrect: false,
              keyboardType: TextInputType.number,
              inputFormatters: [
                FilteringTextInputFormatter.digitsOnly,
                LengthLimitingTextInputFormatter(5),
              ],
              decoration: InputDecoration(
                labelText: 'Code postal',
                border: OutlineInputBorder(),
                floatingLabelBehavior: FloatingLabelBehavior.always,
              ),
              onChanged: (value) {
                setState(() {
                  postalCode = value;
                });
              },
            ),
          ],
        ),
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text(translations.text('cancel'),
              style: TextStyle(
                color: ColorPallet.darkTextColor,
                fontSize: 14 * f,
              )),
        ),
        TextButton(
          onPressed: () {
            if (_postalCodeController.text.length == 5) {
              final displayText =
                  (cityName?.toUpperCase() ?? '') + ', ' + postalCode;

              LocationController()
                  .addNewLocation(cityName ?? '', postalCode, displayText);
              SearchSuggestions.addLocation(
                  cityName ?? '', postalCode, displayText);

              Navigator.of(context).pop();
              Navigator.pop(context, [cityName, postalCode, displayText]);
            } else {
              Fluttertoast.showToast(
                msg: translations
                    .text('Le code postal doit contenir 5 chiffres'),
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIosWeb: 1,
              );
            }
          },
          child: Text(translations.text('add'),
              style: TextStyle(
                color: ColorPallet.darkTextColor,
                fontSize: 14 * f,
              )),
        ),
      ],
    );
  }
}
