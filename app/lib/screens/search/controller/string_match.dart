import 'package:bdf_survey/screens/search/model/match.dart';

import 'util/edit_distance.dart';
import 'util/load_match_list.dart';
import 'util/sort_results.dart';

Future<List<SearchMatch>> getMatches(
  String userInput,
  bool isProductSearch,
  SortOrder sortOrder,
  int examplesPerCategory,
  int numberOfResults,
) async {
  //debugPrint('getMatches for user input: $userInput');
  final _matchs =
      isProductSearch ? await getProductMatchList() : await getShopMatchList();
  final results = <SearchMatch>[];

  //Compute edit distance between user input and all product names
  final matchs = <SearchMatch>[];
  for (final match in _matchs) {
    final distance = getEditDistance(match.name, userInput);
    if (distance < 1) {
      match.editDistance = distance;
      matchs.add(match);
    }
  }

  //Sort matches
  sortMatchs(matchs, sortOrder);

  //Select [examplesPerCategory] examples per category, and [numberOfResults] results in total
  final _categories = <String>[];
  for (final match in matchs) {
    if (results.length < numberOfResults) {
      if (countOccurences(_categories, match.category) < examplesPerCategory) {
        _categories.add(match.category);
        results.add(match);
      }
    }
  }

  return results;
}

Future<List<SearchMatch>> getLocationMatches(
  String userInput,
  SortOrder sortOrder,
  int examplesPerCategory,
  int numberOfResults,
) async {
  final _matchs = await getLocationMatchList();

  if (userInput.isEmpty) {
    print('User input is empty');
    return _matchs;
  }

  final results = <SearchMatch>[];

  final matchs = <SearchMatch>[];

  bool isNumeric(String s) {
    return double.tryParse(s) != null;
  }

  for (final match in _matchs) {
    double distance;

    if (isNumeric(userInput)) {
      distance = getEditDistanceForLocation(match.category, userInput);
    } else {
      distance = getEditDistance(match.code, userInput);
    }
    if (distance < 1) {
      match.editDistance = distance;
      matchs.add(match);
    }
  }

  sortMatchs(matchs, sortOrder);
  final _categories = <String>[];
  for (final match in matchs) {
    if (results.length < numberOfResults) {
      if (countOccurences(_categories, match.category) < examplesPerCategory) {
        _categories.add(match.category);
        results.add(match);
      }
    }
  }
  return results;
}

int countOccurences(List<String> categories, String category) {
  final count = categories.where((String c) => c == category).toList().length;
  return count;
}
