import 'package:bdf_survey/core/data/database/tables/location.dart';

import 'util/load_match_list.dart';

class LocationController {
  void addNewLocation(
      String cityName, String postalCode, String displayText) async {
    await TableLocation().insert(cityName, postalCode, displayText);
    loadLocationtMatchList();
  }
}
