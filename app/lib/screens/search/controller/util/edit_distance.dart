import 'dart:math';

import 'package:bdf_survey/core/controller/util/searchDistance.dart';

import 'string_extensions.dart';

double getEditDistance(String productName, String userInput) {
  var score = 0.0;

  productName = productName.toLowerCase().withoutDiacriticalMarks;
  userInput = userInput.toLowerCase().withoutDiacriticalMarks;

  if (userInput.contains(' ')) {
    score =
        StringDistance.normalizedLevenshteinDistance(userInput, productName);
    //score = stringDistanceMetric.normalizedDistance(userInput, productName);
  } else {
    final wordScores = <double>[];
    for (final productWord in productName.split(' ')) {
      final _wordScore =
          StringDistance.normalizedLevenshteinDistance(userInput, productWord);
      wordScores.add(_wordScore);
    }
    score = wordScores.reduce(min);
  }
  return score;
}

// Calcultate the distance if you want results for productName strictly starting with userInput

double getEditDistanceForLocation(String category, String userInput) {
  var score = double.infinity;
  userInput = userInput.toLowerCase().withoutDiacriticalMarks;
  category = category.toLowerCase().withoutDiacriticalMarks;


  final wordScores = <double>[];

  if (category.startsWith(userInput)) {
    final _wordScore =
        StringDistance.normalizedLevenshteinDistance(userInput, category);
    wordScores.add(_wordScore);
  }
  if (wordScores.isNotEmpty) {
    score = wordScores.reduce(min);
  }
  
  
  return score;
}
