import 'package:bdf_survey/core/data/database/tables/shop.dart';

import 'util/load_match_list.dart';

class ShopController {
  void addNewShop(String store) async {
    await TableShop().insert(store);
    loadShopMatchList();
  }
}
