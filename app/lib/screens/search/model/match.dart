
class SearchMatch {
  final String code;
  final int frequency;
  final String name;
  final String category;

  double? editDistance;

  SearchMatch({
    required this.code,
    required this.frequency,
    required this.name,
    required this.category,
  });
  @override
  String toString() {
    return 'SearchMatch(code: $code, frequency: $frequency, name: $name, category: $category, editDistance: $editDistance)';
  }
}
