import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../core/controller/util/input.dart';
import '../../core/controller/util/responsive_ui.dart';
import '../../core/model/color_pallet.dart';
import '../../core/state/translations.dart';
import '../../core/widget/draggable_scrollbar.dart';
import '../../features/para_data/para_data_name.dart';
import '../settings/state/settings_state.dart';
import 'controller/add_product.dart';
import 'controller/add_search_suggestion.dart';
import 'controller/add_shop.dart';
import 'controller/string_match.dart';
import 'controller/util/sort_results.dart';
import 'model/match.dart';

late Translations translations;

class SearchWidget extends StatefulWidget with ParaDataName {
  final bool isProductSearch;

  const SearchWidget(this.isProductSearch);

  @override
  String get name => 'SearchWidget';

  @override
  State<StatefulWidget> createState() {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: ColorPallet.primaryColor,
    ));
    return __SearchWidgetState();
  }
}

class __SearchWidgetState extends State<SearchWidget> {
  late FocusNode myFocusNode;
  late List<SearchMatch> results;
  String searchString = '';

  final _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    myFocusNode = FocusNode();
  }

  bool inputChecker() {
    searchString = InputUtil.textSanitizer(searchString);
    if (!InputUtil.textValidator(searchString) || searchString.isEmpty) {
      Fluttertoast.showToast(
        msg: translations.text('invalidInput'),
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
      );
      setState(() {
        _controller.clear();
        FocusScope.of(context).requestFocus(myFocusNode);
      });
      return false;
    } else {
      return true;
    }
  }

  String formatSearchString(String searchStringTest) {
    if (searchStringTest.isNotEmpty) {
      return searchString[0].toUpperCase() + searchString.substring(1);
    } else {
      return '';
    }
  }

  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Search');
    final _arrowsController = ScrollController();
    return PopScope(
      canPop: true,
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            Positioned(
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      color: ColorPallet.primaryColor,
                    ),
                  ),
                  Expanded(
                    child: Container(
                      color: Colors.white,
                    ),
                  )
                ],
              ),
            ),
            Positioned(
              child: SafeArea(
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 73.0 * y,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          border: Border.all(
                              width: 13.0 * x, color: ColorPallet.primaryColor),
                        ),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                alignment: Alignment.centerLeft,
                                child: TextField(
                                  autocorrect: false,
                                  focusNode: myFocusNode,
                                  keyboardType: TextInputType.text,
                                  onSubmitted: (value) {},
                                  onChanged: (value) {
                                    setState(() {
                                      searchString = value;
                                    });
                                  },
                                  autofocus: true,
                                  controller: _controller,
                                  style: TextStyle(
                                      color: ColorPallet.darkTextColor,
                                      fontSize: 20.0 * f,
                                      fontWeight: FontWeight.w500),
                                  decoration: InputDecoration(
                                    hintText: widget.isProductSearch
                                        ? translations
                                            .text('findProductService')
                                        : translations.text('findShop'),
                                    hintStyle: TextStyle(
                                        color: ColorPallet.midGray,
                                        fontSize: 18.0 * f),
                                    prefixIcon: InkWell(
                                      onTap: () {
                                        SystemChrome.setSystemUIOverlayStyle(
                                          const SystemUiOverlayStyle(
                                            statusBarColor:
                                                ColorPallet.primaryColor,
                                          ),
                                        );
                                        Navigator.of(context).pop();
                                      },
                                      child: Icon(
                                        Icons.arrow_back,
                                        color: ColorPallet.midGray,
                                        size: 24 * x,
                                      ),
                                    ),
                                    border: InputBorder.none,
                                  ),
                                ),
                              ),
                            ),
                            if (searchString.isEmpty)
                              Container()
                            else
                              InkWell(
                                onTap: () {
                                  if (inputChecker()) {
                                    if (results.isNotEmpty) {
                                      showDialog(
                                          context: context,
                                          routeSettings: const RouteSettings(
                                              name: 'SearchNotFoundDialog'),
                                          builder: (BuildContext context) {
                                            return _SearchNotFoundDialog(
                                                searchString,
                                                results,
                                                widget.isProductSearch);
                                          });
                                    }
                                  }
                                },
                                child: Container(
                                  width: 110 * x,
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 10 * x, vertical: 10 * y),
                                  decoration: BoxDecoration(
                                      color: ColorPallet.primaryColor,
                                      borderRadius:
                                          BorderRadius.circular(10 * x),
                                      boxShadow: [
                                        BoxShadow(
                                          color: ColorPallet.lightGray,
                                          offset: Offset(1.0 * x, 1.0 * y),
                                          blurRadius: 1.5 * x,
                                          spreadRadius: 1.5 * x,
                                        )
                                      ]),
                                  child: Center(
                                    child: Text(
                                        '${translations.text('notFound')}',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 13 * f,
                                            fontWeight: FontWeight.w500)),
                                  ),
                                ),
                              ),
                          ],
                        ),
                      ),
                      SizedBox(height: 4 * y),
                      Expanded(
                        child: Listener(
                          onPointerDown: (v) {
                            myFocusNode.unfocus();
                          },
                          child: FutureBuilder(
                            future: getMatches(
                                searchString.isNotEmpty ? searchString : '',
                                widget.isProductSearch,
                                SortOrder.wordLengthThenFrequency,
                                20,
                                20),
                            builder: (BuildContext context,
                                AsyncSnapshot<List<SearchMatch>> snapshot) {
                              //debugPrint('Snapshot: ${snapshot}');
                              if (!snapshot.hasData ||
                                  snapshot.data!.isEmpty ||
                                  searchString.isEmpty) {
                                //debugPrint('No search results');
                                return _QuickSuggestionList(
                                    widget.isProductSearch);
                              }
                              results = snapshot.data!;

                              return DraggableScrollbar.rrect(
                                alwaysVisibleScrollThumb: true,
                                heightScrollThumb: 50,
                                backgroundColor: Colors.grey,
                                padding: EdgeInsets.only(right: 4.0 * x),
                                controller: _arrowsController,
                                child: ListView.builder(
                                  controller: _arrowsController,
                                  itemCount: snapshot.data!.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return _SearchResultTile(
                                      snapshot.data![index].name.capitalize(),
                                      snapshot.data![index].category
                                          .capitalize(),
                                      widget.isProductSearch,
                                      false,
                                      snapshot.data![index].code,
                                    );
                                  },
                                ),
                              );
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _QuickSuggestionList extends StatelessWidget {
  final bool isProductSearch;

  const _QuickSuggestionList(this.isProductSearch);

  Future<List<Map<String, dynamic>>> getSearchResults(
      BuildContext context, bool mostRecentSearchSuggestion) async {
    if (isProductSearch) {
      if (mostRecentSearchSuggestion) {
        return SearchSuggestions.getMostRecentProducts();
      } else {
        return SearchSuggestions.getMostFrequentProducts();
      }
    } else {
      if (mostRecentSearchSuggestion) {
        return SearchSuggestions.getMostRecentStores();
      } else {
        return SearchSuggestions.getMostFrequentStores();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final _arrowsController = ScrollController();
    return ScopedModelDescendant<SettingState>(
      builder: (context, child, model) => FutureBuilder(
        future: getSearchResults(
            context,
            isProductSearch
                ? model.mostRecentSearchSuggestionProducts
                : model.mostRecentSearchSuggestionStore),
        builder: (BuildContext context,
            AsyncSnapshot<List<Map<String, dynamic>>> snapshot) {
          if (!snapshot.hasData || snapshot.data!.isEmpty) {
            return Container();
          }
          return DraggableScrollbar.rrect(
            alwaysVisibleScrollThumb: true,
            heightScrollThumb: 50,
            backgroundColor: Colors.grey,
            padding: EdgeInsets.only(right: 4.0 * x),
            controller: _arrowsController,
            child: ListView.builder(
              controller: _arrowsController,
              itemCount: snapshot.data!.length,
              itemBuilder: (BuildContext context, int index) {
                if (isProductSearch) {
                  try {
                    return _SearchResultTile(
                        snapshot.data![index]['product']
                                .toString()[0]
                                .toUpperCase() +
                            snapshot.data![index]['product']
                                .toString()
                                .substring(1),
                        snapshot.data![index]['productCategory'].toString(),
                        isProductSearch,
                        index == 0);
                  } catch (e) {
                    print('Error formatting product: $e');
                    return Container();
                  }
                } else {
                  try {
                    return _SearchResultTile(
                        snapshot.data![index]['storeName']
                                .toString()[0]
                                .toUpperCase() +
                            snapshot.data![index]['storeName']
                                .toString()
                                .substring(1),
                        snapshot.data![index]['storeType'].toString(),
                        isProductSearch,
                        index == 0);
                  } catch (e) {
                    print('Error formatting store name: $e');
                    return Container();
                  }
                }
              },
            ),
          );
        },
      ),
    );
  }
}

class _SearchResultTile extends StatelessWidget {
  const _SearchResultTile(this.headerText, this.subText, this.isProductSearch,
      this.displaySearchSuggestionOption,
      [this.code = '']);

  final bool displaySearchSuggestionOption;
  final String headerText;
  final bool isProductSearch;
  final String subText;
  final String code;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        if (displaySearchSuggestionOption)
          SizedBox(
            height: 40 * y,
            width: MediaQuery.of(context).size.width,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ScopedModelDescendant<SettingState>(
                    builder: (context, child, model) => InkWell(
                        onTap: () {
                          if (isProductSearch) {
                            model.changeSearchSuggestionTypeProducts();
                          } else {
                            model.changeSearchSuggestionTypeStore();
                          }
                        },
                        child: Row(
                          children: <Widget>[
                            Text(
                              isProductSearch
                                  ? translations.text('recentItems')
                                  : "Magasins récents",
                              style: TextStyle(
                                  color: (isProductSearch &&
                                              model
                                                  .mostRecentSearchSuggestionProducts) ||
                                          (!isProductSearch &&
                                              model
                                                  .mostRecentSearchSuggestionStore)
                                      ? ColorPallet.primaryColor
                                      : ColorPallet.midGray,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16 * f),
                            ),
                            SizedBox(width: 3 * x),
                            Icon(
                              Icons.arrow_downward,
                              color: (isProductSearch &&
                                          model
                                              .mostRecentSearchSuggestionProducts) ||
                                      (!isProductSearch &&
                                          model.mostRecentSearchSuggestionStore)
                                  ? ColorPallet.primaryColor
                                  : ColorPallet.midGray,
                              size: 15 * x,
                            ),
                            SizedBox(width: 30 * x),
                            Text(
                              isProductSearch
                                  ? translations.text('frequentItems')
                                  : "Magasins fréquents",
                              style: TextStyle(
                                  color: (isProductSearch &&
                                              model
                                                  .mostRecentSearchSuggestionProducts) ||
                                          (!isProductSearch &&
                                              model
                                                  .mostRecentSearchSuggestionStore)
                                      ? ColorPallet.midGray
                                      : ColorPallet.primaryColor,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16 * f),
                            ),
                            SizedBox(width: 3 * x),
                            Icon(
                              Icons.arrow_downward,
                              color: (isProductSearch &&
                                          model
                                              .mostRecentSearchSuggestionProducts) ||
                                      (!isProductSearch &&
                                          model.mostRecentSearchSuggestionStore)
                                  ? ColorPallet.midGray
                                  : ColorPallet.primaryColor,
                              size: 15 * x,
                            ),
                          ],
                        )),
                  ),
                ]),
          )
        else
          Container(),
        InkWell(
          onTap: () {
            if (!isProductSearch) {
              SearchSuggestions.addStore(headerText, subText);
            }
            Navigator.pop(context, [headerText, subText, code]);
          },
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 5,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.symmetric(
                          horizontal: 17 * x, vertical: 2 * y),
                      child: Text(
                        headerText,
                        style: TextStyle(
                            fontSize: 16 * f,
                            fontWeight: FontWeight.w700,
                            color: ColorPallet.darkTextColor),
                      ),
                    ),
                    SizedBox(height: 3 * y),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 17 * x),
                      child: Text(
                        subText,
                        style: TextStyle(
                            fontSize: 12.5 * f,
                            color: ColorPallet.darkTextColor.withOpacity(0.8),
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    SizedBox(height: 3 * y),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 17 * x, vertical: 2 * y),
          width: MediaQuery.of(context).size.width,
          height: 1 * y,
          color: ColorPallet.lightGray,
        )
      ],
    );
  }
}

class _SearchNotFoundDialog extends StatefulWidget {
  final bool isProductSearch;
  final List<SearchMatch> results;
  final String searchString;

  const _SearchNotFoundDialog(
      this.searchString, this.results, this.isProductSearch);

  @override
  __SearchNotFoundDialogState createState() => __SearchNotFoundDialogState();
}

class __SearchNotFoundDialogState extends State<_SearchNotFoundDialog> {
  List<String> categoryOptions = [];
  String? selectedCategory;

  @override
  void initState() {
    super.initState();
    for (final result in widget.results) {
      if (!(categoryOptions.contains(result.category) ||
          result.category == 'Unknown')) {
        if (categoryOptions.length < 4) {
          categoryOptions.add(result.category);
        }
      }
    }
    categoryOptions.add(translations.text('additional2'));
    selectedCategory = 'Ajout utilisateur';
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      titlePadding: const EdgeInsets.all(0),
      backgroundColor: ColorPallet.white,
      surfaceTintColor: Colors.transparent,
      title: Container(
        color: ColorPallet.primaryColor,
        padding: EdgeInsets.symmetric(horizontal: 25 * x, vertical: 20 * y),
        child: Center(
          child: Text(translations.text('addYourself'),
              style: TextStyle(
                  fontSize: 20 * f,
                  fontWeight: FontWeight.w500,
                  color: Colors.white)),
        ),
      ),
      content: Container(
        height: 50 * y,
        child: Column(
          children: <Widget>[
            Text(
              widget.isProductSearch
                  ? translations.text('productService')
                  : translations.text('shop'),
              style: TextStyle(
                fontSize: 14 * f,
                fontWeight: FontWeight.w500,
                color: ColorPallet.darkTextColor.withOpacity(0.8),
              ),
            ),
            SizedBox(height: 5 * y),
            Text(
              widget.searchString[0].toUpperCase() +
                  widget.searchString.substring(1),
              style: TextStyle(
                fontSize: 20 * f,
                fontWeight: FontWeight.w600,
                color: ColorPallet.darkTextColor,
              ),
            ),
          ],
        ),
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text(translations.text('cancel'),
              style: TextStyle(
                color: ColorPallet.darkTextColor,
                fontSize: 14 * f,
              )),
        ),
        TextButton(
          onPressed: () {
            final example = widget.searchString[0].toUpperCase() +
                widget.searchString.substring(1);

            if (widget.isProductSearch) {
              ProductController().addNewProduct(example);
            } else {
              SearchSuggestions.addStore(example, 'Ajout utilisateur');
              ShopController().addNewShop(example);
            }
            Navigator.of(context).pop();
            Navigator.pop(
                context, [example, selectedCategory, selectedCategory]);
          },
          child: Text(translations.text('add'),
              style: TextStyle(
                color: ColorPallet.darkTextColor,
                fontSize: 14 * f,
              )),
        ),
      ],
    );
  }
}

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}
