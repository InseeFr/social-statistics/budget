import 'package:bdf_survey/core/controller/util/currency_formatter.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

import '../../../core/model/color_pallet.dart';

class DonutChart extends StatelessWidget {
  final bool animate;
  final List<Map<String, dynamic>> seriesList;
  final Function(String code) changeCategory;

  const DonutChart(this.seriesList, this.changeCategory,
      {required this.animate});

   
  @override
  Widget build(BuildContext context) {
    final fontSize = 13.0;
    final radius = (MediaQuery.of(context).size.width / 5);
    final colorList = [
      ColorPallet.yellow,
      ColorPallet.orange,
      ColorPallet.primaryColor,
      ColorPallet.pink,
      ColorPallet.darkGreen,
      ColorPallet.midblue,
      ColorPallet.yellow.withOpacity(0.5),
      ColorPallet.orange.withOpacity(0.5),
      ColorPallet.primaryColor.withOpacity(0.5),
      ColorPallet.pink.withOpacity(0.5),
      ColorPallet.darkGreen.withOpacity(0.5),
      ColorPallet.midblue.withOpacity(0.5),
    ];

    final List<PieChartSectionData> chartData = [];
    for (var i = 0; i < seriesList.length; i++) {
      chartData.add(
        PieChartSectionData(
          color: colorList.elementAt(i % colorList.length),
          value: double.parse(
              double.parse(seriesList[i]['spendTotal'].toString())
                  .toStringAsFixed(2)),
          title:
              '${double.parse(seriesList[i]['spendTotal'].toString()).toStringAsFixed(2).addCurrencyFormat()}',
          radius: radius,
          titleStyle: TextStyle(
            fontSize: fontSize,
            fontWeight: FontWeight.bold,
            color: ColorPallet.white,
          ),
        ),
      );
    }
    return PieChartInsight(chartData: chartData, key: UniqueKey());

  }
}

class PieChartInsight extends StatefulWidget {
  final List<PieChartSectionData> chartData;

  const PieChartInsight({required Key key, required this.chartData})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => PieChartInsightState();
}

class PieChartInsightState extends State<PieChartInsight> {
  int touchedIndex = -1;

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1,
      child: Row(
        children: <Widget>[
          const SizedBox(
            height: 3,
          ),
          Expanded(
            child: AspectRatio(
              aspectRatio: 1,
              child: PieChart(
                PieChartData(
                  pieTouchData: PieTouchData(
                    touchCallback: (FlTouchEvent event, pieTouchResponse) {
                      setState(() {
                        if (!event.isInterestedForInteractions ||
                            pieTouchResponse == null ||
                            pieTouchResponse.touchedSection == null) {
                          touchedIndex = -1;
                          return;
                        }
                        touchedIndex = pieTouchResponse
                            .touchedSection!.touchedSectionIndex;
                      });
                    },
                  ),
                  borderData: FlBorderData(
                    show: false,
                  ),
                  sectionsSpace: 0,
                  centerSpaceRadius: MediaQuery.of(context).size.width / 7,
                  sections:
                      widget.chartData.map<PieChartSectionData>((section) {
                    final isTouched =
                        widget.chartData.indexOf(section) == touchedIndex;
                    final double radius = isTouched ? 100 : 80;
                    return PieChartSectionData(
                      color: section.color,
                      value: section.value,
                      title: section.title,
                      radius: radius,
                      titleStyle: section.titleStyle,
                    );
                  }).toList(),
                ),
              ),
            ),
          )
        ],
      ),
    );
    
  }
}
