import 'dart:async';

import '../../../core/controller/util/date.dart';
import '../../../core/data/database/database_helper.dart';
import '../../../core/data/database/tables/receipt.dart';
import '../../../core/data/database/tables/receipt_product.dart';

class Item {
  Item(this.id, this.description, this.value, this.parentId, this.date);

  String date;
  String description;
  String id;
  String parentId;
  double value;

  int get dateInt {
    return DateUtil.dateStringToDateInt(date);
  }
}

class Node {
  Node(this.id, this.description, this.type);

  List<String> children = [];
  String description;
  String id;
  List<Item> items = [];
  int type;
  double value = 0;
}

class Tree {
  Tree._();

  static final Tree instance = Tree._();

  String marker = ' #_# ';
  int? maxProductDate;
  int? minProductDate;
  Map<String, Node> nodes = {};
  String selectedId = '.';
  String separator = '.';

  String get rootId {
    return separator;
  }

  Node? get selectedNode {
    return nodes[selectedId];
  }

  Future<void> initCoicop() async {
    final db = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> tblCoicop = await db.query('tblCoicop_fr');

    nodes.clear();
    addNode(
        rootId, 'Catégories');
    for (final coicop in tblCoicop) {
      addNode(coicop['code'].toString(), coicop['coicop'].toString());
    }
  }

  Future<void> initShops() async {
    final db = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> tblShops = await db.query('tblShop_fr');

    nodes.clear();
    addNode(rootId, 'Magasins');

    // Create a map to keep track of the shop types we've seen
    final Map<String, String> shopTypes = {};

    for (final shop in tblShops) {
      final shopType = shop['shoptype'].toString();

      // If we haven't seen this shop type before, add a new node for it
      if (!shopTypes.containsKey(shopType)) {
        final code = (shopTypes.length + 1).toString().padLeft(2, '0');
        addNode(code, shopType);
        shopTypes[shopType] = code;
      }
    }
  }

  Future<DateTime> getProductDate(String receiptId) async {
    final receipts = await ReceiptTable().query(id: receiptId);
    final receipt = receipts.first;
    return receipt.date;
  }

  Future<void> initProducts(double minFilterPrice, double maxFilterPrice, int minFilterDate, int maxFilterDate) async {
    final receiptProducts = await ReceiptProductTable().query();

    deleteAllItems();
    minProductDate = 25000101;
    maxProductDate = 19000101;

    var nn = 0;
    for (final receiptProduct in receiptProducts) {
      final prodDate =
          DateUtil.dateStringToDateInt(receiptProduct.productDate!);
      nn = nn + 1;
      if (receiptProduct.getPrice() >= minFilterPrice && receiptProduct.getPrice() <= maxFilterPrice) {
        if (receiptProduct.getPrice() > 0.0 && receiptProduct.isReturn == false) {
          if (prodDate >= minFilterDate && prodDate <= maxFilterDate) {
            for (var i = 0; i < receiptProduct.count!; i++) {
              addItem(
                  nn,
                  receiptProduct.coicop!,
                  receiptProduct.name!,
                  receiptProduct.getPrice(),
                  DateUtil.dateIntToDateString(prodDate));
            }
            minProductDate =
                minProductDate! < prodDate ? minProductDate : prodDate;
            maxProductDate =
                maxProductDate! > prodDate ? maxProductDate : prodDate;
          }
        }
      }
    }
    calculateAllValues();
  }

  Future<void> initProductsShop() async {
    final receipts = await ReceiptTable().query();

    deleteAllItems();
    var nn = 0;
    for (final receipt in receipts) {
      final prodDate = DateUtil.dateTimeToDateInt(receipt.date);
      nn = nn + 1;
      if (receipt.products.getTotalPrice() > 0.0) {
        addItemStore(
            receipt.store
                .category,
            receipt.store.name,
            receipt.products.getTotalPrice(),
            DateUtil.dateIntToDateString(prodDate));
      }
    }
  }

  void calculateAllValues() {
    _calculateValues(rootId);
  }

  void _calculateValues(String id) {
    var sum = 0.0;
    final node = nodes[id];
    for (final child in node!.children) {
      _calculateValues(child);
      sum += nodes[child]!.value;
    }
    for (final item in node.items) {
      sum += item.value;
    }
    node.value = sum;
  }

  void deleteAllItems() {
    _deleteItems(rootId);
    _deleteItemNodes();
  }

  void _deleteItems(String id) {
    final node = nodes[id];
    for (final child in node!.children) {
      _deleteItems(child);
    }
    node.items.clear();
    node.value = 0.0;
  }

  void _deleteItemNodes() {
    nodes.forEach((k, v) => v.children.removeWhere((c) => nodes[c]!.type == 2));
    nodes.removeWhere((k, v) => v.type == 2);
  }

  String parentId(String id) {
    if (id == rootId) {
      return id;
    } else if (!id.contains(separator)) {
      return rootId;
    }
    return id.substring(0, id.lastIndexOf(separator));
  }

  void addNode(String id, String description) {
    if (!nodes.keys.contains(id)) {
      nodes[id] = Node(id, description, 1);
      _addNodeToParent(id);
    } else {
      nodes[id]!.description = description;
    }
  }

  void _addParentNode(String id) {
    if (!nodes.keys.contains(id)) {
      nodes[id] = Node(id, '?', 1);
      _addNodeToParent(id);
    }
  }

  void _addNodeToParent(String id) {
    if (id != rootId) {
      final parent = parentId(id);
      _addParentNode(parent);
      if (!nodes[parent]!.children.contains(id)) {
        nodes[parent]!.children.add(id);
      }
    }
  }

  void addItem(int nn, String id, String description, double value, String date) {
    _addParentNode(id);
    final itemId = '$id${separator}_${nn.toString()}';
    final itemDescription = description + marker + nn.toString();
    nodes[itemId] = Node(itemId, itemDescription, 2);
    nodes[id]!.children.add(itemId);
    nodes[itemId]!.items.add(Item(itemId, itemDescription, value, id, date));
  }

  void addItemStore(
      String shoptype, String description, double value, String date) {
    String id = nodes.entries
        .firstWhere((entry) => entry.value.description == shoptype,
            orElse: () => MapEntry<String, Node>("", Node("", "", 0)))
        .key;
    _addParentNode(id);
    nodes[id]!.items.add(Item(id, description, value, parentId(id), date));
  }

  void addItemOKE(String id, String description, double value, String date) {
    _addParentNode(id);
    nodes[id]!.items.add(Item(id, description, value, parentId(id), date));
  }

  Item item(String id) {
    if (!nodes.keys.contains(id)) {
      return Item(id, 'Id ' ' + id + ' ' does not exist!', 0, parentId(id), '');
    }
    return Item(id, nodes[id]!.description, nodes[id]!.value, parentId(id), '');
  }

  List<Item> items(String id) {
    final result = <Item>[];

    if (!nodes.keys.contains(id)) {
      result.add(Item(id, 'Id ' ' + id + ' ' does not exist!', 0, parentId(id), ''));
      return result;
    }

    final sumItems = <String, Item>{};
    for (final i in nodes[id]!.items) {
      if (!sumItems.keys.contains(i.description)) {
        sumItems[i.description] = Item(i.id, i.description, i.value, i.parentId, i.date);
      } else {
        sumItems[i.description]!.value += i.value;
      }
    }
    for (final i in sumItems.values) {
      result.add(i);
    }

    return result;
  }

  List<Item> allItemsDated(String id) {
    final result = <Item>[];

    for (final i in nodes[id]!.items) {
      result.add(Item(id, i.description, i.value, id, i.date));
    }

    for (final child in nodes[id]!.children) {
      for (final i in allItemsDated(child)) {
        result.add(
            Item(child, nodes[child]!.description, i.value, child, i.date));
      }
    }

    return result;
  }

  List<Item> periodItemsDated(String id, String fromDate, String toDate) {
    final result = <Item>[];
    for (final i in allItemsDated(id)) {
      if (fromDate.compareTo(i.date) <= 0 && toDate.compareTo(i.date) >= 0) {
        result.add(i);
      }
    }
    return result;
  }

  List<Item> children(String id) {
    final result = <Item>[];

    if (!nodes.keys.contains(id)) {
      result.add(Item(id, 'Id ' ' + id + ' ' does not exist!', 0, parentId(id), ''));
      return result;
    }

    for (final child in nodes[id]!.children) {
      if (item(child).value >= 0) {
        result.add(item(child));
      }
    }

    return result;
  }

  List<Item> ancestorsInclusive(String id) {
    final result = <Item>[];

    if (!nodes.keys.contains(id)) {
      result.add(Item(id, 'Id ' ' + id + ' ' does not exist!', 0, parentId(id), ''));
      return result;
    }

    if (id != rootId) {
      for (final i in ancestorsInclusive(parentId(id))) {
        result.add(i);
      }
    }

    result.add(item(id));

    return result;
  }

  List<Item> ancestors(String id) {
    final result = ancestorsInclusive(id);
    result.removeLast();
    return result;
  }
}
