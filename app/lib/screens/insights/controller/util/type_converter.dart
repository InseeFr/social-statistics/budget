import 'package:intl/intl.dart';

String dateTimeToString(DateTime input) {
  return DateFormat('yyyy-MM-dd').format(input);
}

int dateTimeToInt(DateTime input) {
  return int.parse(DateFormat('yyyyMMdd').format(input));
}
