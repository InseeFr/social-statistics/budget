= Statistical Service Specification
:Author: Ole Mussmann
:Email: bo.mussmann@cbs.nl
:Date: 2020-03-19
:Document-Revision: 0.1
:Software-Revision: 2.0.0+24

*Logistical description*, version _{document-revision}_

{date} by {author} {email}

== *@HBS App*

.Version
{software-revision}

.Ownership
https://www.cbs.nl/[Statistics Netherlands (CBS)]

== Protocol
=== Backend
The communication between the `@HBS App` and `@HBS App Backend` is defined in the link:swagger.yaml[swagger file].

=== User
Users install the app from https://play.google.com/store[Google Play Store] or https://www.apple.com/ios/app-store/[Apple App Store].

== Service Methods
There are no exposed service methods.

== Applicable Methodologies
=== User Interface
The app frontend main screen is a *calendar* indicating what days are part of the respondent data collection period.
The starting data and length of the period are open parameters.
Respondents can move to three other screens: an *overview* of all entered purchases, *expenditure statistics* and *settings*.

=== Data Entry
Purchases can be enterend *manually* or through *scanning of receipts*.
For manual data entry three *lists* are used.

List of Products::
Set in every-day language and is linked to the lowest COICOP level.
Products are shown to the respondent based on *historical input* and the https://en.wikipedia.org/wiki/Jaro%E2%80%93Winkler_distance[*Jaro-Winkler distance*].

List of shop types::
About 50 entries long.
Shop types are used for respondent statistics and shop names for *OCR of receipts*.

List of shop names::
Uniquely linked to shop types.

=== Data Processing
Receipt scans are partially processed in-app; *text recognition* is performed in-app but *classification* is performed in-house.
Classified receipts are returned to the respondents.

=== Respondent Motivation
Respondents are motivated using *individual household expenditure statistics* displayed in the app and by *conditional incentives*.
The conditional incentives are optional.

=== Respondent Support
Respondents are assisted in-app by a welcome *tutorial* and by various *help buttons*.

== TODO
* app version
* swagger file

