# Budget Des Familles App

This app is a simple budget app that allows you to track your expenses during the survey period. 

For more informations on the survey, please visit the [in depth presentation of the survey](https://www.insee.fr/fr/metadonnees/source/serie/s1194).

This app is built with Flutter, and is available on both Android and iOS. 
Minimum supported versions are Android 10.0 and iOS 15.0.

## Screenshots
<img src="screenshots/overview.png"  height="350">
<img src="screenshots/adding_manually.png"  height="350">
<img src="screenshots/expense_list.png"  height="350">
<img src="screenshots/insights_category_view.png" height="350">
<img src="screenshots/picture_receipt.png" height="350">


## Getting Started
This part will guide you through the process of setting up the project on your local machine.

### Prerequisites
- Flutter SDK (currently using version 3.16.7)
- Android Studio
- Xcode (for iOS development)
- Android Emulator / iOS Simulator or a physical device

### Configuration
1. Create a new config file in assets/configs/ and name it `config-"something".json`
2. Copy the content of the following example into the new file and replace the values with your own
```json
{
    "apiUrl": "Url of the API",
    "issuer": "Url of the issuer (realm) of your authentication server",
    "discoveryUrl": "Url of the discovery url of your authentication server",
    "authClientId": "Client ID of the authentication server",
    "redirectUrl": "fr.insee.householdbudgetsurvey://oidc-callback", <- This is the default redirect url, you will have to set it up in your authentication server
    "authorizationEndpoint": "Authorization endpoint ",
    "tokenEndpoint": "Token endpoint",
    "endSessionEndpoint": "End session endpoint",
}
```
3. Change the `main.dart` file to import the new config file
```dart
void main() async {
  await configureApp('something');
  [...]
}
```

#### Authentication
This app uses OpenID Connect for authentication. You will need to set up an authentication server and fill in the `config.json` file with the correct values.

The user will be redirected to the authentication server to log in and then redirected back to the app with an access token.
You will need to set up the redirect url in your authentication server to `fr.insee.householdbudgetsurvey://oidc-callback` and the client id to the one you set in the `config.json` file.

The app make usage of the refresh token, and of the offline session to keep the user logged in, you will need to configure those values.

### Installation
1. Clone the repository
```sh
git clone
cd budget/app
```
2. Install the dependencies and ensure that flutter is properly set up
```sh
flutter doctor
flutter pub get
```
3. Run the app
```sh
flutter run
```

## Build and Deploy
This part will guide you through the process of building and deploying the app on the different platforms.
### Version check
Before building the app, make sure to update the version number and the app bundle name in the `pubspec.yaml` file.

### Android
Check the `android/app/build.gradle` and `android/app/AndroidManifest.xml` file and make sure that the application id and the package name are set to the correct value.
You can also change the app name and the app icon in the `android/app/src/main/res/values/strings.xml` and `android/app/src/main/res/drawable/` folder.

1. Build the app
```sh
flutter build appbundle
```
The app bundle will be located in the `build/app/outputs/bundle/release/` folder. You will be able to deploy it on the Google Play Console.
You can also build an APK by running
```sh
flutter build apk
```

### iOS
1. Open the project in Xcode
```sh
open ios/Runner.xcworkspace
```
2. Change the bundle identifier and the app name in the `Runner` target settings to match your app name and bundle identifier in the `ios/Runner/Info.plist` file
3. Change the app icon by replacing the `AppIcon` folder in the `Runner` target settings and check your signing certificate and provisioning profile
4. Build the app
```sh
cd ios
pod install
cd ..
flutter build ios
```
5. Deploy the app on the App Store or on TestFlight using the Archive menu in Xcode