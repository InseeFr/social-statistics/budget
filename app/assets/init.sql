-- Création des groupes
INSERT INTO tbl_group (name, status) VALUES ('superuser', 'enabled');
INSERT INTO tbl_group (name, status) VALUES ('casemanagement', 'enabled');
INSERT INTO tbl_group (name, status) VALUES ('bdf', 'enabled');

DECLARE @bdfGroupId INT;
SET @bdfGroupId = SCOPE_IDENTITY();

INSERT INTO tbl_group_info (group_id, key, value)
VALUES 
(@bdfGroupId, 'InsightsConfiguration', 'enabled'),
(@bdfGroupId, 'QuestionnaireConfiguration', 'disabled'),
(@bdfGroupId, 'ParadataConfiguration', 'enabled'),
(@bdfGroupId, 'OCRConfiguration', 'disabled');

-- Création des utilisateurs classiques

DECLARE @counter INT = 1;

WHILE @counter <= 10
BEGIN
    DECLARE @userName NVARCHAR(255);
    SET @userName = 'USER_' + RIGHT('000' + CAST(@counter AS NVARCHAR(3)), 3);

    DECLARE @password NVARCHAR(255);
    SET @password = @userName;

    DECLARE @hashedPassword VARBINARY(64);
    SET @hashedPassword = HASHBYTES('SHA2_256', @password);

    INSERT INTO tbl_user (group_id, name, password, sync_order)
    VALUES (@bdfGroupId, @userName, @hashedPassword, 0);

    SET @counter = @counter + 1;
END;


DECLARE @superuserGroupId INT;
SET @superuserGroupId = (SELECT id FROM tbl_group WHERE name = 'superuser');

DECLARE @superuserName NVARCHAR(255);
SET @superuserName = 'SuperUser'; 

DECLARE @superuserPassword NVARCHAR(255);
SET @superuserPassword = @superuserName;

DECLARE @hashedPassword VARBINARY(64);
    SET @hashedPassword = HASHBYTES('SHA2_256', @superuserPassword);

INSERT INTO tbl_user (group_id, name, password, sync_order)
VALUES (@superuserGroupId, @superuserName, @hashedPassword, 0);
