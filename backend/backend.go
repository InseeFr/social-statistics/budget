package main

import (
	"backend/database"
	"backend/helpers"
	"backend/restapi"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/gorilla/mux"
)

const STOPONMISSINGENVIRONMENT bool = false

func main() {

	//Read config file
	helpers.ReadConfig()

	var port string = helpers.AppConfig.PORT
	var host string = helpers.AppConfig.DBHOST
	var dbname string = helpers.AppConfig.DBNAME
	var dbport string = helpers.AppConfig.DBPORT
	var user string = helpers.AppConfig.DBUSER
	var password string = helpers.AppConfig.DBPASSWORD
	var oidcPK string = helpers.AppConfig.OIDCPUBLICKEY

	//Init log system
	initLog()
	log.Println("Log configured")

	//Reading conf values
	log.Println(fmt.Sprintf("Port to expose : %s", port))
	log.Println(fmt.Sprintf("DB host name : %s", host))
	log.Println(fmt.Sprintf("DB database name : %s", dbname))
	log.Println(fmt.Sprintf("DB port : %s", dbport))
	log.Println(fmt.Sprintf("DB user : %s", user))
	log.Println(fmt.Sprintf("OIDC public key: %s", oidcPK))

	//Get (TODO) Public Key from Oauth server and parse it.
	helpers.InitializeOauthPublicKey()
	log.Println("Public Key configured")

	var dbportInt int
	dbportInt, _ = strconv.Atoi(dbport)

	database.Init(host, dbportInt, user, password, dbname)

	log.Println("Creating routes")
	router := mux.NewRouter()

	router.HandleFunc("/budget/group/list", restapi.Group_List).Methods("POST")
	router.HandleFunc("/budget/group/create", restapi.Group_Create).Methods("POST")
	router.HandleFunc("/budget/group/enable", restapi.Group_Enable).Methods("POST")
	router.HandleFunc("/budget/group/disable", restapi.Group_Disable).Methods("POST")
	router.HandleFunc("/budget/group/delete", restapi.Group_Delete).Methods("POST")
	router.HandleFunc("/budget/group/rename", restapi.Group_Rename).Methods("POST")
	router.HandleFunc("/budget/group/infos", restapi.Group_Infos).Methods("POST")

	router.HandleFunc("/budget/users/list", restapi.Users_List).Methods("POST")
	router.HandleFunc("/budget/users/listall", restapi.Users_List_All).Methods("POST")
	router.HandleFunc("/budget/users/create", restapi.Users_Create).Methods("POST")
	router.HandleFunc("/budget/users/delete", restapi.Users_Delete).Methods("POST")
	router.HandleFunc("/budget/users/regroupgroup", restapi.Users_Regroup_Group).Methods("POST")
	router.HandleFunc("/budget/users/regroupusers", restapi.Users_Regroup_Users).Methods("POST")
	router.HandleFunc("/budget/users/passwords", restapi.Users_Passwords).Methods("POST")
	router.HandleFunc("/budget/users/generate", restapi.Users_Generate).Methods("POST")

	router.Handle("/budget/phone/register", helpers.Protect(http.HandlerFunc(restapi.Phone_Register))).Methods("POST")

	router.Handle("/budget/data/pushreceipt", helpers.Protect(http.HandlerFunc(restapi.Data_Push_Receipt))).Methods("POST")
	router.Handle("/budget/data/pullreceipt", helpers.Protect(http.HandlerFunc(restapi.Data_Pull_Receipt))).Methods("POST")
	router.Handle("/budget/data/pushproduct", helpers.Protect(http.HandlerFunc(restapi.Data_Push_SearchProduct))).Methods("POST")
	router.Handle("/budget/data/pullproduct", helpers.Protect(http.HandlerFunc(restapi.Data_Pull_SearchProduct))).Methods("POST")
	router.Handle("/budget/data/pushstore", helpers.Protect(http.HandlerFunc(restapi.Data_Push_SearchStore))).Methods("POST")
	router.Handle("/budget/data/pullstore", helpers.Protect(http.HandlerFunc(restapi.Data_Pull_SearchStore))).Methods("POST")
	router.Handle("/budget/data/pushparadata", helpers.Protect(http.HandlerFunc(restapi.Data_Push_Paradata))).Methods("POST")
	router.Handle("/budget/data/pushactivity", helpers.Protect(http.HandlerFunc(restapi.Data_Push_Activity))).Methods("POST")
	router.Handle("/budget/data/pullactivity", helpers.Protect(http.HandlerFunc(restapi.Data_Pull_Activity))).Methods("POST")
	router.Handle("/budget/data/pushquestionnaire", helpers.Protect(http.HandlerFunc(restapi.Data_Push_Questionnaire))).Methods("POST")
	router.Handle("/budget/data/pullquestionnaire", helpers.Protect(http.HandlerFunc(restapi.Data_Pull_Questionnaire))).Methods("POST")
	router.Handle("/budget/data/pushexperience", helpers.Protect(http.HandlerFunc(restapi.Data_Push_Experience))).Methods("POST")
	router.Handle("/budget/data/pullexperience", helpers.Protect(http.HandlerFunc(restapi.Data_Pull_Experience))).Methods("POST")

	router.Handle("/budget/process/pullreceipt", helpers.Protect(http.HandlerFunc(restapi.Process_Pull_ReceiptForProcess))).Methods("POST")
	router.Handle("/budget/process/pushreceipt", helpers.Protect(http.HandlerFunc(restapi.Process_Push_ReceiptForProcess))).Methods("POST")

	// TODO : remove unused endpoint ??
	router.Handle("/budget/dashboard/receiptsperday", helpers.Protect(http.HandlerFunc(restapi.Dashboard_ReceiptsPerDay))).Methods("POST")
	router.Handle("/budget/dashboard/receiptsperphone", helpers.Protect(http.HandlerFunc(restapi.Dashboard_ReceiptsPerPhone))).Methods("POST")
	router.Handle("/budget/dashboard/receiptsperuser", helpers.Protect(http.HandlerFunc(restapi.Dashboard_ReceiptsPerUser))).Methods("POST")
	router.Handle("/budget/dashboard/userreceipts", helpers.Protect(http.HandlerFunc(restapi.Dashboard_UserReceipts))).Methods("POST")
	router.Handle("/budget/dashboard/paradatadatetime", helpers.Protect(http.HandlerFunc(restapi.Dashboard_ParadataDateTime))).Methods("POST")
	router.Handle("/budget/dashboard/paradatascreentime", helpers.Protect(http.HandlerFunc(restapi.Dashboard_ParadataScreenTime))).Methods("POST")
	router.Handle("/budget/dashboard/paradataclick", helpers.Protect(http.HandlerFunc(restapi.Dashboard_ParadataClick))).Methods("POST")

	//public health-check endpoints
	router.HandleFunc("/health-check", HealthCheck).Methods("GET")

	//specific secure endpoints
	router.Handle("/secure", helpers.Protect(http.HandlerFunc(SecureZone))).Methods("GET")

	log.Println("API ready to listen and serve")
	http.ListenAndServe(":"+port, router)

}

func initLog() {
	//add external file for logging
	f, err := os.OpenFile(helpers.AppConfig.LOG, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	log.SetOutput(f)
}

func HealthCheck(w http.ResponseWriter, r *http.Request) {
	log.Println("entering health check end point")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "API is up and running")
}

func SecureZone(w http.ResponseWriter, r *http.Request) {
	log.Println("entering secure zone end point")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "API secure zone for user : %s", helpers.GetUser(r))
}
