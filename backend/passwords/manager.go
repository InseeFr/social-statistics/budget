package passwords

import (
	"errors"
	"fmt"
	"os"
	"strconv"
	
	"backend/global"
)

const (
	DEFAULT_PASSWORD_DEFINITION string = "ULLD__MULD"
)

func outputQRfile(folder string, prefix string) string {
	return folder + "\\" + prefix + ".sh"
}

func outputPWfile(folder string, prefix string) string {
	return folder + "\\" + prefix + ".txt"
}

func CreatePasswordFiles(folder string, prefix string, up []global.User) error {
	global.MyPrintTrace("START ... CreatePasswordFiles")
	var qrFile string = outputQRfile(folder, prefix)
	var pwFile string = outputPWfile(folder, prefix)

	if global.FileExists(qrFile) || global.FileExists(pwFile) {
		global.MyPrintTrace("ERROR: files exsist! " + qrFile + " " + pwFile)
		return errors.New("Error: files already exist!")
	}

	f1, _ := os.Create(qrFile)
	fmt.Fprintln(f1, "# !!! THIS FILE IS GENERATED !!!")
	fmt.Fprintln(f1, " ")
	fmt.Fprintln(f1, "# Use this file to generate login QR-codes for users.")
	fmt.Fprintln(f1, "# On iMac open terminal and run [sh filename.sh].")
	fmt.Fprintln(f1, "# This creates [Username].png files with QR-code images.")
	fmt.Fprintln(f1, " ")
	fmt.Fprintln(f1, "rm *.png")
	fmt.Fprintln(f1, " ")

	f2, _ := os.Create(pwFile)
	fmt.Fprintln(f2, "# !!! THIS FILE IS GENERATED !!!")
	fmt.Fprintln(f2, " ")
	fmt.Fprintln(f2, "# Use this file to communicate passwords to users. ")
	fmt.Fprintln(f2, " ")
	fmt.Fprintln(f2, "Username Password")
	fmt.Fprintln(f2, "-----------------")

	for _, u := range up {
		_, _ = fmt.Fprintln(f1, "qrencode -o " + u.Name + ".png " + u.Name + "###***###" + u.Password)
		_, _ = fmt.Fprintln(f2, u.Name + " " + u.Password)
	}

	fmt.Fprintln(f1, " ")
	fmt.Fprintln(f1, "# !!! THIS FILE IS GENERATED !!!")

	fmt.Fprintln(f2, " ")
	fmt.Fprintln(f2, "# !!! THIS FILE IS GENERATED !!!")

	global.MyPrintTrace("created " + f1.Name())
	global.MyPrintTrace("created " + f2.Name())

	_ = f1.Close()
	_ = f2.Close()

	global.MyPrintTrace("FINISH ")

	return nil
}

func GenerateUsers(number int, prefix string, passwordDefinition string) []global.User {
	var up []global.User
	var name string
	for i := 0; i < number; i++ {
		name = prefix + strconv.Itoa(1000 + i)[1:4]
		up = append(up, GenerateUser(name, passwordDefinition))
	}
	return up
}

func GenerateUser(name string, passwordDefinition string) global.User {
	var user global.User
	user.Name = name
	user.Password = CreatePassword(passwordDefinition)
	return user
}
