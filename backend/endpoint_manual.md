# Populating the backend with groups and users.

After deploying the backend you will need to populate the database with groups and users. Each user is part of a group. And each group holds a set of key-value pairs which help to configure specific behaviors for all the users within the group.

To populate the database with groups and users, please review the steps below. Values surrounded by angle brackets ```<such as this>```, should be replaced.

Please note that security is enabled only AFTER at least one user has been added to the privileged 'superuser' group.

----

### **[0] Test if GO Budget web-service is running**
---
**Description:** Test whether your Go server is accessible.

**Access:** This endpoint is publicly accessible.

**URL:** ```/test```

**Method:** GET

---
### **[1.1] Create group “superuser”** 
---
**Description:**  Creates a group for superusers.

**Access:** Anybody can do this, as long as no superuser exists.

**URL:** ```/group/create```

**Method:** POST

**Data Params:** 
```json
{
 "superuser" : {"name" : "", "password" : ""},
 "group"     : {"name" : "superuser"}
} 
```
---
### **[1.2] Create group casemanagement** 
---
**Description:**  Creates a group for casemanagement. 

**Access:** Anybody can do this, as long as no superuser exists.

**URL:** ```/group/create```

**Method:** POST

**Data Params:** 
```json
{
 "superuser" : {"name" : "", "password" : ""},
 "group"     : {"name" : "casemanagement"}
} 
```
---
### **[2] Add (create) new superusers to group superusers**
---
**Description:**  The second thing to do is to create a first set of superusers. 

**Access:** Anybody can create the first set of superusers. Once superusers exist, only they can add new superusers.

**URL:** ```/users/create```

**Method:** POST

**Data Params:** 
```json
{
 "superuser" : {"name" : "", "password" : ""},
 "group"     : {"name" : "superuser"},
 "users"     : [ {"name" : "<SUPERUSER_0>", "password" : "<PASSWORD>"},
                 {"name" : "<SUPERUSER_1>", "password" : "<PASSWORD>"},
                 {"name" : "<SUPERUSER_2>", "password" : "<PASSWORD>"},
                 {"name" : "<SUPERUSER_3>", "password" : "<PASSWORD>"} ]
}
```

---
### **[3] Create groups for normal users**
---
**Description:**  Create a group of normal users. Through ```groupInfos``` key-value pairs, you can configure the following settings for members of the group:

```dart
//Show insight charts within the app directly, or only once the experiment has completed:
InsightsConfiguration: "enabled" or "delayed"
//Show a questionnaire when the app is first launched, or disable this feature:
QuestionnaireConfiguration: "enabled" or "disabled"
//Collect paradata (/usage data) from users, or disable this feature:
ParadataConfiguration: "enabled" or "disabled" 
//Configure the OCR feature (local_ocr = fully enabled, only_crop = no ocr but enable user cropping to select the receipt, disabled = user can make a picture, and nothing more)
OCRConfiguration: "local_ocr" or "only_crop" or "disabled"
```

**Access:** Superuser

**URL:** ```/group/create```

**Method:** POST

**Data Params:** 
```json
{
 "superuser"   : {"name" : "<SUPERUSER_0>", "password" : "<PASSWORD>"},
 "group"       : {"name" : "<GROUP NAME>"}, 
 "groupInfos"  : [ {"key" : "InsightsConfiguration",       "value" : "enabled"},
                   {"key" : "QuestionnaireConfiguration",  "value" : "enabled"},
                   {"key" : "ParadataConfiguration",       "value" : "enabled"},
                   {"key" : "OCRConfiguration",            "value" : "local_ocr"}]
}
```
---
### **[4] Enable a group**
---
**Description:** Only users who are within an 'enabled' group are able to authenticate with the backend.

**Access:** Superuser

**URL:** ```/group/enable```

**Method:** POST

**Data Params:** 
```json
{
 "superuser" : {"name" : "<SUPERUSER_0>", "password" : "<PASSWORD>"},
 "group"     : {"name" : "<GROUP NAME>"}
}
```

---
### **[5] Disable a group**
---
**Description:** All users in a group with the status "disabled" have no access.

**Access:** Superuser

**URL:** ```/group/disable```

**Method:** POST

**Data Params:** 
```json
{
 "superuser" : {"name" : "<SUPERUSER_0>", "password" : "<PASSWORD>"},
 "group"     : {"name" : "<GROUP NAME>"}
}
```

---
### **[6] Add (create) new users to an existing group**
---
**Description:** Adds new users to a group.

**Access:** Superuser

**URL:** ```/users/create```

**Method:** POST

**Data Params:** 
```json
{
 "superuser" : {"name" : "<SUPERUSER_0>", "password" : "<PASSWORD>"},
 "group"     : {"name" : "<GROUP NAME>"},
 "users"     : [ {"name" : "<USER_0>", "password" : "<PASSWORD>"},
                 {"name" : "<USER_1>", "password" : "<PASSWORD>"},
                 {"name" : "<USER_2>", "password" : "<PASSWORD>"},
                 {"name" : "<USER_3>", "password" : "<PASSWORD>"} ]
}
```

---
### **[7] List all groups**
---
**Description:** List all existing groups.

**Access:** Superuser

**URL:** ```/group/list```

**Method:** POST

**Data Params:** 
```json
{
 "superuser" : {"name" : "<SUPERUSER_0>", "password" : "<PASSWORD>"}
}
```

---
### **[8] List all users that belong to a group**
---
**Description:** List all users that belong to a group

**Access:** Superuser

**URL:** ```/users/list```

**Method:** POST

**Data Params:** 
```json
{
 "superuser" : {"name" : "<SUPERUSER_0>", "password" : "<PASSWORD>"},
 "group"     : {"name" : "<GROUP NAME>"}
}
```


---
### **[9] Replace the GroupInfos of a group**
---
**Description:** Replace the GroupInfos of a group. See step [3] for more info about possible settings.

**Access:** Supseruser

**URL:** ```/group/infos```

**Method:** POST

**Data Params:** 
```json
{
 "superuser"   : {"name" : "<SUPERUSER_0>", "password" : "<PASSWORD>"},
 "group"       : {"name" : "<GROUP NAME>"}, 
 "groupInfos"  : [ {"key" : "InsightsConfiguration",       "value" : "delayed"},
                   {"key" : "QuestionnaireConfiguration",  "value" : "disabled"},
                   {"key" : "ParadataConfiguration",       "value" : "disabled"} ]
}
```

---
### **[10] Change passwords of existing users**
---
**Description:** Change passwords of existing users.

**Access:** Superuser

**URL:** ```/users/passwords```

**Method:** POST

**Data Params:** 
```json
{
 "superuser" : {"name" : "<SUPERUSER_0>", "password" : "<PASSWORD>"},
 "users"     : [ {"name" : "<USER_0>", "password" : "<NEW_PASSWORD>"},
                 {"name" : "<USER_3>", "password" : "<NEW_PASSWORD>"} ]
}
```

---
### **[11] Change the group for all users in a group**
---
**Description:** Change the group for all users in a group. 

Note: You can not change the group superusers for all superusers.

**Access:** Superuser

**URL:** ```/users/regroupgroup```

**Method:** POST

**Data Params:** 
```json
{
 "superuser" : {"name" : "<SUPERUSER_0>", "password" : "<PASSWORD>"},
 "group"     : {"name" : "<GROUP NAME>"},
 "regroup"   : {"name" : "<NEW GROUP NAME>"}
}
```

---
### **[12] Change the group for a set of users**
---
**Description:** Change the group for a set of users. 

Note 1: You can change the group superusers for a set of superusers. 

Note 2: Create new superusers (see [2]) if all superusers ae gone.

**Access:** Superuser

**URL:** ```/users/regroupusers```

**Method:** POST

**Data Params:** 
```json
{
 "superuser" : {"name" : "<SUPERUSER_0>", "password" : "<PASSWORD>"},
 "regroup"   : {"name" : "<NEW GROUP NAME>"},
 "users"     : [ {"name" : "<USER_0>"},
                 {"name" : "<USER_3>"} ]
}
```





---
### **[13] Generate a set of users**
---
**Description:** Generate a set of users

**Access:** Superuser

**URL:** ```/users/generate```

**Method:** POST

**Data Params:** 
```json
{
 "superuser" : {"name" : "<SUPERUSER_0>", "password" : "<PASSWORD>"},
 "group"     : {"name" : "<GROUP NAME>"},
 "generate"  : {"number" : <100>, "prefix" : "<HBSB_>", "file" : ""}
}
```
