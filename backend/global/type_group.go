package global

type Group struct {
	Id          int64     `json:"id"`
	Name        string    `json:"name"`
	Status      string    `json:"status"`
	Users       int64     `json:"users"`
}

type Group_Info struct {
	Id          int64     `json:"id"`
	Group_Id    int64	  `json:"groupId"`
	Key        	string    `json:"key"`
	Value    	string    `json:"value"`
}

type GroupBody struct {
	Superuser	*User			`json:"superuser"`
	Group		*Group			`json:"group"`
	Group_Infos	[]Group_Info	`json:"groupInfos"`
}

type GroupData struct {
	Group		*Group			`json:"group"`
	Group_Infos	[]Group_Info	`json:"groupInfos"`
}
