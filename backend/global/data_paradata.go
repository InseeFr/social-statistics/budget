package global

type Paradata struct {
	Action 		string  `json:"action"`
	ObjectName 	string  `json:"objectName"`
	Timestamp 	int64  	`json:"timestamp"`
}

type ParadataData struct {
	Synchronisation 	*Synchronisation 	`json:"synchronisation"`
	Paradatas  			[]Paradata 			`json:"paradatas"`
}

type ParadataBody struct {
	User 				*User 				`json:"user"`
	Phone 				*Phone 				`json:"phone"`
	Sync_Order 			int64	     		`json:"syncOrder"`
	Synchronisation 	*Synchronisation 	`json:"synchronisation"`
	Paradatas  			[]Paradata 			`json:"paradatas"`
}

type ParadataDateTime struct {
	GroupName	string		`json:"groupName"`
	UserName	string		`json:"userName"`
	Action		string		`json:"action"`
	ObjectName	string		`json:"objectName"`
	Timestamp	int64		`json:"timestamp"`
	Year		string		`json:"year"`
	Month		string		`json:"month"`
	Day			string		`json:"day"`
	Hour		string		`json:"hour"`
	Min			string		`json:"min"`
	Sec			string		`json:"sec"`
	Msec		string		`json:"msec"`
}

type ParadataDateTimeData struct {
	ParadataDateTimes	[]ParadataDateTime	`json:"paradataDateTimes"`
}

type ParadataScreenTime struct {
	GroupName	string		`json:"groupName"`
	UserName	string		`json:"userName"`
	ObjectName	string		`json:"objectName"`
	ScreenTime	int64		`json:"screenTime"`
}

type ParadataScreenTimeData struct {
	ParadataScreenTimes	[]ParadataScreenTime	`json:"paradataScreenTimes"`
}

type ParadataClick struct {
	GroupName	string		`json:"groupName"`
	UserName	string		`json:"userName"`
	Action		string		`json:"action"`
	ObjectName	string		`json:"objectName"`
	Clicks		int64		`json:"clicks"`
}

type ParadataClickData struct {
	ParadataClicks	[]ParadataClick	`json:"paradataClicks"`
}





