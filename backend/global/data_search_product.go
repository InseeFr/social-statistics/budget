package global


type SearchProduct struct {
	Product   		string  `json:"product"`
	ProductCategory string  `json:"productCategory"`
	ProductCode 	string  `json:"productCode"`
	LastAdded       int 	`json:"lastAdded"`
	Count     		int  	`json:"count"`
}

type SearchProductData struct {
	Synchronisation *Synchronisation 	`json:"synchronisation"`
	SearchProduct   *SearchProduct 		`json:"searchProduct"`
}

type SearchProductBody struct {
	User 			*User 				`json:"user"`
	Phone 			*Phone 				`json:"phone"`
	Sync_Order 		int64	     		`json:"syncOrder"`
	Synchronisation *Synchronisation 	`json:"synchronisation"`
	SearchProduct   *SearchProduct 		`json:"searchProduct"`
}