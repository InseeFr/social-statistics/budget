package global


type ExperienceSampling struct {
	SendTime string  			`json:"sendTime"`
	ReadTime string  			`json:"readTime"`
	IsFeelingQuestionair int  	`json:"isFeelingQuestionair"`
	IsFirstAttempt int  		`json:"isFirstAttempt"`
	Happy int  					`json:"happy"`
	Energetic int  				`json:"energetic"`
	Relaxed int  				`json:"relaxed"`
	Cheerful int  				`json:"cheerful"`
	SocialMedia int  			`json:"socialMedia"`
	Texting int  				`json:"texting"`
	Games int  					`json:"games"`
	NewsOnline int  			`json:"newsOnline"`
	ReadingPaper int  			`json:"readingPaper"`    
}

type ExperienceSamplingData struct {
	Synchronisation 	*Synchronisation 		`json:"synchronisation"`
	ExperienceSampling  *ExperienceSampling 	`json:"experienceSampling"`
}

type ExperienceSamplingBody struct {
	User 				*User 					`json:"user"`
	Phone 				*Phone 					`json:"phone"`
	Sync_Order 			int64	     			`json:"syncOrder"`
	Synchronisation 	*Synchronisation 		`json:"synchronisation"`
	ExperienceSampling  *ExperienceSampling 	`json:"experienceSampling"`
}