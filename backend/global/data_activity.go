package global

type ActivityInfo struct {
    TreeCode      string  `json:"treeCode"`
    DateId        int64   `json:"dateId"`
    ActivityType  int     `json:"activityType"`
    NodeCode      string  `json:"nodeCode"`
    Description   string  `json:"description"`
    Tick          int     `json:"tick"`
    Height        int     `json:"height"`
    StartHour     int     `json:"startHour"`
    StartMinute   int     `json:"startMinute"`
    StopHour      int     `json:"stopHour"`
    StopMinute    int     `json:"stopMinute"`
}

type ActivityProperty struct {
    TreeCode      	string  `json:"treeCode"`
    DateId        	int64   `json:"dateId"`
    ActivityType  	int     `json:"activityType"`
    StartHour     	int     `json:"startHour"`
	StartMinute   	int     `json:"startMinute"`
    Key				string  `json:"key"`
	Value			string  `json:"value"`
}

type ActivityComplete struct {
    Year      	string  `json:"year"`
    Month      	string  `json:"month"`
    Day      	string  `json:"day"`
}
	
type ActivityData struct {
	Synchronisation 	*Synchronisation 	`json:"synchronisation"`
	ActivityInfos       []ActivityInfo 		`json:"activityInfos"`
	ActivityProperties  []ActivityProperty 	`json:"activityProperties"`
	ActivityCompletes   []ActivityComplete 	`json:"activityCompletes"`
}

type ActivityBody struct {
	User 				*User 				`json:"user"`
	Phone 				*Phone 				`json:"phone"`
	Sync_Order 			int64	     		`json:"syncOrder"`
	Synchronisation 	*Synchronisation 	`json:"synchronisation"`
	ActivityInfos       []ActivityInfo 		`json:"activityInfos"`
	ActivityProperties  []ActivityProperty 	`json:"activityProperties"`
	ActivityCompletes   []ActivityComplete 	`json:"activityCompletes"`
}
