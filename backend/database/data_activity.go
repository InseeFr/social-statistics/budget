package database

import (
	_ "github.com/lib/pq"
	"backend/global"
)

// ### ACTIVITY_DATA ######################################################################################

func PushNextActivityData(body global.ActivityBody) int64 {
	var user global.User
	var phone global.Phone
	var sync_id int64
	var updated bool
	var addData bool

	var errS error
	var errP error
	var errI error
	var errC error

	user, _ = Select_User_ById(body.Synchronisation.User_Id)
	phone, _ = Select_Phone_ById(body.Synchronisation.Phone_Id)

	var pushed int64 = body.Synchronisation.Sync_Order
	var latest int64 = user.Sync_Order
	latest = latest + 1
	body.Synchronisation.Sync_Order = latest

	//BEGIN TRANSACTION #####################################
	global.MyPrint("Begin Transaction")
	var validTransaction bool = true
	tx, err := con.Begin()
	if err != nil {
		validTransaction = false
	} else {

		addData = false

		var synchronisation global.Synchronisation
		synchronisation, errS = Select_Synchronisation_ByDataIdentifier(tx,
			body.Synchronisation.Data_Type,
			body.Synchronisation.Data_Identifier, body.Synchronisation.User_Id)

		if errS != nil {
			sync_id, errS = Create_Synchronisation(tx, body.Synchronisation)
			if errS != nil {
				validTransaction = false
			} else {
				addData = true
			}
		} else {
			sync_id = synchronisation.Id
			body.Synchronisation.Id = synchronisation.Id
			updated, err = Update_Synchronisation(tx, body.Synchronisation)
			if err != nil {
				validTransaction = false
			} else {
				if updated {
					errI = Delete_Activity_Infos(tx, sync_id)
					errP = Delete_Activity_Properties(tx, sync_id)
					errC = Delete_Activity_Completes(tx, sync_id)
					if errI != nil || errP != nil || errC != nil {
						validTransaction = false
					} else {
						addData = true
					}
				}
			}
		}

		if validTransaction {
			if addData {
				for _, activityInfo := range body.ActivityInfos {
					errI = Create_Activity_Info(tx, sync_id, &activityInfo)
					if errI != nil {
						break
					}
				}
				for _, activityProperty := range body.ActivityProperties {
					errP = Create_Activity_Property(tx, sync_id, &activityProperty)
					if errP != nil {
						break
					}
				}
				for _, activityComplete := range body.ActivityCompletes {
					errC = Create_Activity_Complete(tx, sync_id, &activityComplete)
					if errP != nil {
						break
					}
				}
			}
		}

		if validTransaction {
			user.Sync_Order = latest
			err = Update_UserSyncOrder(tx, user)
			if err != nil {
				validTransaction = false
			} else {
				phone.Sync_Order = pushed
				err = Update_Phone(tx, phone)
				if err != nil {

					validTransaction = false
				}
			}
		}
	}

	if validTransaction {
		tx.Commit()
		global.MyPrint("& Commit Transaction")
	} else {
		tx.Rollback()
		global.MyPrint("! Rollback Transaction")
	}
	//END TRANSACTION #######################################

	return sync_id
}

func PullNextActivityData(user_id int64, phone_id int64, pulled_sync_order int64) (global.ActivityData, error) {
	var synchronisation global.Synchronisation
	var activityInfos []global.ActivityInfo
	var activityProperties []global.ActivityProperty
	var activityCompletes  []global.ActivityComplete

	synchronisation, err = Select_Synchronisation_NextToPull(global.DATA_ACTIVITY, user_id, phone_id, pulled_sync_order)

	if synchronisation.Action != global.ACTION_DELETE {
		activityInfos, _ = Select_Activity_Infos(synchronisation.Id)
		activityProperties, _ = Select_Activity_Properties(synchronisation.Id)
		activityCompletes, _ = Select_Activity_Completes(synchronisation.Id)
	}

	var data global.ActivityData
	data.Synchronisation = &synchronisation
	data.ActivityInfos = activityInfos
	data.ActivityProperties = activityProperties
	data.ActivityCompletes = activityCompletes

	return data, nil
}

func GetActivityData(sync_id int64) global.ActivityData {
	var synchronisation global.Synchronisation
	var activityInfos []global.ActivityInfo
	var activityProperties []global.ActivityProperty
	var activityCompletes  []global.ActivityComplete

	synchronisation, _ = Select_Synchronisation_ById(sync_id)
	activityInfos, _ = Select_Activity_Infos(synchronisation.Id)
	activityProperties, _ = Select_Activity_Properties(synchronisation.Id)
	activityCompletes, _ = Select_Activity_Completes(synchronisation.Id)

	var data global.ActivityData
	data.Synchronisation = &synchronisation
	data.ActivityInfos = activityInfos
	data.ActivityProperties = activityProperties
	data.ActivityCompletes = activityCompletes

	return data
}
