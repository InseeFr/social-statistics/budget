package database

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"backend/global"
)

func scan_ParadataClick_Row(rows *sql.Rows) global.ParadataClick {
	var groupName string
	var userName string
	var action string
	var objectName string
	var clicks int64

	err = rows.Scan(&groupName, &userName, &action, &objectName, &clicks)
	if err != nil {
		panic(err)
	}
	return global.ParadataClick{
		GroupName: 	groupName,
		UserName:   userName,
		Action: 	action,
		ObjectName: objectName,
		Clicks:  	clicks}
}

func select_ParadataClick(group_id int64) ([]global.ParadataClick, error) {
	var paradataClicks []global.ParadataClick
	
	sqlStatement := "" +
	"SELECT g.name, u.name, p.click as action, p.objectname, p.clicks " +
	"FROM (SELECT sync_id, " +
    "      case when left(action, 8) = 'open day' then 'open day' else action end as click, " +
    "      objectname, count(*) as clicks " +
	"      FROM public.tbl_paradata " +
	"      where left(action,4) = 'open' " +
	"      group by sync_id, click, objectname " +
	"	  ) p " +
	"left join tbl_sync s on p.sync_id = s.id  " +
	"left join tbl_user u on s.user_id = u.id  " +
	"left join tbl_group g on u.group_id = g.id  " +
	"where g.id = $1 " 

	rows, err := con.Query(sqlStatement, group_id)
	if err != nil {
		return paradataClicks, err
	}
	defer rows.Close()
	
	var found bool = false
	for rows.Next() {
		paradataClicks = append(paradataClicks, scan_ParadataClick_Row(rows))
		found = true
	}

	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return paradataClicks, err
	}

	if found {
		return paradataClicks, nil
	} else {
		return paradataClicks, errors.New("Error: No ParadataClicks foud")
	}
}


func Get_ParadataClick(group_id int64) (global.ParadataClickData, error) {
	var paradataClicks []global.ParadataClick
	paradataClicks, err = select_ParadataClick(group_id)

	var data global.ParadataClickData
	data.ParadataClicks = paradataClicks

	return data, nil
}


