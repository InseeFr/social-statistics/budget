package database

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"backend/global"
)

func scan_ParadataScreenTime_Row(rows *sql.Rows) global.ParadataScreenTime {
	var groupName string
	var userName string
	var objectName string
	var screenTime int64

	err = rows.Scan(&groupName, &userName, &objectName, &screenTime)
	if err != nil {
		panic(err)
	}
	return global.ParadataScreenTime{
		GroupName: 	groupName,
		UserName:   userName,
		ObjectName: objectName,
		ScreenTime:  screenTime}
}

func select_ParadataScreenTime(group_id int64) ([]global.ParadataScreenTime, error) {
	var paradataScreenTimes []global.ParadataScreenTime
	
	sqlStatement := "" +
	"SELECT g.name, u.name, p.objectname, p.screentime " +
	"FROM (SELECT z.sync_id, z.objectname, sum(z.d) as screentime " +
	"	   FROM (SELECT y.sync_id, y.objectname, y.c - y.o as d " +
	"		     FROM (SELECT x.sync_id, x.objectname, x.o, min(x.c) as c " +
	"			       FROM (SELECT o.sync_id, o.objectname, o.timestamp as o, c.timestamp as c " +
	"				         FROM tbl_paradata o " +
	"				         JOIN tbl_paradata c on o.sync_id = c.sync_id and o.objectname = c.objectname  " +
	"				         and o.action = 'open screen' and c.action = 'close screen' and o.timestamp < c.timestamp " +
	"				        ) x " +
	"			       group by x.sync_id, x.objectname, x.o " +
	"			      ) y " +
	"		    ) z " +
	"	   group by z.sync_id, z.objectname " +
	"	  ) p " +
	"left join tbl_sync s on p.sync_id = s.id  " +
	"left join tbl_user u on s.user_id = u.id  " +
	"left join tbl_group g on u.group_id = g.id  " +
	"where g.id = $1 " 

	rows, err := con.Query(sqlStatement, group_id)
	if err != nil {
		return paradataScreenTimes, err
	}
	defer rows.Close()
	
	var found bool = false
	for rows.Next() {
		paradataScreenTimes = append(paradataScreenTimes, scan_ParadataScreenTime_Row(rows))
		found = true
	}

	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return paradataScreenTimes, err
	}

	if found {
		return paradataScreenTimes, nil
	} else {
		return paradataScreenTimes, errors.New("Error: No ParadataScreenTimes foud")
	}
}


func Get_ParadataScreenTime(group_id int64) (global.ParadataScreenTimeData, error) {
	var paradataScreenTimes []global.ParadataScreenTime
	paradataScreenTimes, err = select_ParadataScreenTime(group_id)

	var data global.ParadataScreenTimeData
	data.ParadataScreenTimes = paradataScreenTimes

	return data, nil
}


