package database

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"backend/global"
)

func scan_ParadataDateTime_Row(rows *sql.Rows) global.ParadataDateTime {
	var groupName string
	var userName string
	var action string
	var objectName string
	var timestamp int64
	var year string
	var month string
	var day string
	var hour string
	var min string
	var sec string
	var msec  string

	err = rows.Scan(&groupName, &userName, &action, &objectName, &timestamp, 
		&year, &month, &day, &hour, &min, &sec, &msec)
	if err != nil {
		panic(err)
	}
	return global.ParadataDateTime{
		GroupName: 	groupName,
		UserName:   userName,
		Action:   	action,
		ObjectName: objectName,
		Timestamp:  timestamp,
		Year:   	year,
		Month:   	month,
		Day:   		day,
		Hour:   	hour,
		Min:   		min,
		Sec:   		sec,
		Msec:   	msec}
}

func select_ParadataDateTime(group_id int64) ([]global.ParadataDateTime, error) {
	var paradataDateTimes []global.ParadataDateTime
	
	sqlStatement := "" +
	"SELECT g.name, u.name, p.action, p.objectname, p.timestamp,  " +
	"to_char(to_timestamp(timestamp/1000.0), 'YYYY') as year, " +
	"to_char(to_timestamp(timestamp/1000.0), 'MM') as month, " +
	"to_char(to_timestamp(timestamp/1000.0), 'DD') as day, " +
	"to_char(to_timestamp(timestamp/1000.0), 'HH') as hour, " +
	"to_char(to_timestamp(timestamp/1000.0), 'MI') as min, " +
	"to_char(to_timestamp(timestamp/1000.0), 'SS') as sec, " +
	"substring(timestamp::varchar, 10,4) as msec " +
	"FROM tbl_paradata p " +
	"left join tbl_sync s on p.sync_id = s.id " +
	"left join tbl_user u on s.user_id = u.id " +
	"left join tbl_group g on u.group_id = g.id " +
	"where g.id = $1 "

	rows, err := con.Query(sqlStatement, group_id)
	if err != nil {
		return paradataDateTimes, err
	}
	defer rows.Close()
	
	var found bool = false
	for rows.Next() {
		paradataDateTimes = append(paradataDateTimes, scan_ParadataDateTime_Row(rows))
		found = true
	}

	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return paradataDateTimes, err
	}

	if found {
		return paradataDateTimes, nil
	} else {
		return paradataDateTimes, errors.New("Error: No ParadataDateTimes foud")
	}
}


func Get_ParadataDateTime(group_id int64) (global.ParadataDateTimeData, error) {
	var paradataDateTimes []global.ParadataDateTime
	paradataDateTimes, err = select_ParadataDateTime(group_id)

	var data global.ParadataDateTimeData
	data.ParadataDateTimes = paradataDateTimes

	return data, nil
}


