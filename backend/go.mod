module backend

go 1.23

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.8.1
	github.com/lib/pq v1.10.9
	gopkg.in/yaml.v2 v2.4.0
)
