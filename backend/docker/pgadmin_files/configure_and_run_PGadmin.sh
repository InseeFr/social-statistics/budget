#!/bin/sh

echo Create config file
cat > /var/lib/pgadmin/servers.json << EOF
{
    "Servers": {
        "1": {
            "Name": "$PGADMIN_SERVER_NAME",
            "Group": "Servers",
            "Host": "$DB_HOST",
            "Port": "$DB_PORT",
            "MaintenanceDB": "postgres",
            "Username": "$POSTGRES_USER",
            "SSLMode": "prefer",
            "SSLCert": "<STORAGE_DIR>/.postgresql/postgresql.crt",
            "SSLKey": "<STORAGE_DIR>/.postgresql/postgresql.key",
            "SSLCompression": 0,
            "Timeout": 10,
            "UseSSHTunnel": 0,
            "TunnelPort": "22",
            "TunnelAuthentication": 0
          }
      }
  }
EOF

# We need to delay the loading of the server until the PGadmin service is started. 3 seconds should be enough.
echo Load config file for server "$PGADMIN_SERVER_NAME" and user "$PGADMIN_DEFAULT_EMAIL in 3 seconds..."
sh -c "sleep 3; python /pgadmin4/setup.py --load-servers /var/lib/pgadmin/servers.json --user $PGADMIN_DEFAULT_EMAIL --sqlite-path /var/lib/pgadmin/pgadmin4.db" &

echo Start PGadmin service
/entrypoint.sh
