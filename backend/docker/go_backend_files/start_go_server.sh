#!/bin/bash

# renaming some environment variables
PORT=$GO_PORT
DB_NAME=$POSTGRES_DB
DB_USER=$POSTGRES_USER
DB_PASSWORD=$POSTGRES_PASSWORD

echo Waiting for PostgreSQL service ...
while ! curl http://$DB_USER:$DB_PASSWORD@$DB_HOST:$DB_PORT/$DB_NAME 2>&1 | grep 52; do sleep 1; done;
echo PostgreSQL online

# wait a bit more, just in case
sleep 2

if [[ $INITIALIZE_DATABASE == "true" || $INITIALIZE_DATABASE == "True" || $INITIALIZE_DATABASE == "TRUE" ]]
then
  echo Initializing database
  psql "host=$DB_HOST dbname=$DB_NAME user=$DB_USER password=$DB_PASSWORD port=$DB_PORT" \
    -a -f /app/scripts/create_tables.sql
fi

echo Starting GO backend server
/app/backend
