= Statistical Service Definition
:Author: Ole Mussmann
:Email: bo.mussmann@cbs.nl
:Date: 2020-03-19
:Document-Revision: 0.1
:Software-Revision: 0.1

*Conceptual description*, version _{document-revision}_

{date} by {author} {email}

== *@HBS App Backend*
.Version
{software-revision}

.Ownership
https://www.cbs.nl/[Statistics Netherlands (CBS)]

== Business Process: GSBPM
The service is used in the following GSBPM processes:

.BUILD
[horizontal]
3.1:: Reuse or build collection instrument

.COLLECT
[horizontal]
4.2:: Set up collection
4.3:: Run collection
4.4:: Finalize collection

== Business Function
The *aim* of the tool:

* `@HBS App Backend` is a REST API backend for smartphone apps used for primary data collection, targeting a preselected sample of respondents.

The tools *main functions* are:

* Smartphone data collection backend
* Collect user input as well as sensor data

The tools *focuses* on:

* General population research
* Government policy research
* Multi-disciplinary research - Target specific research

== Outcomes
* Executed data collection for a defined reference period
* Exchange of data via REST API
* Store data in PostgreSQL database

== Restrictions and Policies
`@HBS App Backend` is a REST API backend and thus needs a frontend for data collection.
This can be for example the `@HBS App`.

== Service Input/Output
=== GSIM Input
*API calls*

* Respondent’s configuration settings, e.g. different settings for different cohorts, login credentials
* Global configuration settings, e.g. survey period

=== GSIM Output
*PostgreSQL database* as data storage:

* User input
* Sensor measurements

== Service Dependencies
The tool needs to communicate via *REST API* with a *frontend* for data collection, for example the `@HBS App`.

== Exposure
The tool is open source and publicly available under the permissive https://opensource.org/licenses/MIT[MIT EXPAT license].

=== DOI
todo

=== Source Repository
https://gitlab.com/tabi/projects/budget/backend

== TODO
* app version
* DOI
