package helpers

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	_ "github.com/gorilla/mux"

	jwt "github.com/dgrijalva/jwt-go"
)

var (
	public_key_string string
	public_key        *rsa.PublicKey
	err               error
)

func InitializeOauthPublicKey() {
	// TODO Add HTTP GET to get back "public_key" value from JSON response
	public_key_string = AppConfig.OIDCPUBLICKEY
	public_key, err = parseKeycloakRSAPublicKey(public_key_string)
	log.Println("Public Key parsed")
}

func parseKeycloakRSAPublicKey(base64Encoded string) (*rsa.PublicKey, error) {
	buf, err := base64.StdEncoding.DecodeString(base64Encoded)
	if err != nil {
		return nil, err
	}
	parsedKey, err := x509.ParsePKIXPublicKey(buf)
	if err != nil {
		return nil, err
	}
	publicKey, ok := parsedKey.(*rsa.PublicKey)
	if ok {
		return publicKey, nil
	}
	return nil, fmt.Errorf("unexpected key type %T", publicKey)
}

func Protect(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		authHeader := r.Header.Get("Authorization")
		if len(authHeader) < 1 {
			w.WriteHeader(401)
			log.Println(fmt.Sprintf("UnauthorizedError, no header or len(authHeader) < 1 : %s", authHeader))
			json.NewEncoder(w).Encode(UnauthorizedError())
			return
		}

		token_string := strings.Split(authHeader, " ")[1]

		token, err := jwt.Parse(token_string, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
				return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
			}
			// return the public key that is used to validate the token.
			return public_key, nil
		})

		if err != nil {
			w.WriteHeader(400)
			log.Println(fmt.Sprintf("Error parsing or validating token: %s", err.Error()))
			json.NewEncoder(w).Encode(BadRequestError(err.Error()))
			return
		}

		if !token.Valid {
			w.WriteHeader(401)
			log.Println(fmt.Sprintf("UnauthorizedError, !isTokenValid = true - Header: %s ", authHeader))
			json.NewEncoder(w).Encode(UnauthorizedError())
			return
		}

		next.ServeHTTP(w, r)

	})
}

func GetUser(r *http.Request) string {
	authHeader := r.Header.Get("Authorization")
	token_string := strings.Split(authHeader, " ")[1]
	token, err := jwt.Parse(token_string, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		// return the public key that is used to validate the token.
		return public_key, nil
	})

	if err != nil {
		log.Println(fmt.Sprintf("Error parsing or validating token: %s", err.Error()))
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		return claims["preferred_username"].(string)
	}
	return ""
}
