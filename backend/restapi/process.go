package restapi

import (
	"backend/database"
	"backend/global"
	"backend/helpers"
	"encoding/json"
	"net/http"
)

func Process_Pull_ReceiptForProcess(w http.ResponseWriter, r *http.Request) {

	var body global.PullProcessBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	if body.User.Name != helpers.GetUser(r) {
		Forbidden(w, true, "Error: User-Token combination does not have permission")
		return
	}

	if !database.IsUserCbsProcess(*body.User) {
		badRequest(w, true, "User '"+body.User.Name+"' is not a CBS Process!")
		return
	}

	phone, err := database.RegisterPhone(*body.User, *body.Phone)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	group, err := database.Select_Group_ByName(body.Group.Name)
	if err != nil {
		badRequest(w, true, "Group '"+body.Group.Name+"' does not exist!")
		return
	}

	var receiptData global.ReceiptData
	receiptData, _ = database.PullNextReceiptDataForProcess(phone.Id, group.Id, body.Sync_Id, body.Sync_Time)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(receiptData)
}

func Process_Push_ReceiptForProcess(w http.ResponseWriter, r *http.Request) {

	var body global.ReceiptBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	if body.User.Name != helpers.GetUser(r) {
		Forbidden(w, true, "Error: User-Token combination does not have permission")
		return
	}

	if !database.IsUserCbsProcess(*body.User) {
		badRequest(w, true, "User '"+body.User.Name+"' is not a CBS Process!")
		return
	}

	phone, err := database.RegisterPhone(*body.User, *body.Phone)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	setSynchronisation(body.Synchronisation, global.DATA_RECEIPT, body.Synchronisation.User_Id, phone.Id, body.Sync_Order)
	sync_id := database.PushNextReceiptData(body, false)
	data := database.GetReceiptData(sync_id)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}
