package restapi

import (
	"backend/database"
	"backend/global"
	"backend/helpers"
	"encoding/json"
	"net/http"
)

func Data_Pull_Activity(w http.ResponseWriter, r *http.Request) {

	var body global.PullDataBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	if body.User.Name != helpers.GetUser(r) {
		Forbidden(w, true, "Error: User-Token combination does not have permission")
		return
	}

	phone, err := database.RegisterPhone(*body.User, *body.Phone)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	var data global.ActivityData
	data, _ = database.PullNextActivityData(phone.User_Id, phone.Id, body.Sync_Order)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}

func Data_Push_Activity(w http.ResponseWriter, r *http.Request) {

	var body global.ActivityBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	if body.User.Name != helpers.GetUser(r) {
		Forbidden(w, true, "Error: User-Token combination does not have permission")
		return
	}

	phone, err := database.RegisterPhone(*body.User, *body.Phone)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	setSynchronisation(body.Synchronisation, global.DATA_ACTIVITY, phone.User_Id, phone.Id, body.Sync_Order)
	sync_id := database.PushNextActivityData(body)
	data := database.GetActivityData(sync_id)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}
