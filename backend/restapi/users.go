package restapi

import (
	"encoding/json"
	"net/http"
	"backend/database"
	"backend/global"
	"backend/passwords"
)

// UserBody
// {
// 	"superuser" : {"name" : "su1n", "password" : "su1p"},
// 	"group"  	: {"name" : "HBO 2020 A"},
// 	"regroup"  	: {"name" : "HBO 2020 B"},
// 	"users"     : [	{"name" : "nu1n", "password" : "nu1p"},
// 					{"name" : "nu2n", "password" : "nu2p"},
// 					{"name" : "nu3n", "password" : "nu3p"},
// 					{"name" : "nu4n", "password" : "nu4p"}]
// }

func Users_List_All(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("Users_List_All")

	var body global.UserBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	if !database.IsUserSuperuser(*body.Superuser) && !database.IsUserMonitoring(*body.Superuser) && !database.IsUserCasemanagement(*body.Superuser) {
		badRequest(w, true, "Only superusers can list all users!")
		return
	}

	var data global.UserNameData
	data.UserNames, _ = database.Select_All_Users()

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}

func Users_List(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("Users_List")

	var body global.UserBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	if !database.IsUserSuperuser(*body.Superuser) && !database.IsUserMonitoring(*body.Superuser) && !database.IsUserCasemanagement(*body.Superuser) {
		badRequest(w, true, "Only superusers can list Users Group!")
		return
	}
	
	group, err := database.Select_Group_ByName(body.Group.Name)
	if err != nil {
		badRequest(w, true, "Group '" + body.Group.Name + "' does not exist!")
		return
	}

	users, err := database.Select_Users_ByGroup(group.Id)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(users)
}

func Users_Create(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("Users_Create")

	var id int64

	var body global.UserBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	var group global.Group	

	
	group, err = database.Select_Group_ByName(global.GROUP_SUPERUSER)
	if err != nil {
		badRequest(w, true, "Group '" + global.GROUP_SUPERUSER + "' has to be created first!")
		return
	} else {
		body.Superuser.Group_Id = group.Id
		_, err = database.Select_User_ByNamePasswordGroup(*body.Superuser)
		if err != nil {
			
			group, err = database.Select_Group_ByName(global.GROUP_CMM)
			if err != nil {
				badRequest(w, true, "Group '" + global.GROUP_CMM + "' has to be created first!")
				return
			} else {
				body.Superuser.Group_Id = group.Id
				_, err = database.Select_User_ByNamePasswordGroup(*body.Superuser)
				if err != nil {
					var exists bool
					exists, err = database.Exists_Users_InGroup(group.Id)
					if err != nil {
						global.MyPrint(err.Error())
						badRequest(w, true, err.Error())
						return
					}
					if exists {
						global.MyPrint(err.Error())
						badRequest(w, true, "Only superusers and case management users can create users!")
						return
					} 
				}
			}
		}
	}
	
	group, err = database.Select_Group_ByName(body.Group.Name)
	if err != nil {
		badRequest(w, true, "Group '" + body.Group.Name + "' does not exists!")
		return
	}

	var users []global.User
	for _, user := range body.Users {
		user.Group_Id = group.Id

		_, err = database.Select_User_ByName(user.Name)
		if err == nil {
			badRequest(w, true, "User '" + user.Name + "' already exists.")
			return
		}

		id, err = database.Create_User(user)
		if err != nil {
			badRequest(w, true, err.Error())
			return
		}

		user, err = database.Select_User_ById(id)
		if err != nil {
			badRequest(w, true, err.Error())
			return
		}

		users = append(users, user)
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(users)
}

func Users_Regroup_Users(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("Users_Regroup_Users")

	var body global.UserBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	if !database.IsUserSuperuser(*body.Superuser)  && !database.IsUserCasemanagement(*body.Superuser) {
		badRequest(w, true, "Only superusers and case management users can change Users Group!")
		return
	}

	regroup, err := database.Select_Group_ByName(body.Regroup.Name)
	if err != nil {
		badRequest(w, true, "Group '" + body.Regroup.Name + "' does not exist!")
		return
	}

	var users []global.User
	for _, user := range body.Users {
		user, err = database.Select_User_ByName(user.Name)
		if err != nil {
			badRequest(w, true, "User '" + user.Name + "' does not exist!")
			return
		}

		err = database.Change_OneUserGroup(user, regroup.Id)
		if err != nil {
			badRequest(w, true, err.Error())
			return
		}

		user, err = database.Select_User_ByName(user.Name)
		if err != nil {
			badRequest(w, true, "User '" + user.Name + "' does not exist!")
			return
		}

		users = append(users, user)
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(users)
}

func Users_Regroup_Group(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("Users_Regroup_Group")

	var body global.UserBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	if !database.IsUserSuperuser(*body.Superuser)  && !database.IsUserCasemanagement(*body.Superuser) {
		badRequest(w, true, "Only superusers and case management users can change Users Group!")
		return
	}

	if body.Group.Name == global.GROUP_SUPERUSER {
		badRequest(w, true, "Group '" + body.Group.Name + "' can not be changed!")
		return
	}

	group, err := database.Select_Group_ByName(body.Group.Name)
	if err != nil {
		badRequest(w, true, "Group '" + body.Group.Name + "' does not exist!")
		return
	}

	regroup, err := database.Select_Group_ByName(body.Regroup.Name)
	if err != nil {
		badRequest(w, true, "Group '" + body.Regroup.Name + "' does not exist!")
		return
	}

	err = database.Change_AllUsersGroup(group.Id, regroup.Id)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(regroup)
}

func Users_Passwords(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("Users_Passwords")

	var body global.UserBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	if !database.IsUserSuperuser(*body.Superuser) && !database.IsUserCasemanagement(*body.Superuser) {
		badRequest(w, true, "Only superusers and case management users can change User Passwords!")
		return
	}

	var users []global.User
	for _, user := range body.Users {
		xuser, err := database.Select_User_ByName(user.Name)
		if err != nil {
			badRequest(w, true, "User '" + user.Name + "' does not exist!")
			return
		}

		err = database.Update_User_Password(xuser.Id, user.Password)
		if err != nil {
			badRequest(w, true, err.Error())
			return
		}

		xuser, err = database.Select_User_ByName(user.Name)
		if err != nil {
			badRequest(w, true, "User '" + user.Name + "' does not exist!")
			return
		}

		users = append(users, xuser)
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(users)
}

func Users_Generate(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("Users_Generate")

	var body global.UserBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	if !database.IsUserSuperuser(*body.Superuser)  && !database.IsUserCasemanagement(*body.Superuser) {
		badRequest(w, true, "Only superusers and case management users can generate Users!")
		return
	}

	group, err := database.Select_Group_ByName(body.Group.Name)
	if err != nil {
		badRequest(w, true, "Group '" + body.Group.Name + "' does not exist!")
		return
	}

	var users [] global.User
	users = passwords.GenerateUsers(body.Generate.Number, body.Generate.Prefix, "ULLD__MULD")

	var created [] global.User

	for _, user := range users {
		user.Group_Id = group.Id

		_, err = database.Select_User_ByName(user.Name)
		if err == nil {
			badRequest(w, true, "User '" + user.Name + "' already exists.")
			return
		}

		id, err := database.Create_User(user)
		if err != nil {
			badRequest(w, true, err.Error())
			return
		}

		user, err = database.Select_User_ById(id)
		if err != nil {
			badRequest(w, true, err.Error())
			return
		} 

		created = append(created, user)
	}

	passwords.CreatePasswordFiles(body.Generate.Folder, body.Generate.Prefix, created)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(created)
}

func Users_Delete(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("Users_Delete")

	var body global.UserBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	if !database.IsUserSuperuser(*body.Superuser)  && !database.IsUserCasemanagement(*body.Superuser) {
		badRequest(w, true, "Only superusers and case management users can delete Users!")
		return
	}

	user, err := database.Select_User_ByName(body.Users[0].Name)
	if err != nil {
		badRequest(w, true, "user '" + user.Name + "' does not exist!")
		return
	}

	var deleted [] global.User
	for _, user := range body.Users {
		user, err = database.Select_User_ByName(user.Name)
		if err != nil {
			badRequest(w, true, "User '" + user.Name + "' does not exist!")
			return
		}

		err = database.Delete_User(user.Name)
		if err != nil {
			badRequest(w, true, err.Error())
			return
		}
	}
	user.Password = "DELETED"
	deleted = append(deleted, user)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(deleted)
}

// func Users_Generate_Test(groupName string, number int, prefix string, passwordDefinition string) {
// 	global.MyPrint("#")
// 	global.MyPrint("Users_Generate_Test")

// 	group, err := database.Select_Group_ByName(groupName)
// 	if err != nil {
// 		badRequest(w, true, "Group '" + groupName + "' does not exist!")
// 		return
// 	}

// 	var users [] global.User
// 	users = passwords.GenerateUsers(number, prefix, passwordDefinition)

// 	var created [] global.User

// 	for _, user := range users {
// 		global.MyPrint(user.Name + " " + user.Password)
		
// 		user.Group_Id = group.Id

// 		_, err = database.Select_User_ByName(user.Name)
// 		if err == nil {
// 			badRequest(w, true, "User '" + user.Name + "' already exists.")
// 			return
// 		}

// 		id, err := database.Create_User(user)
// 		if err != nil {
// 			badRequest(w, true, err.Error())
// 			return
// 		}

// 		user, err = database.Select_User_ById(id)
// 		if err != nil {
// 			badRequest(w, true, err.Error())
// 			return
// 		} 

// 		created = append(created, user)
// 	}
// }
