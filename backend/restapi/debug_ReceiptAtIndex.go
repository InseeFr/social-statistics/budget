package restapi

import (
	"backend/database"
	"backend/global"
	"encoding/json"
	"net/http"
)

func Debug_ReceiptAtIndex(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("Process_Pull_ReceiptForProcess")

	//Load the json form the http request into a struct
	var body global.PullReceiptIndexBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	//Authenticate user, which requires: tbl_group.name = 'CBS Process'
	if !database.IsUserCbsProcess(*body.User) {
		badRequest(w, true, "User '"+body.User.Name+"' is not a CBS Process!")
		return
	}

	//Pull receipt at index
	var receiptData global.ReceiptData
	receiptData, _ = database.PullReceiptAtIndex(body.Index)

	// Send back receipt
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(receiptData)
}