package restapi

import (
	"encoding/json"
	"net/http"
	"backend/database"
	"backend/global"
)


// type Parameter struct {
// 	Name 		string  `json:"name"`
// 	Value	 	string  `json:"value"`
// }

// type DashboardBody struct {
// 	Superuser	*User		`json:"superuser"`
// 	Group		*Group		`json:"group"`
// 	User		*User		`json:"user"`
// 	Parameters	[]Parameter	`json:"parameters"`
// }


func Dashboard_ReceiptsPerDay(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("Dashboard_ReceiptePerDay")

	var body global.DashboardBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	if !database.IsUserSuperuser(*body.Superuser) && !database.IsUserMonitoring(*body.Superuser) {
		badRequest(w, true, "Only superusers and monitoring users can view receipts per day!")
		return
	}
	
	group, err := database.Select_Group_ByName(body.Group.Name)
	if err != nil {
		badRequest(w, true, "Group '" + body.Group.Name + "' does not exist!")
		return
	}

	var data global.ReceiptsPerDayData
	data, _ = database.Get_ReceiptsPerDay(group.Id)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}
